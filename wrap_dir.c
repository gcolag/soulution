
/*
 * File:         wrap_dir
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  DIR wrapper
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include "config_mgr.h"
#include "cs8416.h"
#include "dbio.h"


/*****************************************************************************/


unsigned char WM8805_status;

char WM8805_init (void) { return 0; }

unsigned char WM8805_get_interrupt(void) { return 0; }

unsigned char WM8805_check_int_pin(void) { return 0; }
/*
{

  unsigned short cs8416_status;
  int locked;

  cs8416_get_status(&cs8416_status);
  locked = (cs8416_status & 0x0080) != 0;
 
  return locked; 
}
*/


unsigned char WM8805_get_status(void) { return 0; }

char WM8805_RX_select (unsigned char channel)
{
  switch (channel)
  {
    case DIGITAL_INPUT_SPDIF:
      cs8416_select_input(CS8416_INPUT_RXP2);
//      dbio_select_dir();
      break;
    case DIGITAL_INPUT_AES_EBU:
      cs8416_select_input(CS8416_INPUT_RXP1);
//      dbio_select_dir();
      break;
    case DIGITAL_INPUT_TOSLINK:
      cs8416_select_input(CS8416_INPUT_RXP3);
//      dbio_select_dir();
      break;
  }

  return 0; 
}


/*****************************************************************************/

