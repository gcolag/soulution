
/*
 * File:         test
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 *
 * Created:      2015/04/10
 * Description:  Test module
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include <p24fxxxx.h>
#include <PPS.h>

#include <stdio.h>

#include "GenericTypeDefs.h"

#include "periph_spi.h"
#include "hdsp250x.h"
#include "mcp23s17.h"
#include "fp_display.h"
#include "io.h"
#include "test.h"
#include "keys.h"
#include "rc5.h"
#include "i2c.h"
#include "pca9555.h"
#include "dbio.h"
#include "dacbio.h"
#include "cs8416.h"
#include "PCM1792.h"
#include "cpld_dsp_board.h"
#include "eeprom.h"


/*****************************************************************************/


static void eternal_blink(int speed)
{
  while (1) 
  {
    led_cmd_off();
    delay_ms(speed);
    led_cmd_on();
    delay_ms(speed);
  }
}


static void my_delay_2(int delay)
{
  delay_ms(delay);
}


/*****************************************************************************/


void test_1(void)
{
  FP_nRST = 1;

  spi1_open(0);

  FP_nCS = 0;
  spi1_write(0x40);
//  spi_write(0x10);
  spi1_write(0x02);
  spi1_write(0x0F);
  FP_nCS = 1;

  FP_nCS = 0;
  spi1_write(0x40);
  spi1_write(0x56);
  spi1_write(0xF0);
  FP_nCS = 1;

  spi1_close();
}


/*****************************************************************************/


void test_2(void)
{
  int status;

  FP_nRST = 1;

  status = mcp23s17_init();

/*
  if (status == 0)
    eternal_blink(30);
  else if (status == 0xFF)
    eternal_blink(5);
  else
  {
    eternal_blink(100);
  }
*/

  if (status == 0)
  {
#if 0
//    mcp23s17_write(0x01, 0x00);
//    mcp23s17_write(0x13, 0xAF);
    mcp23s17_write(0x01, 0x0F);
    mcp23s17_write(0x13, 0x70);
#else
    mcp23s17_tris_clr(0xF000);
    mcp23s17_gpio_set(0x7000);
#endif
  }

  if (status == 0)
    eternal_blink(30);
  else
    eternal_blink(100);

}


/*****************************************************************************/


void test_3(void)
{
  int status;

  FP_nRST = 1;
  status = mcp23s17_init();
  hdsp250x_init();
  hdsp250x_test();

  while (1) 
  {
    hdsp250x_test();

    STATUS_LED = 0;
    delay_ms(200);
    STATUS_LED = 1;
    delay_ms(200);
  }
}


/*****************************************************************************/


void test_4(void)
{
  int status;

  FP_nRST = 1;
  status = mcp23s17_init();
  fpd_init();


  while (1)
  {
    fpd_write_string(0, 0, "Salut les copains");
    fpd_write_string(1, 1, "HELLO");
    fpd_write_string(2, 2, "BYE");
    
    my_delay_2(100);

    fpt_set_brightness(6);

    my_delay_2(100);

    fpt_set_brightness(3);

    my_delay_2(100);

    fpt_set_brightness(0);

    my_delay_2(100);

    fpd_clear();

    my_delay_2(100);
  }

  int speed = 50;

  while (1) 
  {
    STATUS_LED = 0;
    my_delay_2(speed);
    STATUS_LED = 1;
    my_delay_2(speed);
  }
}


/*****************************************************************************/


void test_5(void)
{
  int status;

  FP_nRST = 1;
  status = mcp23s17_init();
  fpd_init();

  while (1)
  {
    if (SW_1)
      fpd_write_string(0, 0, "1");
    else
      fpd_write_string(0, 0, " ");

    if (SW_2)
      fpd_write_string(0, 1, "2");
    else
      fpd_write_string(0, 1, " ");

    if (SW_3)
      fpd_write_string(0, 2, "3");
    else
      fpd_write_string(0, 2, " ");

    if (SW_4)
      fpd_write_string(0, 3, "4");
    else
      fpd_write_string(0, 3, " ");

    if (SW_5)
      fpd_write_string(0, 4, "5");
    else
      fpd_write_string(0, 4, " ");
  }
}


/*****************************************************************************/


extern int g_rc_timer;
extern int g_keyid;


void test_6(void)
{
  int i;
  int status;
  char dbuff[] = "0";
  char dbuff2[20];
#if 0
  int cnt;
#endif

  FP_nRST = 1;
  status = mcp23s17_init();
  fpd_init();

  while (1)
  {
    for (i=0 ; i<10 ; i++)
    {
      dbuff[0] = '0' + i;
      fpd_write_string(0, 0, dbuff);

#if 0
      cnt = readTimerCounter();
      sprintf(dbuff2, "%u   ", cnt);
      fpd_write_string(1, 0, dbuff2);

      cnt = T1CON;
      sprintf(dbuff2, "%04X", cnt);
      fpd_write_string(2, 0, dbuff2);
#endif

#if 0
      cnt = IC5CON1;
      sprintf(dbuff2, "%04X", cnt);
      fpd_write_string(1, 0, dbuff2);

      cnt = IC5CON2;
      sprintf(dbuff2, "%04X", cnt);
      fpd_write_string(2, 0, dbuff2);
#endif

#if 1
      rcKeyStruct key;

      readRCKey(&key);
//      sprintf(dbuff2, "%02X", key.id);
//      sprintf(dbuff2, "%02X", g_keyid);
      sprintf(dbuff2, "%02X/%02X", key.id, g_keyid);

      fpd_write_string(1, 0, dbuff2);

      sprintf(dbuff2, "%04X", key.count);
      fpd_write_string(2, 0, dbuff2);
#endif

      if (FP_ROTA_A)
        fpd_write_string(0, 4, "o");
      else
        fpd_write_string(0, 4, "O");

      if (FP_ROTA_B)
        fpd_write_string(0, 5, "o");
      else
        fpd_write_string(0, 5, "O");

      if (FP_ROTA_PUSH)
        fpd_write_string(0, 6, "o");
      else
        fpd_write_string(0, 6, "O");

      sprintf(dbuff2, "%5i", g_rc_timer);
      fpd_write_string(0, 8, dbuff2);

      sprintf(dbuff2, "%3i", rc5_get_error());
      fpd_write_string(1, 5, dbuff2);

      delay_ms(200);
    }
  }
}


/*****************************************************************************/


#define ADDRESS_PCA9555				(unsigned char)0x40


void test_7(void)
{
#if 0
  int i;
  int status;
  char dbuff[] = "0";
  char dbuff2[20];
  int cnt;
  unsigned char data;
  int mode = 0;

  FP_nRST = 1;
  status = mcp23s17_init();
  fpd_init();

	// Enable I2C communication port, Master, 100kHz
	OpenI2C1(I2C_ON | I2C_SLW_DIS, 230);

#if 0
//  status = InitPCA9554(ADDRESS_PCA9555, 0x00, 0x00);
//  data = ReadPCA9554(ADDRESS_PCA9555, 0);

//  data = DetectPCA9554(ADDRESS_PCA9555);
  data = DetectPCA9554(0x20);

//  sprintf(dbuff2, "%04X", status);
//  sprintf(dbuff2, "%02X", data);
  sprintf(dbuff2, "%i", data);
  fpd_write_string(2, 0, dbuff2);
#endif

#if 0
//  pca9555_reg_wr(ADDRESS_PCA9555, 0x06, 0xFF);
  pca9555_reg_wr(ADDRESS_PCA9555, 0x06, 0xFC);
//  pca9555_reg_wr(ADDRESS_PCA9555, 0x02, 0x01);
  pca9555_reg_wr(ADDRESS_PCA9555, 0x02, 0x02);

  pca9555_reg_rd(ADDRESS_PCA9555, 0x06, &data);
  sprintf(dbuff2, "%02X", data);
  fpd_write_string(2, 0, dbuff2);

  pca9555_reg_rd(ADDRESS_PCA9555, 0x07, &data);
  sprintf(dbuff2, "%02X", data);
  fpd_write_string(2, 4, dbuff2);
#endif

#if 0
  while (1)
  {
    for (i=0 ; i<10 ; i++)
    {
      dbuff[0] = '0' + i;
      fpd_write_string(0, 0, dbuff);

      mode = mode ? 0 : 1;
      if (mode)
        pca9555_reg_wr(ADDRESS_PCA9555, 0x02, 0x01);
      else
        pca9555_reg_wr(ADDRESS_PCA9555, 0x02, 0x02);

      delay_ms(200);
    }
  }
#endif
#endif
}


/*****************************************************************************/


void test_8(void)
{
#if 0
  int i;
  int status;
  char dbuff[] = "0";
  int mode = 0;

  FP_nRST = 1;
  status = mcp23s17_init();
  fpd_init();

	// Enable I2C communication port, Master, 100kHz
	OpenI2C1(I2C_ON | I2C_SLW_DIS, 230);
  dbio_init();
//  dbio_assert_osc_24M_en();

  while (1)
  {
    for (i=0 ; i<10 ; i++)
    {
      dbuff[0] = '0' + i;
      fpd_write_string(0, 0, dbuff);

      mode = mode ? 0 : 1;
      if (mode)
        dbio_assert_osc_22M_en();
      else
        dbio_deassert_osc_22M_en();

      delay_ms(200);
    }
  }
#endif
}


/*****************************************************************************/


void test_9(void)
{
#if 0
  int i;
  int status;
  char dbuff[] = "0";
  int mode = 0;

  FP_nRST = 1;
  status = mcp23s17_init();
  fpd_init();

	// Enable I2C communication port, Master, 100kHz
	OpenI2C1(I2C_ON | I2C_SLW_DIS, 230);
  dbio_init();

  dbio_select_nmr();
  dbio_deassert_usb_lan_reset();
  dbio_deassert_main_mute();

  while (1)
  {
    for (i=0 ; i<10 ; i++)
    {
      dbuff[0] = '0' + i;
      fpd_write_string(0, 0, dbuff);

      mode = mode ? 0 : 1;
      if (mode)
        dbio_assert_osc_22M_en();
      else
        dbio_deassert_osc_22M_en();

      fpd_write_string(0, 12, dbio_get_osc_22M_en() ? "1" : "0");
      fpd_write_string(0, 13, dbio_get_osc_24M_en() ? "1" : "0");

      delay_ms(200);
    }
  }
#endif
}


/*****************************************************************************/


void test_10(void)
{
  int status;

  FP_nRST = 1;
  status = mcp23s17_init();
  fpd_init();

	// Enable I2C communication port, Master, 100kHz
	OpenI2C1(I2C_ON | I2C_SLW_DIS, 230);
  dbio_init();

  dbio_select_nmr();
  dbio_deassert_usb_lan_reset();
  dbio_deassert_main_mute();


  while (1)
  {
    rcKeyStruct key;

    readRCKey(&key);

    if (key.id != 0xFF)
    {
      char dbuff[10];
      sprintf(dbuff, "%02X", key.id);
      fpd_write_string(2, 0, dbuff);
    }
  }
}


/*****************************************************************************/


void test_11(void)
{
  fpd_write_string(0, 0, "test 11");
 
  while (1)
  {
    cdb_write(0x07);
  }
}


/*****************************************************************************/


#define CS8416_ID_VERSION                0x7F


void test_12(void)
{
  unsigned char data;
  char dbuff[10];

  fpd_write_string(0, 0, "test 12");

  DIR_nCS = 1;
  dbio_assert_dir_reset();
  delay_ms(1000);
  dbio_deassert_dir_reset();

  while (1)
  {
    cs8416_read(CS8416_ID_VERSION, &data);
    sprintf(dbuff, "%02X", data);
    fpd_write_string(1, 0, dbuff);
  }
}


/*****************************************************************************/


#if 0

static void system_init(void)
{
  int status;

//  io_init();
  configIO();

  initTimer();
  initRc();

  FP_nRST = 1;
  status = mcp23s17_init();
  fpd_init();

	// Enable I2C communication port, Master, 100kHz
	OpenI2C1(I2C_ON | I2C_SLW_DIS, 230);
  dbio_init();

  spi2_open(0);
  cdb_write(0x07);

#if 0
  if (cs8416_init() != 0)
    error(ERR__CS8416_INIT);
#else
  cs8416_init();
#endif
}

#endif


void test_13(void)
{
#if 0
  int i;
  char dbuff[10];

  system_init();

  fpd_write_string(0, 0, "Hello");
    
  while (1)
  {
    for (i=0 ; i<10 ; i++)
    {
      dbuff[0] = '0' + i;
      dbuff[1] = 0;
      fpd_write_string(1, 0, dbuff);

      fpd_write_string(2, 6, dbio_get_osc_22M_en() ? "1" : "0");
      fpd_write_string(2, 7, dbio_get_osc_24M_en() ? "1" : "0");

      delay_ms(200);
    }
  }
#endif
}


/*****************************************************************************/


/*
 * Dispay RC code
 */


void test_14(void)
{
  fpd_write_string(0, 0, (char*)__FUNCTION__);

  while (1)
  {
    rcKeyStruct key;

    readRCKey(&key);

    if (key.id != 0xFF)
    {
      char dbuff[10];
      sprintf(dbuff, "%02X", key.id);
      fpd_write_string(2, 0, dbuff);
    }
  }
}


/*****************************************************************************/


/*
 * Dispay RC code
 */


void test_15(void)
{
  unsigned char cmd;
  char dbuff[10];
  int cnt = 0;

  fpd_write_string(0, 0, (char*)__FUNCTION__);
  volatileConfig.state = STATE_EXTERNAL_INPUT;

  while (1)
  {
    cmd = getCmd();

    if (cmd != 0x00)
    {
      sprintf(dbuff, "%02X %i ", cmd, cnt);
      fpd_write_string(2, 0, dbuff);
    }

    if (cmd == 0x17)
      if (cnt > 0)
        cnt--;
    if (cmd == 0x18)
      if (cnt < 9)
        cnt++;
  }
}


/*****************************************************************************/


/*
 * DAC board inits
 */


#define DAC_BOARD_LEFT_PCA9554_ADDRESS   0x70
#define DAC_BOARD_RIGHT_PCA9554_ADDRESS  0x72
#define TEST_PCA9554_ADDRESS  0x7E

#define DSP_BOARD_GENERAL_CTRL_PCA9555_ADDRESS   0x40
#define DSP_BOARD_HDMI_CTRL_PCA9555_ADDRESS      0x42

#define DAC_LEFT_I2C_ADDR    0x98 
#define DAC_RIGHT_I2C_ADDR   0x9A


void test_16(void)
{
  unsigned char cmd;
  char dbuff[10];
  int cnt = 0;
  int prev_cnt = -1;

  pca9555_dev_type  test_dev_var;
  pca9555_dev_type *test_dev = &test_dev_var;

  pca9555_dev_type  dac_left_dev_var;
  pca9555_dev_type *dac_left_dev = &dac_left_dev_var;
  pca9555_dev_type  dac_right_dev_var;
  pca9555_dev_type *dac_right_dev = &dac_right_dev_var;
  pca9555_dev_type  general_ctrl_dev_var;
  pca9555_dev_type *general_ctrl_dev = &general_ctrl_dev_var;
  pca9555_dev_type  dac_dev_var;
  pca9555_dev_type *dac_dev = &dac_dev_var;
  unsigned char data;
  unsigned char data2;

  test_dev->addr = TEST_PCA9554_ADDRESS;

  dac_left_dev->addr = DAC_BOARD_LEFT_PCA9554_ADDRESS;
  dac_right_dev->addr = DAC_BOARD_RIGHT_PCA9554_ADDRESS;
  general_ctrl_dev->addr = DSP_BOARD_GENERAL_CTRL_PCA9555_ADDRESS;
  dac_dev->addr = DAC_LEFT_I2C_ADDR;

  fpd_write_string(0, 0, (char*)__FUNCTION__);
  volatileConfig.state = STATE_EXTERNAL_INPUT;

  while (1)
  {
    cmd = getCmd();

    if (cmd == 0x17)
      if (cnt > 0)
        cnt--;
    if (cmd == 0x18)
      if (cnt < 9)
        cnt++;

    if (cmd != 0x00)
    {
      sprintf(dbuff, "%02X %i ", cmd, cnt);
      fpd_write_string(2, 0, dbuff);
    }

    if (prev_cnt != cnt)
    {
      switch (cnt)
      {
        case 1:
        {
          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_CONFIG_PORT,
                         &data);

//          dacbio_init();
          pca9555_reg_wr(dac_left_dev,
                         PCA9554_REG_CONFIG_PORT,
                         0xF8);

          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_CONFIG_PORT,
                         &data2);

          sprintf(dbuff, "%02X %02X", data, data2);
          fpd_write_string(1, 0, dbuff);

          break;
        }
        case 2:
        {
          pca9555_reg_rd(dac_right_dev,
                         PCA9554_REG_CONFIG_PORT,
                         &data);

//          dacbio_init();
          pca9555_reg_wr(dac_right_dev,
                         PCA9554_REG_CONFIG_PORT,
                         0xF8);

          pca9555_reg_rd(dac_right_dev,
                         PCA9554_REG_CONFIG_PORT,
                         &data2);

          sprintf(dbuff, "%02X %02X", data, data2);
          fpd_write_string(1, 0, dbuff);

          break;
        }
        case 3:
        {
#if 0
          pca9555_reg_rd(test_dev,
                         PCA9554_REG_CONFIG_PORT,
                         &data);

          pca9555_reg_wr(test_dev,
                         PCA9554_REG_CONFIG_PORT,
                         0xF8);

          pca9555_reg_rd(test_dev,
                         PCA9554_REG_CONFIG_PORT,
                         &data2);
#endif

#if 1
          pca9555_reg_rd(test_dev,
                         0x01,
                         &data);

          pca9555_reg_wr(test_dev,
                         0x01,
                         0x35);

          pca9555_reg_rd(test_dev,
                         0x01,
                         &data2);
#endif

#if 0
          data = ReadPCA9554(TEST_PCA9554_ADDRESS, 0x01);
          WritePCA9554(TEST_PCA9554_ADDRESS, 0x34);
          data2 = ReadPCA9554(TEST_PCA9554_ADDRESS, 0x01);
#endif

          sprintf(dbuff, "%02X %02X", data, data2);
          fpd_write_string(1, 0, dbuff);

          break;
        }
        case 4:
        {
          initPCM1792(DAC_ID_LEFT);

          fpd_write_string(1, 0, "        ");

          break;
        }
        case 5:
        {
          dacbio_left_assert_rst();

          fpd_write_string(1, 0, "        ");

          break;
        }
        case 6:
        {
          dacbio_left_deassert_rst();

          fpd_write_string(1, 0, "        ");

          break;
        }
        case 7:
        {
          pca9555_reg_rd(general_ctrl_dev,
                         PCA9555_REG_CONFIG_PORT_0,
                         &data);

          sprintf(dbuff, "%02X    ", data);
          fpd_write_string(1, 0, dbuff);

          break;
        }
        case 8:
        {
          pca9555_reg_rd(dac_dev,
                         DAC_REG_23,
                         &data);

          sprintf(dbuff, "%02X    ", data);
          fpd_write_string(1, 0, dbuff);

          break;
        }
        case 0:
        {
          fpd_write_string(1, 0, "        ");

          break;
        }
        case 9:
        {
          goto end;
        }
      }
    }
    prev_cnt = cnt;
  }

end:
  return;
}


/*****************************************************************************/


// DAC umute
//


void test_17(void)
{
  unsigned char cmd;
  char dbuff[10];
  int cnt = 0;
  int prev_cnt = -1;

  pca9555_dev_type  test_dev_var;
  pca9555_dev_type *test_dev = &test_dev_var;

  pca9555_dev_type  dac_left_dev_var;
  pca9555_dev_type *dac_left_dev = &dac_left_dev_var;
  pca9555_dev_type  dac_right_dev_var;
  pca9555_dev_type *dac_right_dev = &dac_right_dev_var;
  pca9555_dev_type  general_ctrl_dev_var;
  pca9555_dev_type *general_ctrl_dev = &general_ctrl_dev_var;
  pca9555_dev_type  dac_dev_var;
  pca9555_dev_type *dac_dev = &dac_dev_var;
  unsigned char data;
  unsigned char data2;
  unsigned char data3;
  unsigned char data4;

  test_dev->addr = TEST_PCA9554_ADDRESS;

  dac_left_dev->addr = DAC_BOARD_LEFT_PCA9554_ADDRESS;
  dac_right_dev->addr = DAC_BOARD_RIGHT_PCA9554_ADDRESS;
  general_ctrl_dev->addr = DSP_BOARD_GENERAL_CTRL_PCA9555_ADDRESS;
  dac_dev->addr = DAC_LEFT_I2C_ADDR;

  fpd_write_string(0, 0, (char*)__FUNCTION__);
  volatileConfig.state = STATE_EXTERNAL_INPUT;

//  delay_ms(500);
//  dacbio_init();

  while (1)
  {
    cmd = getCmd();

    if (cmd == 0x17)
      if (cnt > 0)
        cnt--;
    if (cmd == 0x18)
      if (cnt < 9)
        cnt++;

    if (cmd != 0x00)
    {
      sprintf(dbuff, "%02X %i ", cmd, cnt);
      fpd_write_string(2, 0, dbuff);
    }

    if (prev_cnt != cnt)
    {
      switch (cnt)
      {
        case 1:
        {
          dacbio_left_assert_rst();
          delay_ms(100);
          dacbio_left_deassert_rst();
          delay_ms(100);

          fpd_write_string(1, 0, "        ");

          break;
        }
        case 2:
        {
          pca9555_reg_rd(dac_dev,
//                         DAC_REG_23,
                         DAC_REG_21,
                         &data);
          pca9555_reg_rd(dac_dev,
                         DAC_REG_18,
                         &data2);
          pca9555_reg_rd(dac_dev,
                         DAC_REG_19,
                         &data3);
          pca9555_reg_rd(dac_dev,
                         DAC_REG_20,
                         &data4);

          sprintf(dbuff, "%02X%02X%02X%02X", data, data2, data3, data4);
          fpd_write_string(1, 0, dbuff);

          break;
        }
        case 3:
        {
          initPCM1792(DAC_ID_LEFT);

          fpd_write_string(1, 0, "        ");

          break;
        }
        case 4:
        {
          dacbio_left_deassert_mcu_mute();

          dacbio_left_deassert_mcu_unmute();
          dacbio_left_assert_mcu_unmute();

          dacbio_assert_main_mute();

          fpd_write_string(1, 0, "        ");

         break;
        }

        case 5:
        {
          dacbio_deassert_main_mute();

          fpd_write_string(1, 0, "        ");

         break;
        }

        case 6:
        {
          dacbio_left_deassert_mcu_unmute();
          dacbio_left_assert_mcu_unmute();

          fpd_write_string(1, 0, "UNMUTE  ");

         break;
        }

        case 7:
        {
          dacbio_left_assert_mcu_mute();
          dacbio_left_deassert_mcu_mute();

          fpd_write_string(1, 0, "MUTE    ");

         break;
        }


/*
        case 5:
        {
#if 1
          dacbio_left_assert_mcu_mute();
          fpd_write_string(1, 0, "        ");
#else
          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         &data);
          data = data & ~0x02;
          pca9555_reg_wr(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         data);
#endif

          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         &data);
          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_CONFIG_PORT,
                         &data2);
          sprintf(dbuff, "%02X %02X", data, data2);
          fpd_write_string(1, 0, dbuff);

          break;
        }
        case 6:
        {
#if 1
          dacbio_left_deassert_mcu_mute();
          fpd_write_string(1, 0, "        ");
#else
          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         &data);
          data = data | 0x02;
          pca9555_reg_wr(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         data);
#endif

          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         &data);
          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_CONFIG_PORT,
                         &data2);
          sprintf(dbuff, "%02X %02X", data, data2);
          fpd_write_string(1, 0, dbuff);

          break;
        }
        case 7:
        {
#if 1
          dacbio_left_assert_mcu_unmute();
          fpd_write_string(1, 0, "        ");
#else
          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         &data);
          data = data & ~0x04;
          pca9555_reg_wr(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         data);
#endif

          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         &data);
          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_CONFIG_PORT,
                         &data2);
          sprintf(dbuff, "%02X %02X", data, data2);
          fpd_write_string(1, 0, dbuff);

          break;
        }
        case 8:
        {
#if 1
          dacbio_left_deassert_mcu_unmute();
          fpd_write_string(1, 0, "        ");
#else
          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         &data);
          data = data | 0x04;
          pca9555_reg_wr(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         data);
#endif

          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_OUTPUT_PORT,
                         &data);
          pca9555_reg_rd(dac_left_dev,
                         PCA9554_REG_CONFIG_PORT,
                         &data2);
          sprintf(dbuff, "%02X %02X", data, data2);
          fpd_write_string(1, 0, dbuff);

          break;
        }
*/
        case 9:
        {
/*
          // Reset DAC
          dacbio_left_assert_rst();
          delay_ms(100);
          dacbio_left_deassert_rst();
          delay_ms(100);

          // Configure DAC
          initPCM1792(DAC_ID_LEFT);

          // Unmute
          dacbio_left_deassert_mcu_mute();

          dacbio_left_deassert_mcu_unmute();
          dacbio_left_assert_mcu_unmute();

          dacbio_deassert_main_mute();
*/

          // go !
          fpd_write_string(1, 0, "boot!   ");
          delay_ms(1000);

          goto end;
        }
        default:
        {
          fpd_write_string(1, 0, "        ");

          break;
        }
      }
    }
    prev_cnt = cnt;
  }

end:
  return;
}


/*****************************************************************************/


void flashReadData(dword address, byte* buffer, word len);
byte flashReadStatus(void);
void flashSectorErase(dword address);
void flashWriteData(dword address, byte* buffer, word len);
void flashReadID(byte* buffer);

UINT8 mcp23s17_read( UINT8 reg);
extern T_FlashStatus FlashStatus;

void test_18(void)
{
  unsigned char cmd;
  char dbuff[10];
  int cnt = 0;
  int prev_cnt = -1;
  unsigned char test_buff[4] = { 1, 2, 3, 4 };
  unsigned char read_buff[4];
  int buff_size = 4;

  fpd_write_string(0, 0, (char*)__FUNCTION__);
  volatileConfig.state = STATE_EXTERNAL_INPUT;

  while (1)
  {
    cmd = getCmd();

    if (cmd == 0x17)
      if (cnt > 0)
        cnt--;
    if (cmd == 0x18)
      if (cnt < 9)
        cnt++;

    if (cmd != 0x00)
    {
      sprintf(dbuff, "%02X %i ", cmd, cnt);
      fpd_write_string(2, 0, dbuff);
    }

    if (cnt == 0)
    {
      flashReadID((byte*)read_buff);
    }

    if (prev_cnt != cnt)
    {
      switch (cnt)
      {
        case 1:
        {
          flashReadID((byte*)read_buff);

          sprintf(dbuff, "Id%02X%02X%02X", 
                  read_buff[0], read_buff[1], read_buff[2]);
          fpd_write_string(1, 0, dbuff);

          break;
        }
        case 2:
        {
          {
            int i;
            for (i=0 ; i<4 ; i++)
              read_buff[i] = 0x33;
          }

          flashReadData(FLASH_SECTOR, (byte*)read_buff, (word)buff_size);
          
          sprintf(dbuff, "%02X%02X%02X%02X", 
                  read_buff[0], read_buff[1], read_buff[2], read_buff[3]);
          fpd_write_string(1, 0, dbuff);
          
          break;
        }

        case 3:
        {
          {
            int i;
            for (i=0 ; i<4 ; i++)
              test_buff[i] += 1;
          }

          unsigned char dSize = 4;
          unsigned char * ptr = test_buff;
          T_FlashStatus FlashStatus;

          // Erase sector - flash memory !
          flashSectorErase(FLASH_SECTOR);
	
          // Wait end of operation.
          do
          {
            FlashStatus.value = flashReadStatus();
          } while (FlashStatus.wip == 1);
	
          // Write data to external flash memory.
          flashWriteData(FLASH_SECTOR, ptr, dSize);
	
          // Wait end of operation.
          do
          {
            FlashStatus.value = flashReadStatus();
          } while (FlashStatus.wip == 1);

          fpd_write_string(1, 0, "write!  ");
          delay_ms(1000);
          sprintf(dbuff, "%02X%02X%02X%02X", 
                  test_buff[0], test_buff[1], test_buff[2], test_buff[3]);
          fpd_write_string(1, 0, dbuff);

          break;
        }

        case 4:
        {
          fpd_write_string(1, 0, "wr cfg  ");
          
          writeConfigToFlash();
          
          fpd_write_string(1, 0, "done    ");
          delay_ms(1000);
          fpd_write_string(1, 0, "        ");
          
          break;
        }

        case 9:
        {
          // go !
          fpd_write_string(1, 0, "boot!   ");
          delay_ms(1000);
          
          goto end;
        }

        default:
        {
          fpd_write_string(1, 0, "        ");
          
          break;
        }
      }
      prev_cnt = cnt;
    }
  }

end:
  return;
}


/*****************************************************************************/


// keypad
//


void test_19(void)
{
  char dbuff[10];
	keyStruct keypad;
  int i;
  unsigned char cmd;
  unsigned char key_id = 0x00;

  fpd_write_string(0, 0, (char*)__FUNCTION__);
  volatileConfig.state = STATE_EXTERNAL_INPUT;

  initKeypad();

  while (1)
  {
    for (i=0 ; i<10 ; i++)
    {
      sprintf(dbuff, "%i %i%i%i %02X", i,
              get_key1(), get_key2(), get_key3(), keypad.id);
//      dbuff[0] = '0' + i;
      fpd_write_string(2, 0, dbuff);

//      readKeypad(&keypad);
      cmd = getCmd();

      if (cmd != 0x00)
      {
        sprintf(dbuff, "%02X", cmd);
        fpd_write_string(1, 0, dbuff);
      }

      {
        unsigned char currentPress;

        if (keypad.id != NO_KEY)
        {
          key_id = keypad.id;
        }

        currentPress = 
          (keypad_get_key3() << 3) | 
          (keypad_get_key2() << 2) | 
          (keypad_get_key1() << 1) | 
          (encoder_get_A_push_button() << 0) ;
        currentPress |= 0xF0;

        sprintf(dbuff, "%02X %02X", currentPress, key_id);
        fpd_write_string(0, 8, dbuff);

      }

/*
      sprintf(dbuff, "%02X %02X %02X", 
              keypad.id, keypad.count, cmd);
      fpd_write_string(1, 0, dbuff);
*/

//      delay_ms(300);
    }
  }

  return ;
}


/*****************************************************************************/


// encoder
//


void test_20(void)
{
  char dbuff[10];
  encoderStruct encoder;
  unsigned int	currentValue;;

  fpd_write_string(0, 0, (char*)__FUNCTION__);
  volatileConfig.state = STATE_EXTERNAL_INPUT;
  volatileConfig.setVolume == ON;

  initEncoderA();

  while (1)
  {
    readEncoderKeyA(&encoder);

    currentValue = (encoder_get_rot_A_B() << 9) | (encoder_get_rot_A_A() << 8);

    sprintf(dbuff, "%02X %04X ", encoder.direction, currentValue);
    fpd_write_string(1, 0, dbuff);
  }

  return ;
}


/*****************************************************************************/
