
/*
 * File:         Util
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/05/15
 * Description:  Toolbox
 *
 * History :
 *
 *   2015/05/15  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __UTIL_H
#define __UTIL_H

/*****************************************************************************/


void iToStr(int val, char* buff);


/*****************************************************************************/

#endif // __UTIL_H



