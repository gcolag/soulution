
/*
 * File:         cpld_dsp_board
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/18
 * Description:  CPLD Dsp Board I/O
 *
 * History :
 *
 *   2015/04/18  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __CDB_H
#define	__CDB_H

/*****************************************************************************/


#include "GenericTypeDefs.h"


/*****************************************************************************/


void  cdb_write(UINT8 data);
UINT8 cdb_read(void);

void cdb_bit_set(unsigned char bit);
void cdb_bit_clr(unsigned char bit);

int  cdb_get_bit(unsigned char bit);


/*****************************************************************************/

#endif	/* __CDB_H */
