
/*
 * File:         periph_spi
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  SPI peripheral custom library
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __PERIPH_SPI_H__
#define __PERIPH_SPI_H__

/** I M P O R T A N T ********************************************************/
// You must add the "USE_AND_OR" macro definition to your project build
// options under MPLAB C30 tab.
// Alternatively you can uncomment the line below.
/*****************************************************************************/
//#define USE_AND_OR


/** I N C L U D E S **********************************************************/
#include <spi.h>


/** D E C L A R A T I O N S **************************************************/
// Send one byte of data and receive one back at the same time.
unsigned char mWriteSPI1( unsigned char i );

// Send one dummy byte of data and receive one back at the same time.
unsigned char mReadSPI1( void );

void mOpenSPI1(unsigned int config1, unsigned int config2, unsigned int stat);

void mCloseSPI1();

void mConfigIntSPI1(unsigned int config);

// Send one byte of data and receive one back at the same time.
unsigned char mWriteSPI2( unsigned char i );

// Send one dummy byte of data and receive one back at the same time.
unsigned char mReadSPI2( void );

void mOpenSPI2(unsigned int config1, unsigned int config2, unsigned int stat);

void mCloseSPI2();

void mConfigIntSPI2(unsigned int config);


/*****************************************************************************/


//									CPOL	CPHA		CKP		CKE
//#define MODE0		//		0		0		0			0		1
//#define MODE1		//		1		0		1			0		0
//#define MODE2		//		2		1		0			1		1
//#define MODE3		//		3		1		1			1		0

#define MODE0		0
#define MODE1		1
#define MODE2		2
#define MODE3		3


void  spi1_open( UINT8 mode );
UINT8 spi1_write( UINT8 data );
void  spi1_close( void );

void  spi2_open( UINT8 mode );
UINT8 spi2_write( UINT8 data );
void  spi2_close( void );

void  spi3_open( UINT8 mode );
UINT8 spi3_write( UINT8 data );
void  spi3_close( void );


/*****************************************************************************/

#endif // __PERIPH_SPI_H__


