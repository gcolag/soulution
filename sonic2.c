
/*
 * File:         sonic2
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Sonic 2 module driver
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:
 *
 */

/** I N C L U D E S **********************************************************/


#include <p24fxxxx.h>
#include <PPS.h>

#include "periph_spi.h"
#include "io.h"
#include "dbio.h"

#include "sonic2.h"


/*****************************************************************************/


// HAL
//

static te_volume_ctrl_t regCtrl = OFF_CTRL;

static UINT8 spi_read_write(UINT8 dataOut)
{
  return spi2_write(dataOut);
}


static void assert_cs(void)
{
  // HACK : Disable ChangeNotification interrupts.
  //
  // ChangeNotification interrupts will trig I2C com when
  // the MUTE signal toggles, potentially in conflict with the
  // I2C access below. Protect this section.
  //
  // Note : waiting for a better implementation (I2C ressource
  //        sharing handling)
  //
  io_disable_cn_int();

  S8_nCS = 0;
}


static void deassert_cs(void)
{
  S8_nCS = 1;

  // HACK : Enable ChangeNotification interrupts.
  //
  io_enable_cn_int();
}


static void EnableSonic2(void)
{
  dbio_deassert_s8_reset();
}


static void ResetSonic2(void)
{
  dbio_assert_s8_reset();
}


/** V A R I A B L E S ********************************************************/
// Input control register.
static struct
{
	unsigned char format : 2; // input format (READ / WRITE).
	unsigned char XFS    : 3; // Input frequency sampling (READ only).
	unsigned char        : 3; // Not used.
} InputCtrlReg;

// Process control register.
static struct
{
	unsigned char PHI    : 1; // Phase inversion (R/W).
	unsigned char mute   : 1; // Mute (R/W).
	unsigned char        : 6; // Not used.
} ProcsCtrlReg;

// Level control left.
static unsigned char leftLevelReg;

// Level control right.
static unsigned char rightLevelReg;


static void Sonic2WriteSPI (unsigned char addr, unsigned char data);
static unsigned char Sonic2ReadSPI (unsigned char addr);


/** F U N C T I O N S ********************************************************/


/*****************************************************************************
 * initSonic2
 *
 * Overview		:	Initialize the Sonic 2 module in software mode.
 *
 * Returns 	 0	:	Initialisation done with no error
 *			-1	:	Wrong device ID
 *
 *****************************************************************************/
char initSonic2 (void)
{
	unsigned char chk;

	EnableSonic2();		// Release reset.
//	delay_ms(500);		// Wait for Sonic2 to wake up from reset.
	delay_ms(1200);		// Wait for Sonic2/Sonic8 to wake up from reset.

	// ID check.
	chk = getSonic2ID();
	if ((chk != SONIC2_PROD_ID) && (chk != SONIC8_PROD_ID))
		return -1;

	// Configuration of internal registers.
	setSonic2InputFormat(0);	// I2S.
	setSonic2Phi(OFF);			// No phase inversion.

	return 0;
}

/*****************************************************************************
 * Sonic2WriteSPI
 *
 * Overview			:	Write a data byte into a register from the Sonic 2
 *
 * Params	addr	:	Address of the register to write
 *			data	:	New value to write
 *
 *****************************************************************************/
void Sonic2WriteSPI (unsigned char addr, unsigned char data)
{
	assert_cs();
	spi_read_write(addr | SONIC_SPI_WR);
	delay_50us();
	spi_read_write(data);
	deassert_cs();
}

/*****************************************************************************
 * Sonic2ReadSPI
 *
 * Overview			:	Read a register value from the Sonic 2
 *
 * Param	addr	:	Address of the register to read
 *
 * Returns			:	Register value
 *
 *****************************************************************************/
unsigned char Sonic2ReadSPI (unsigned char addr)
{
	unsigned char rxBuffer;

	assert_cs();
	spi_read_write(addr | SONIC_SPI_RD);
	delay_50us();
	rxBuffer = spi_read_write(0x00);
	deassert_cs();

	return rxBuffer;
}

/*****************************************************************************
 * setSonic2InputFormat
 *
 * Overview			:	Write a data byte into a register from the Sonic 2
 *
 * Params	format	:	0b000xxx00 = I2S
 *						0b000xxx11 = DSD
 *
 *****************************************************************************/
void setSonic2InputFormat (unsigned char format)
{
	InputCtrlReg.format = format;
	Sonic2WriteSPI(REG_INPUT_CTRL, *((unsigned char*)(&InputCtrlReg)));
}

/*****************************************************************************
 * setSonic2Phi
 *
 * Overview			:	Set the phase inversion
 *
 * Params	state	:	OFF = No inversion
 *						ON  = Phase inverted
 *
 *****************************************************************************/
void setSonic2Phi (T_ONorOFF state)
{
	ProcsCtrlReg.PHI = state;
	Sonic2WriteSPI(REG_PROCS_CTRL, *((unsigned char*)(&ProcsCtrlReg)));
	// Pb?
}

/*****************************************************************************
 * setSonic2Mute
 *
 * Overview			:	Mute the Sonic 2
 *
 * Params	state	:	OFF = No mute
 *						ON  = Mute
 *
 *****************************************************************************/
void setSonic2Mute (T_ONorOFF state)
{
  unsigned char procs_ctrl;

	ProcsCtrlReg.mute = state;

	procs_ctrl = Sonic2ReadSPI(REG_PROCS_CTRL);
  if (state == ON)
    procs_ctrl |= 0x02;
  else
    procs_ctrl &= ~0x02;
  Sonic2WriteSPI(REG_PROCS_CTRL, procs_ctrl);
}

/*****************************************************************************
 * setSonic2LLevel
 *
 * Overview			:	Set left channel volume on Sonic 2
 *
 * Params	level	:	0(0dB)..1(-0.5dB)..255(mute)
 *
 *****************************************************************************/
void setSonic2LLevel (unsigned char level)
{
   if (regCtrl == STD_CTRL) {

      leftLevelReg = level;
	   Sonic2WriteSPI(REG_LLEVL_CTRL, leftLevelReg);
   }
   else if (regCtrl == LEEDH_CTRL) {

      leftLevelReg = level >> 1;
	   Sonic2WriteSPI(REG_LLEVL2_CTRL, leftLevelReg );
   }
   else {

   }
}


/*****************************************************************************
 * setSonic2RLevel
 *
 * Overview			:	Set right channel volume on Sonic 2
 *
 * Params	level	:	0(0dB)..1(-0.5dB)..255(mute)
 *
 *****************************************************************************/
void setSonic2RLevel (unsigned char level)
{
   if (regCtrl == STD_CTRL) {
   rightLevelReg = level;
      Sonic2WriteSPI(REG_RLEVL_CTRL, rightLevelReg);
   }
   else if (regCtrl == LEEDH_CTRL) {

      rightLevelReg = level >> 1;
      Sonic2WriteSPI(REG_RLEVL2_CTRL, rightLevelReg );
   }
   else {

   }
}

/*****************************************************************************
 * setSonic2VolumeReg
 *
 * Overview			:	Set the right registers set for volume on Sonic 2
 *
 * Params	register set	: STD_CTRL for standard, or LEEDH_CTRL
 *
 *****************************************************************************/
void setSonic2VolumeReg(te_volume_ctrl_t ctrl , unsigned char init)
{

   if (ctrl == STD_CTRL) {
      if ((regCtrl == LEEDH_CTRL) || (init!= 0)) {

         regCtrl = LEEDH_CTRL;
         setSonic2LLevel(160);
         delay_50us();
         setSonic2RLevel(160);
         delay_50us();

         regCtrl = STD_CTRL;
         setSonic2LLevel(160);
         delay_50us();
         setSonic2RLevel(160);
         delay_50us();

         regCtrl = LEEDH_CTRL;
         setSonic2LLevel(0);
         delay_50us();
         setSonic2RLevel(0);
         delay_50us();

         regCtrl = STD_CTRL;

      }
   }
   else if (ctrl == LEEDH_CTRL) {
      if ((regCtrl == STD_CTRL) || (init != 0)) {

         regCtrl = STD_CTRL;
         setSonic2LLevel(160);
         delay_50us();
         setSonic2RLevel(160);
         delay_50us();

         regCtrl = LEEDH_CTRL;
         setSonic2LLevel(160);
         delay_50us();
         setSonic2RLevel(160);
         delay_50us();

         regCtrl = STD_CTRL;
         setSonic2LLevel(0);
         delay_50us();
         setSonic2RLevel(0);
         delay_50us();

         regCtrl = LEEDH_CTRL;

      }
   }
}
/*****************************************************************************
 * setUpsamplingFilters
 *
 * Overview			:	Select upsampling filter
 *
 * Params	level	:	filter index 0..7
 *
 *****************************************************************************/
void setUpsamplingFilters (unsigned char filters)
{
	Sonic2WriteSPI(REG_FILTERS_CTRL, filters);
}

unsigned char getsetUpsamplingFilters(void)
{
	return Sonic2ReadSPI(REG_FILTERS_CTRL);
}


/*****************************************************************************
 * setDcOffset
 *
 * Overview			:	Select DC offset
 *
 * Params	level	:	DC offset 0..255
 *
 *****************************************************************************/
void setDcOffsetLeft (unsigned char filters)
{
	Sonic2WriteSPI(REG_DC_OFFSET_LEFT_CTRL, filters);
}

void setDcOffsetRight (unsigned char filters)
{
	Sonic2WriteSPI(REG_DC_OFFSET_RIGHT_CTRL, filters);
}

unsigned char getDcOffsetLeft(void)
{
	return Sonic2ReadSPI(REG_DC_OFFSET_LEFT_CTRL);
}

unsigned char getDcOffsetRight(void)
{
	return Sonic2ReadSPI(REG_DC_OFFSET_RIGHT_CTRL);
}

/*****************************************************************************
 * setPhaseEq
 *
 * Overview			: Enable/Disable Phase Eq
 *
 * Params	level	:	Enable(1)/Disable(0) 0..1
 *
 *****************************************************************************/
void setPhaseEq(unsigned char phaseEq)
{
	Sonic2WriteSPI(REG_PHASE_EQ, phaseEq);
}

unsigned char getPhaseEq(void)
{
	return Sonic2ReadSPI(REG_PHASE_EQ);
}

/*****************************************************************************
 * getSonic2Version
 *
 * Overview		:	Get the Sonic 2 version
 *
 * Retuns		:	[4MSB] -> Major version
 *					[4LSB] -> Minor version
 *
 *****************************************************************************/
unsigned char getSonic2Version (void)
{
	return Sonic2ReadSPI(REG_SWR_REV);
}

unsigned char getSonic2VersionMinor(void)
{
	return Sonic2ReadSPI(REG_SWR_REV_MINOR);
}

/*****************************************************************************
 * getSonic2ID
 *
 * Overview		:	Get the Sonic 2 ID
 *
 * Retuns		:	0x04
 *
 *****************************************************************************/
unsigned char getSonic2ID (void)
{
	return Sonic2ReadSPI(REG_PRD_ID);
}

unsigned char getSonic2SID(void)
{
	return Sonic2ReadSPI(REG_PRD_SID);
}

/*****************************************************************************
 * getSonic2InputFormat
 *
 * Overview		:	Get the input data format
 *
 * Retuns		:	0b000xxx00 = I2S
 *					0b000xxx11 = DSD
 *
 *****************************************************************************/
unsigned char getSonic2InputFormat (void)
{
	return ((Sonic2ReadSPI(REG_INPUT_CTRL) & 0x03));
}

/*****************************************************************************
 * getSonic2InputFs
 *
 * Overview		:	Get the input sampling frequency
 *
 * Retuns		:	0b000000xx = Unlocked
 *					0b000001xx = 32 kHz
 *					0b000010xx = 44.1 kHz
 *					0b000011xx = 48 kHz
 *					0b000100xx = 88.2 kHz
 *					0b000101xx = 96 kHz
 *					0b000110xx = 176.4 kHz
 *					0b000111xx = 192 kHz
 *					0b001000xx = 352.8 kHz
 *					0b001001xx = 384 kHz
 *
 *****************************************************************************/
unsigned char getSonic2InputFs (void)
{
	//return ((Sonic2ReadSPI(REG_INPUT_CTRL) & 0x1C) >> 2);
	return (Sonic2ReadSPI(REG_INPUT_CTRL));
	// Pb?
}

/*****************************************************************************
 * getSonic2RLevel
 *
 * Overview		:	Get the Sonic 2 left channel volume
 *
 * Retuns		:	0(0dB)..1(-0.5dB)..255(mute)
 *
 *****************************************************************************/
unsigned char getSonic2RLevel (void)
{
   if (regCtrl == STD_CTRL) {

      return Sonic2ReadSPI(REG_RLEVL_CTRL);
   }
   else if (regCtrl == LEEDH_CTRL) {

      return (Sonic2ReadSPI(REG_RLEVL2_CTRL) << 1);
   }
   else {

      return 0;
   }
}

/*****************************************************************************
 * getSonic2LLevel
 *
 * Overview		:	Get the Sonic 2 right channel volume
 *
 * Retuns		:	0(0dB)..1(-0.5dB)..255(mute)
 *
 *****************************************************************************/
unsigned char getSonic2LLevel (void)
{
   if (regCtrl == STD_CTRL) {

	   return Sonic2ReadSPI(REG_LLEVL_CTRL);
   }
   else if (regCtrl == LEEDH_CTRL) {

	   return (Sonic2ReadSPI(REG_LLEVL2_CTRL) << 1);
   }
   else {

      return 0;
   }
}


unsigned char getsetUpsamplingFilters(void);
