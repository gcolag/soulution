
/*
 * File:         wrap_disp
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Display wrapper
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include "io.h"
#include "mcp23s17.h"
#include "fp_display.h"
#include "SCF5742.h"


/*****************************************************************************/


static char line1_buff[13] = "            ";
static char line2_buff[9]  = "        ";
static char line3_buff[9]  = "        ";
static int  line1_cur_pos  = 0;
static int  line2_cur_pos  = 0;
static int  line3_cur_pos  = 0;


/*****************************************************************************/


void dispInit(void)
{
  deassert_fp_reset();
  mcp23s17_init();
  fpd_init();
}


void dispBrightness(char Brightness) 
{
  UINT8 fpt_brightness;

  switch (Brightness)
  {
    case BRIGHTNESS_LOW:
      fpt_brightness = 6;
      break;
    case BRIGHTNESS_MEDIUM:
      fpt_brightness = 3;
      break;
    default:
    case BRIGHTNESS_HIGH:
      fpt_brightness = 0;
      break;
  }

  fpt_set_brightness(fpt_brightness);
}


void dispClearAll(void)
{
  fpd_clear();
}


void dispClearLine1(void)
{
  line1_buff[0] = 0;
  fpd_write_string(0, 0, "                ");
}


void dispPutLine1(const char *Text)
{
  line1_cur_pos = 4;
  if (strcmp(Text, line1_buff))
  {
    fpd_write_string(0, line1_cur_pos, (char*)Text);
    strncpy(line1_buff, Text, 12);
    line1_buff[12] = 0;
  }
}


void dispClearLine2(void)
{
  line2_buff[0] = 0;
  fpd_write_string(1, 0, "        ");
}


void dispPutLine2(const char *Text)
{
  line2_cur_pos = 0;
  if (strcmp(Text, line2_buff))
  {
    fpd_write_string(1, line2_cur_pos, (char*)Text);
    strncpy(line2_buff, Text, 8);
    line2_buff[8] = 0;
  }
}


void dispClearLine3(void)
{
  line3_buff[0] = 0;
  fpd_write_string(2, 0, "        ");
}


void dispPutLine3(const char *Text)
{
  line3_cur_pos = 0;
  if (strcmp(Text, line3_buff))
  {
    fpd_write_string(2, line3_cur_pos, (char*)Text);
    strncpy(line3_buff, Text, 8);
    line3_buff[8] = 0;
  }
}


/*****************************************************************************/

