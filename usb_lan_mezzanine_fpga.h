
/*
 * File:         usb_lan_mezzanine_fpga
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  control I/O exandder in fpga of usb_lan mezzanine board
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************
* ReadUsbLanFpga
*
* overview : Read on SPI
*
*****************************************************************************/
void ReadUsbLanFpga(unsigned char* data);


/*****************************************************************************
* WriteUsbLanFpga
*
* overview : Write on SPI
*
*****************************************************************************/
void WriteUsbLanFpga(unsigned char data);


/*****************************************************************************
* UsbLanFpgaGetDsdFlag
*
* overview : Return DSD Flag value 
*            1 : DSD
*            0 : PCM
*
*****************************************************************************/
int UsbLanFpgaGetDsdFlag(void);


/* END OF FILE ***************************************************************/
