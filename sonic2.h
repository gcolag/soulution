/*
 * File:         sonic2
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Sonic 2 module driver
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:
 *
 */

/*****************************************************************************/

#ifndef	__SONIC2
#define	__SONIC2

/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>

#include "variables.h"
#include "periph_timer.h"
#include "config_boards.h"


/** D E F I N E S ************************************************************/
//		MD_RST				// S2_RST# - see config_boards.h
#define MD_CS				LATEbits.LATE9
//		MD_FLAG0			// Right justified - Tied to +3.3V
//		MD_FLAG1			// DSD/PCM# - see config_boards.h
//		MD_FLAG2			// S2 Master - Tied to GND
//		MD_FLAG3			// S2_VALID# - see config_boards.h
//		MD_INT				// N.C.

#define	SONIC2_PROD_ID		0x04
#define	SONIC8_PROD_ID		0x05

#define	SONIC_SPI_WR		0x10
#define	SONIC_SPI_RD		0x00

#define REG_INPUT_CTRL            0x00
#define REG_FILTERS_CTRL          0x01
#define REG_PROCS_CTRL            0x02
#define REG_LLEVL_CTRL            0x03
#define REG_RLEVL_CTRL            0x04
#define REG_SCRATCH               0x05
#define REG_SWR_REV               0x06
#define REG_PRD_ID                0x07
#define REG_SWR_REV_MINOR         0x08
#define REG_PRD_SID               0x09

#define REG_LLEVL2_CTRL           0x20
#define REG_RLEVL2_CTRL           0x21

#define REG_SWR_REV_MINOR         0x08
#define REG_SUB_PRD_ID            0x09
#define REG_PHASE_EQ              0x0A
#define REG_DC_OFFSET_LEFT_CTRL   0x0B
#define REG_DC_OFFSET_RIGHT_CTRL  0x0C

#define APP_SPI_REG_INPUT_CONTROL               0x00
#define APP_SPI_REG_FILTERS                     0x01
#define APP_SPI_REG_PROCESS_CONTROL             0x02
#define APP_SPI_REG_LEVEL_CONTROL_LEFT          0x03
#define APP_SPI_REG_LEVEL_CONTROL_RIGHT         0x04
#define APP_SPI_REG_SCRATCH                     0x05
#define APP_SPI_REG_REVISION                    0x06
#define APP_SPI_REG_PRODUCT_ID                  0x07

#define APP_SPI_REG_REVISION_MINOR              0x08
#define APP_SPI_REG_SUB_PRODUCT_ID              0x09
#define APP_SPI_REG_PHASE_EQ                    0x0A
#define APP_SPI_REG_DC_OFFSET_LEFT              0x0B
#define APP_SPI_REG_DC_OFFSET_RIGHT             0x0C

#define SONIC2_LEVEL_MUTE	255

#define SONIC2_UP_SAMPLING_FILTERS_MAX  7
#define SONIC2_UP_SAMPLING_FILTERS_MIN  0

#define SONIC2_DC_OFFSET_MAX   20  // +10mV with 0.5mV steps
#define SONIC2_DC_OFFSET_MIN  -20  // -10mV with 0.5mV steps

/** P U B L I C  P R O T O T Y P E S *****************************************/
char initSonic2(void);
void setSonic2InputFormat(unsigned char format);
void setSonic2Phi(T_ONorOFF state);
void setSonic2Mute(T_ONorOFF state);
void setPhaseEq(unsigned char phaseEq);
void setSonic2LLevel(unsigned char level);
void setSonic2RLevel(unsigned char level);
void setUpsamplingFilters(unsigned char filter);
void setDcOffsetLeft(unsigned char filters);
void setDcOffsetRight(unsigned char filters);
void setSonic2VolumeReg(te_volume_ctrl_t ctrl, unsigned char init);

unsigned char getSonic2Version(void);
unsigned char getSonic2VersionMinor(void);
unsigned char getSonic2ID(void);
unsigned char getSonic2SID (void);
unsigned char getSonic2InputFormat(void);
unsigned char getSonic2InputFs(void);
unsigned char getSonic2RLevel(void);
unsigned char getSonic2LLevel(void);
unsigned char getPhaseEq(void);
unsigned char getsetUpsamplingFilters(void);
unsigned char getDcOffsetLeft(void);
unsigned char getDcOffsetRight(void);


/** E N D   O F   F I L E ****************************************************/
#endif
