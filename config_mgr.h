
/*
 * File:         config_mgr
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Init, running & standby management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:
 *
 */

/*****************************************************************************/

#ifndef __CONFIG_MGR_H
#define __CONFIG_MGR_H

/** I M P O R T A N T ********************************************************/
// You must add the "USE_AND_OR" macro definition to your project build
// options under MPLAB C30 tab.
// Alternatively you can uncomment the line below.
/*****************************************************************************/
//#define USE_AND_OR


/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <stdlib.h>
#include <ports.h>
#include <i2c.h>

#include "variables.h"
#include "periph_timer.h"
#include "periph_spi.h"
#include "pre_amp.h"
#include "cmd_mgr.h"
#include "keypad.h"
#include "RC5.h"
#include "menus_setup.h"
#include "SCF5742.h"
#include "display.h"
#include "encoder.h"
#include "eeprom.h"
#include "io.h"
#include "digital_in.h"
#include "config_boards.h"
#include "rs232.h"


/** D E F I N E S ************************************************************/
// Software version
#define SOFT_VERSION					0x0202
//                                "............"
#define SOFT_VERSION_STRING				"   v2.14.2.1"

// volatileConfig.state
#define	STATE_STANDBY					(unsigned char)0x00
#define	STATE_INIT						(unsigned char)0x01
#define	STATE_CD_PLAYER				(unsigned char)0x02
#define  STATE_EXTERNAL_INPUT			(unsigned char)0x03
#define	STATE_SETUP						(unsigned char)0x04
#define	STATE_OVERCURRENT				(unsigned char)0x05

// Start Mode
#define STARTMODE_NORM					(unsigned char)0x00
#define STARTMODE_LINK					(unsigned char)0x01
#define STARTMODE_AUTO					(unsigned char)0x02

// Repeat Mode
#define	CD_REPEAT_OFF					(unsigned char)0x00
#define	CD_REPEAT_TRACK					(unsigned char)0x01
#define	CD_REPEAT_DISC					(unsigned char)0x02
#define CD_REPEAT_RANDOM				(unsigned char)0x03

// Phase Polarity Mode
#define PHASEPOLARITY_0					(unsigned char)0x00
#define PHASEPOLARITY_180				(unsigned char)0x01

// Active Output SPDIF
#define ACTIVEOUTPUT_SPDIF_ON			(unsigned char)0x00
#define ACTIVEOUTPUT_SPDIF_OFF			(unsigned char)0x01

// Active Output TOSLINK
#define ACTIVEOUTPUT_TOSLINK_ON			(unsigned char)0x00
#define ACTIVEOUTPUT_TOSLINK_OFF		(unsigned char)0x01

// Active Output AES_EBU
#define ACTIVEOUTPUT_AES_EBU_ON			(unsigned char)0x00
#define ACTIVEOUTPUT_AES_EBU_OFF		(unsigned char)0x01

// Active Output ANALOG
#define ACTIVEOUTPUT_ANALOG_ON			(unsigned char)0x00
#define ACTIVEOUTPUT_ANALOG_OFF			(unsigned char)0x01

// Source mode
#define	SRC_CD_PLAYER					(unsigned char)0x02
#define SRC_EXTERNAL_INPUT				(unsigned char)0x03

// Listen Input
#define DIGITAL_INPUT_SPDIF				(unsigned char)0x00
#define DIGITAL_INPUT_AES_EBU			(unsigned char)0x01
#define DIGITAL_INPUT_TOSLINK			(unsigned char)0x02
#define DIGITAL_INPUT_MEZZANINE_USB			(unsigned char)0x03
#define DIGITAL_INPUT_MEZZANINE_NET			(unsigned char)0x04

// Record Input
#define RECORD_INPUT_CD					(unsigned char)0x00
#define RECORD_INPUT_DIR				(unsigned char)0x01
#define RECORD_INPUT_MEZZANINE			(unsigned char)0x02

// Clock Output
#define CLOCK_OUTPUT_OFF				(unsigned char)0x00
#define CLOCK_OUTPUT_44K				(unsigned char)0x01
#define CLOCK_OUTPUT_48K				(unsigned char)0x02
#define CLOCK_OUTPUT_AUTO				(unsigned char)0x03

#define TIMEOUT_GET_TOC					15000

// Standby parameter
#define STB_NORMAL						0
#define STB_NO_DISPLAY					1


/** D E C L A R A T I O N S **************************************************/
void powerOnInit(void);
void loadDefaultConfig (void);
void initSystem(void);
void standby(unsigned char mode);
unsigned char updateConfig (unsigned char Cmd);


/** E N D   O F   F I L E ****************************************************/

#endif
