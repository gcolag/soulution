
/*
 * File:         pca9555
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/16
 * Description:  PCA9555 driver
 *
 * History :
 *
 *   2015/04/16  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include "pca9555.h"

// HACK : Enable/Disable ChangeNotification interrupts needed.
//
// ChangeNotification interrupts will trig I2C com when
// the MUTE signal toggles, potentially in conflict with the
// I2C access below. 
//
#include "io.h"


/*****************************************************************************/


int pca9555_reg_wr(pca9555_dev_type* dev,
                   unsigned char reg,
                   unsigned char data)
{
  unsigned char address;
  int status;

  // HACK : Disable ChangeNotification interrupts.
  //
  // ChangeNotification interrupts will trig I2C com when
  // the MUTE signal toggles, potentially in conflict with the
  // I2C access below. Protect this section.
  //
  // Note : waiting for a better implementation (I2C ressource
  //        sharing handling)
  //
  io_disable_cn_int();

  address = dev->addr;

	IdleI2C1();								// wait until I2C module is idle
	StartI2C1();							// start transfer
	IdleI2C1();

	if (MasterWriteI2C1(address & 0xFE))	// io expander address, write operation
	{
    status = -3;
    goto end;
  }
	while (I2C1STATbits.TBF);				// wait till data is transmitted.
	IdleI2C1();
	if (I2C1STATbits.ACKSTAT)
	{
    status = -2;
    goto end;
  }
	if(MasterWriteI2C1(reg))				// access output port
	{
    status = -3;
    goto end;
  }
	while (I2C1STATbits.TBF);				// wait till data is transmitted.
	IdleI2C1();
	if (I2C1STATbits.ACKSTAT)
	{
    status = -2;
    goto end;
  }
	if(MasterWriteI2C1(data))				// write output port
	{
    status = -3;
    goto end;
  }
	while (I2C1STATbits.TBF);				// wait till data is transmitted.
	IdleI2C1();
	if (I2C1STATbits.ACKSTAT)
	{
    status = -2;
    goto end;
  }
	StopI2C1();
	IdleI2C1();								// wait until I2C module is idle
	
  if (reg == PCA9555_REG_OUTPUT_PORT_0)
    dev->shadow0 = data;
  if (reg == PCA9555_REG_OUTPUT_PORT_1)
    dev->shadow1 = data;
  if (reg == PCA9554_REG_OUTPUT_PORT)
    dev->shadow0 = data;

  status = 0;
end:
  // HACK : Enable ChangeNotification interrupts.
  //
  io_enable_cn_int();

  return status;
}


int pca9555_reg_rd(pca9555_dev_type* dev,
                   unsigned char reg, 
                   unsigned char* data)
{
  unsigned char address;
	unsigned char register_port;

  // HACK : Disable ChangeNotification interrupts.
  //
  // ChangeNotification interrupts will trig I2C com when
  // the MUTE signal toggles, potentially in conflict with the
  // I2C access below. Protect this section.
  //
  // Note : waiting for a better implementation (I2C ressource
  //        sharing handling)
  //
  io_disable_cn_int();

  address = dev->addr;

	IdleI2C1();								// wait until I2C module is idle
	StartI2C1();							// start transfer
	IdleI2C1();
	MasterWriteI2C1(address & 0xFE);		// io expander address, write operation
	while (I2C1STATbits.TBF);				// wait till data is transmitted.
	IdleI2C1();
	MasterWriteI2C1(reg);					// access input or ouptut port
	while (I2C1STATbits.TBF);				// wait till data is transmitted.
	IdleI2C1();
	RestartI2C1();							// restart transfer
	while (I2C1CONbits.RSEN);		// wait till Repeated Start sequence is completed
	IdleI2C1();
	MasterWriteI2C1(address | 0x01);		// io expander address, read operation
	while (I2C1STATbits.TBF);				// wait till data is transmitted.
	IdleI2C1();
	*data = MasterReadI2C1();		// read input or output port
	IdleI2C1();	
	NotAckI2C1();							// generate not acknowledge condition
	IdleI2C1();
	StopI2C1();
	IdleI2C1();								// wait until I2C module is idle

  // HACK : Enable ChangeNotification interrupts.
  //
  io_enable_cn_int();

	return register_port;
}


/*****************************************************************************/


void pca9555_bit_set(pca9555_dev_type* dev, unsigned char bit)
{
  unsigned short data;
  unsigned char data0;
  unsigned char data1;

  data = 1<<bit;
  data0 = dev->shadow0 | (unsigned char)data;
  data1 = dev->shadow1 | (unsigned char)(data>>8);

  if (data0 != dev->shadow0)
  {
    pca9555_reg_wr(dev, PCA9555_REG_OUTPUT_PORT_0, data0);
  }

  if (data1 != dev->shadow1)
  {
    pca9555_reg_wr(dev, PCA9555_REG_OUTPUT_PORT_1, data1);
  }
}


void pca9555_bit_clr(pca9555_dev_type* dev, unsigned char bit)
{
  unsigned short data;
  unsigned char data0;
  unsigned char data1;

  data = 1<<bit;
  data0 = dev->shadow0 & ~(unsigned char)data;
  data1 = dev->shadow1 & ~(unsigned char)(data>>8);

  if (data0 != dev->shadow0)
  {
    pca9555_reg_wr(dev, PCA9555_REG_OUTPUT_PORT_0, data0);
    dev->shadow0 = data0;
  }

  if (data1 != dev->shadow1)
  {
    pca9555_reg_wr(dev, PCA9555_REG_OUTPUT_PORT_1, data1);
    dev->shadow1 = data1;
  }
}


int pca9555_bit_get(pca9555_dev_type* dev, unsigned char bit)
{
  unsigned char data;
  unsigned char mask;

  if (bit<8)
  {
    pca9555_reg_rd(dev, PCA9555_REG_INPUT_PORT_0, &data);
    mask = (1<<bit);
  }
  else
  {
    pca9555_reg_rd(dev, PCA9555_REG_INPUT_PORT_1, &data);
    mask = (1<<(bit-8));
  }

  return data & mask;
}


/*****************************************************************************/


void pca9554_bit_set(pca9555_dev_type* dev, unsigned char bit)
{
  unsigned char data;

  data = dev->shadow0 | (unsigned char)(1<<bit);

  if (data != dev->shadow0)
  {
    pca9555_reg_wr(dev, PCA9554_REG_OUTPUT_PORT, data);
  }
}


void pca9554_bit_clr(pca9555_dev_type* dev, unsigned char bit)
{
  unsigned char data;

  data = dev->shadow0 & ~(unsigned char)(1<<bit);

  if (data != dev->shadow0)
  {
    pca9555_reg_wr(dev, PCA9554_REG_OUTPUT_PORT, data);
  }
}


int pca9554_bit_get(pca9555_dev_type* dev, unsigned char bit)
{
  unsigned char data;
  unsigned char mask;

  pca9555_reg_rd(dev, PCA9554_REG_INPUT_PORT, &data);
  mask = (1<<bit);

  return data & mask;
}


/** E N D   O F   F I L E ****************************************************/
