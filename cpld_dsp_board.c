
/*
 * File:         cpld_dsp_board
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 *
 * Created:      2015/04/18
 * Description:  CPLD Dsp Board I/O
 *
 * History :
 *
 *   2015/04/18  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include <p24fxxxx.h>
#include <PPS.h>

#include "GenericTypeDefs.h"
#include "periph_spi.h"
#include "io.h"


/*****************************************************************************/


// HAL
//


static UINT8 spi_read_write(UINT8 dataOut)
{
  return spi2_write(dataOut);
}


static void assert_cs(void)
{
  // HACK : Disable ChangeNotification interrupts.
  //
  // ChangeNotification interrupts will trig I2C com when
  // the MUTE signal toggles, potentially in conflict with the
  // I2C access below. Protect this section.
  //
  // Note : waiting for a better implementation (I2C ressource
  //        sharing handling)
  //
  io_disable_cn_int();

  CPLD_nCS = 0;
}


static void deassert_cs(void)
{
  CPLD_nCS = 1;

  // HACK : Enable ChangeNotification interrupts.
  //
  io_enable_cn_int();
}


/*****************************************************************************/


static unsigned char shadow_data;


/*****************************************************************************/


void cdb_write(UINT8 data)
{
  shadow_data = data;

  assert_cs();
  spi_read_write(data);
  deassert_cs();
}


UINT8 cdb_read(void)
{
  UINT8 data;

  assert_cs();
  data = spi_read_write(shadow_data);
  deassert_cs();

  return data;
}


/*****************************************************************************/


void cdb_bit_set(unsigned char bit)
{
  cdb_write(shadow_data | (1<<bit));
}


void cdb_bit_clr(unsigned char bit)
{
  cdb_write(shadow_data & ~(1<<bit));
}


int cdb_get_bit(unsigned char bit)
{
  UINT8 data;

  data = cdb_read();

  return (data & (1<<bit)) == (1<<bit);
}


/*****************************************************************************/
