
/*
 * File:         keys
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Values for keypad, rc5 and rs232 decoding
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __KEYS_H
#define __KEYS_H

/** T Y P E S ****************************************************************/


typedef struct
{
	// Key id.
	unsigned char id;
	unsigned char lastid; 	// last pressed key
	
	// Key count.
	// Every press, this value is reseted. When holding down the button, count
	// will increase.
	unsigned short count;	
} keyStruct;

typedef struct
{
	unsigned char dev;		// Device ID
	unsigned char lastdev;	// Last pressed key device ID
	unsigned char id;		// Key ID
	unsigned char lastid; 	// Last pressed key ID
	unsigned short count;
} rcKeyStruct;

typedef struct
{
	unsigned char id;
	unsigned char state;
} rotaryKeyStruct;


/** D E F I N E S ************************************************************/
// This value is used when no key is detected.
#define	NO_KEY						(unsigned char)0xFF

// Keypad keys definition. Keys are active low.
#define KEY_ENCODER					0xFE
#define KEY_OPEN_CLOSE				0xFD
#define KEY_PROGRAM					0xFB
#define KEY_POWER					0xF7
#define KEY_MUTE				0xFD

// Keys RC5
#define	KEY_RC5_POWER				0x0C
#define	KEY_RC5_OPEN_CLOSE			0x36
#define	KEY_RC5_BACKWARD			0x21
#define	KEY_RC5_PLAY_PAUSE			0x35
#define	KEY_RC5_FORWARD				0x20
#define	KEY_RC5_VOL_UP				0x10
#define	KEY_RC5_VOL_DOWN			0x11
#define	KEY_RC5_PROGRAM				0x0E
#define	KEY_RC5_ENTER				0x3F
#define	KEY_RC5_POWER_ON			0x32
#define	KEY_RC5_POWER_OFF			0x33
#define	KEY_RC5_MUTE				0x0D

// ID remote control
#define CODE_SOULUTION_720			0x10
#define CODE_SOULUTION_CD540		0x14		// Same as CD745
#define CODE_SOULUTION_DAC760		0x0D

// RS232 control protocol
#define RS232_CMD_POWER_ON			63
#define RS232_CMD_POWER_OFF			64

#define RS232_CMD_VOL_UP			20
#define RS232_CMD_VOL_DOWN			21
#define RS232_CMD_BAL_LEFT			24
#define RS232_CMD_BAL_RIGHT			25

#define RS232_CMD_STOP				30
#define RS232_CMD_OPEN_CLOSE		31
#define RS232_CMD_PLAY_PAUSE		32
#define RS232_CMD_SKIP_BW			33
#define RS232_CMD_SKIP_FW			34
#define RS232_CMD_SEARCH_BW			35
#define RS232_CMD_SEARCH_FW			36
#define RS232_CMD_CD_MODE			37
#define RS232_CMD_DAC_MODE			38
#define RS232_CMD_SRC_AES			39
#define RS232_CMD_SRC_SPDIF			40
#define RS232_CMD_SRC_SPDIF2		41	// nur 745
#define RS232_CMD_SRC_OPT			42
#define RS232_CMD_SRC_USB			43
#define RS232_CMD_SRC_UP			44
#define RS232_CMD_SRC_DOWN			45
#define RS232_DIGITAL_OUTPUT_ON		46
#define RS232_DIGITAL_OUTPUT_OFF	47
#define RS232_CMD_SRC_NET			48

#define RS232_REPEAT_OFF			70
#define RS232_REPEAT_TRACK			71
#define RS232_REPEAT_DISC			72
#define RS232_REPEAT_RANDOM			73
#define RS232_TIME_TRACK_FWD		74
#define RS232_TIME_TRACK_BWD		75
#define RS232_TIME_DISC_FWD			76
#define RS232_TIME_DISC_BWD			77

#define RS232_GET_POWER				210
#define RS232_GET_STATE				211
#define RS232_GET_INPUT				212
#define RS232_GET_VERSION			215



/** E N D   O F   F I L E ****************************************************/
#endif
