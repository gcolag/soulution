
/*
 * File:         config_boards
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  I/O expanders control (mostly)
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include "dbio.h"
#include "dacbio.h"
#include "PCM1792.h"
#include "cs8416.h"

#include "cpld_dsp_board.h"
#include "config_boards.h"


/** F U N C T I O N S ********************************************************/
/*****************************************************************************
 * Configure_Boards
 *
 * Description	:	This function configures the different circuits at startup.
 *
 * Note			:	I2C communication must be open before calling this function.
 *
 * Returns	 0	:	No error
 *		  	-1	:	I2C failure
 *			-2	:	Sonic2 failure
 *
 *****************************************************************************/
char initBoards(void)
{
#if 0

	// Init DSP board
	if (InitPCA9554(ADDRESS_PCA9554_DAC, 0x00, 0x01))	// Digital board LED on
		return -1;
	if (InitPCA9554(ADDRESS_PCA9554_FPGA, 0x00, 0x61))	// PCM, Phase normal,
		return -1;										// Listen & Record CD/SACD,
														// S2 data not valid

/*
 * SPARE1 & SPARE3 lines are inverted on 106_11_300_0 board
 * compared to 106_03_200.
 */
#define INVERT_SPARE1_SPARE3_FOR_USB_LAN_MEZANINE

#if defined(INVERT_SPARE1_SPARE3_FOR_USB_LAN_MEZANINE)
	if (InitPCA9554(ADDRESS_PCA9554_STR, 0x80, 0x00))	// Mezzanine disabled,
#else
	if (InitPCA9554(ADDRESS_PCA9554_STR, 0x20, 0x00))	// Mezzanine disabled,
#endif
		return -1;										// Dig. outputs disabled
														// DIR enabled
														// STR_SPARE1 = USB_BUS_DETECT# (input)
	delay_ms(100);
	
  UnselectMezIOExp();

	Nop();
	
	EnableDIR();
	// Wait for DIR RST# (RC filter)
	delay_ms(1000);
	
	if (WM8805_init())
		return -1;
	
	Nop();
	
	// Init Sonic2
	if (initSonic2())
		return -2;
	setVolume(0);
	DataValidSonic2();
	
	initPreamp();
	configListen();
	configRecord();
	configPhasePolarity();
	
	// Init DAC
/*
	EnablePCM1792();
	delay_ms(10);
	initPCM1792(DAC_ID_RIGHT);
	initPCM1792(DAC_ID_LEFT);
*/
  dacbio_deassert_rst();

	
	// Enable output
	EnableOutput();					// Unmute the analog output

#else

  // New arch (DAC760) adds
  {
    dbio_init();
    dacbio_init();

    spi2_open(0);

    cdb_write(0x00); //  all periph rst asserted
    delay_ms(100);
    cdb_write(0x07); //  MUX USB_LAN ; all periph rst deasserted
    delay_ms(100);
    
    // Unmute
    dacbio_deassert_mcu_mute();
    
    dacbio_deassert_mcu_unmute();
    dacbio_assert_mcu_unmute();
  }

  // Init DIT
  dbio_deassert_dit_reset();
	delay_ms(100);

  // Init DIR
  cs8416_init();
  delay_ms(2000);

	// Init Sonic2
	if (initSonic2())
		return -2;
	setVolume(0);

	initPreamp();
	configListen();
	configRecord();
	configPhasePolarity(userConfig.phasePolarity);

  setMute(ON);
//  DataNotValidSonic2();
  DataValidSonic2();

  // Init DAC
  dacbio_assert_rst();
  delay_ms(100);
  dacbio_deassert_rst();
  delay_ms(100);

  initPCM1792(DAC_ID_LEFT);
  initPCM1792(DAC_ID_RIGHT);

	// Enable output
  dacbio_deassert_main_mute();
#endif

	return 0;
}

/*****************************************************************************
 * unlockI2C
 *
 * Description	:	This function must be executed if an I2C communication fails.
 *
 *****************************************************************************/
void unlockI2C (void)
{
	StopI2C1();
	IdleI2C1();
}

/*****************************************************************************
 * configPhasePolarity
 *
 * Description	:	Configure the phase polarity. 
 *
 *****************************************************************************/
void configPhasePolarity(unsigned char phasePolarity)
{
	DataNotValidSonic2();
	delay_ms(10);
	
	if (phasePolarity == PHASEPOLARITY_180)
		ChooseReversePhase();
	else
		ChooseNormalPhase();
	
	delay_ms(10);
	DataValidSonic2();
}

/*****************************************************************************
 * configListen
 *
 * Description		:	This function configures the DIR and the FPGA to select
 *						the Listen Input
 *
 * Returns		 0	:	No error
 *		  		-1	:	An error occured
 *
 *****************************************************************************/
char configListen (void)
{
	char err = 0;
	
	// Select listen input
  if (userConfig.SourceMode == SRC_EXTERNAL_INPUT)
	{
    if (volatileConfig.mute == 0)
    {
      setMute(ON);
//      DataNotValidSonic2();
    }
//		ResetSonic2();
		DataFormatPCM();
//		EnableSonic2();
//		delay_ms(200);	// VOL
//		setVolume(0);
		
		switch (userConfig.DigitalInput)
		{
			case DIGITAL_INPUT_SPDIF:
			case DIGITAL_INPUT_TOSLINK:
			case DIGITAL_INPUT_AES_EBU:
				ChooseListenReceiver();

        // When using one of the Digital Inputs, whe are in assychronous
        // mode : the DIR supplies its own clock to the S8 and the
        // S8 uses the 24M to generate the output.
        // For this to work, the 24M must be selected since the S8's ASRC
        // won't work with a 22M when the digital input is 24M based.
        //
        // Update (2016/05/17) :
        // We now have an option in the user menu to force 22M or 24M
        // when the DIR is selected. The user is not supposed to force
        // 22M when using 24M based signals.
        // 
        switch (userConfig.ClockOutput)
        {
          case CLOCK_OUTPUT_44K:
            dbio_select_22M();
            break;
          case CLOCK_OUTPUT_48K:
          default:
            dbio_select_24M();
            break;
        }

				if (WM8805_RX_select(userConfig.DigitalInput))	// Configure DIR
					err = -1;
				break;
			
			case DIGITAL_INPUT_MEZZANINE_USB:
				ChooseListenMezzanine();
        SelectUSBStreaming();
        if (dbio_get_usb_lan_n22M_24M())
          dbio_select_24M();
        else
          dbio_select_22M();
        break;

			case DIGITAL_INPUT_MEZZANINE_NET:
				ChooseListenMezzanine();
        SelectNetworkStreaming();
        if (dbio_get_usb_lan_n22M_24M())
          dbio_select_24M();
        else
          dbio_select_22M();
				break;
			
			default:
				break;
		}
		
    if (volatileConfig.mute == 0)
    {
//      DataValidSonic2();

      // A small delay is needed here because if we toggled
      // the MCLK of the DACs, these need a short time for
      // their internal state machine to get ready and understand
      // the OPE-bit-clear (= unmute) order.
      // Tested with 10ms, ok ; increased to 50ms for security.
      //
      delay_ms(50);
      setMute(OFF);
    }
	}
	
	return err;
}

/*****************************************************************************
 * configRecord
 *
 * Description		:	This function configures the DIR and the FPGA to select
 *						the Record Input
 *
 *****************************************************************************/
void configRecord (void)
{
	// Enable/disable the digital outputs
	if (userConfig.DigitalOutput == OFF)
	{
		DisableOutputSPDIF();
		DisableOutputTOSLINK();
		DisableOutputAES_EBU();
	}
	else
	{
		// Configure the FPGA
		if (userConfig.SourceMode == SRC_EXTERNAL_INPUT)
		{
			switch (userConfig.DigitalInput)
			{
				case DIGITAL_INPUT_SPDIF:
				case DIGITAL_INPUT_TOSLINK:
				case DIGITAL_INPUT_AES_EBU:
					ChooseRecordReceiver();
					break;
				
				case DIGITAL_INPUT_MEZZANINE_USB:
				case DIGITAL_INPUT_MEZZANINE_NET:
					ChooseRecordMezzanine();
					break;
				
				default:
					break;
			}
		}
		
		EnableOutputSPDIF();
		EnableOutputTOSLINK();
		EnableOutputAES_EBU();
	}
}


/** E N D   O F   F I L E ****************************************************/
