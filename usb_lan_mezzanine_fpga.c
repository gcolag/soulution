
/*
 * File:         usb_lan_mezzanine_fpga
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  control I/O exandder in fpga of usb_lan mezzanine board
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include "config_boards.h"
#include "usb_lan_mezzanine_fpga.h"


/** D E F I N I T I O N S ****************************************************/
// SPI bus used to control the device.
#define	openSPI				mOpenSPI1
#define	closeSPI			mCloseSPI1
#define	writeSPI			mWriteSPI1
#define	readSPI				mReadSPI1
#define configIntSPI		mConfigIntSPI1
#define busySPI				SPI1STATbits.SPITBF


// I/O expander bits definition
#define USB_LAN_DSD_MASK  (1<<0)


/** V A R S ******************************************************************/
static unsigned char g_fpga_data_out;
static unsigned char g_fpga_data_in;


/** F U N C T I O N S ********************************************************/

/*****************************************************************************
* ReadWriteUsbLanFpga
*
* overview : Read/Write on SPI
*
*****************************************************************************/
static void ReadWriteUsbLanFpga(void)
{
//	openSPI (MASTER_ENABLE_ON | SPI_MODE8_ON | PRI_PRESCAL_64_1 | SEC_PRESCAL_8_1
//			| SPI_SMP_OFF | SPI_CKE_OFF | CLK_POL_ACTIVE_HIGH, FRAME_ENABLE_OFF, SPI_ENABLE);
	
	delay_50us();
	
  SelectMezIOExp();
	
	// Send register to be written.
	g_fpga_data_in = writeSPI(g_fpga_data_out);
	while(busySPI);
	
  UnselectMezIOExp();

	delay_50us();
	
//	closeSPI();
}


/*****************************************************************************
* ReadUsbLanFpga
*
* overview : Read on SPI
*
*****************************************************************************/
void ReadUsbLanFpga(unsigned char* data)
{
  ReadWriteUsbLanFpga();
  *data = g_fpga_data_in;
}


/*****************************************************************************
* WriteUsbLanFpga
*
* overview : Write on SPI
*
*****************************************************************************/
void WriteUsbLanFpga(unsigned char data)
{
  g_fpga_data_out = data;
  ReadWriteUsbLanFpga();
}


/*****************************************************************************
* UsbLanFpgaGetDsdFlag
*
* overview : Return DSD Flag value 
*            1 : DSD
*            0 : PCM
*
*****************************************************************************/
int UsbLanFpgaGetDsdFlag(void)
{
/*
  unsigned char data;

  ReadUsbLanFpga(&data);

  if (data & USB_LAN_DSD_MASK)
  {
    return 1;
  }
  else
  {
    return 0;
  }
*/
  return 0;
}

/* END OF FILE ***************************************************************/

