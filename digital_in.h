
/*
 * File:         digital_in
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Digital inputs management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef	__DIGITAL_IN_H
#define	__DIGITAL_IN_H

/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <ports.h>

#include "variables.h"
#include "periph_timer.h"
#include "config_mgr.h"
#include "config_boards.h"
#include "WM8805.h"
#include "PCM1792.h"
#include "SCF5742.h"
#include "sonic2.h" 


/** D E F I N E S ************************************************************/
#define STATE_DAC_INIT				0x00
#define STATE_DAC_UNLOCKED			0x01
#define STATE_DAC_LOCKED			0x02
#define STATE_DAC_PLAYING			0x03
#define STATE_DAC_UNLOCKING			0x04

#define CLOCK44_48					0x01
#define CLOCK88_96					0x02
#define CLOCK176_192				0x03
#define CLOCKERROR					0xFF


/** V A R I A B L E S ********************************************************/
extern unsigned char clock_Rate;
extern char DAC_status;


/** D E C L A R A T I O N S **************************************************/
void manageDigitalInput(void);


/** E N D   O F   F I L E ****************************************************/

#endif
