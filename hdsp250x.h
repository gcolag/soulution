
/*
 * File:         hdsp250x
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  HDSP 250X generic driver
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef _HDSP250X_H_
#define _HDSP250X_H_

/*****************************************************************************/


typedef struct
{
  unsigned char dev_index;
} hdsp250x_dev_type;


/*****************************************************************************/


void hdsp250x_standby(void);
void hdsp250x_init(void);
void hdsp250x_init_udc(hdsp250x_dev_type* dev);
void hdsp250x_test(void);
void hdsp250x_write_char(hdsp250x_dev_type* dev, UINT8 pos, char data);
void hdsp250x_set_brightness(hdsp250x_dev_type* dev, UINT8 brightness);


/*****************************************************************************/

#endif // _HDSP250X_H_

