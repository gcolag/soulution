
/*
 * File:         main
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Main program
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include <p24fxxxx.h>
#include <uart.h>

#include "variables.h"
#include "periph_timer.h"
#include "config_mgr.h"
#include "io.h"
#include "config_boards.h"
#include "SCF5742.h"
#include "display.h"
#include "cmd_mgr.h"
#include "digital_in.h"
#include "rs232.h"

#include <stdio.h>

#include "usb_lan_mezzanine_fpga.h"
#include "fp_display.h"
#include "error.h"
#include "dbio.h"
#include "test.h"

#include "dacbio.h"
#include "PCM1792.h"

#include "hal.h"


/*** Configuration bits *******************************************************/


// Configuration Bits
//
#if defined(__PIC24FJ256GB108__)
	_CONFIG1( JTAGEN_OFF & GCP_OFF & GWRP_OFF & COE_OFF & FWDTEN_OFF & ICS_PGx2) 
	_CONFIG2( DISUVREG_OFF & IESO_OFF & FCKSM_CSDCMD & OSCIOFNC_OFF & POSCMOD_HS & FNOSC_PRIPLL & PLLDIV_DIV3 & IOL1WAY_OFF)
#else
	#warning Cannot define configuration bits.
#endif


/** M A I N   P R O G R A M **************************************************/
// -----------------------------------------------------------------------------
// DAC760 normal operation
// -----------------------------------------------------------------------------

void debug_display(int state);

int run_system(void)
{
	unsigned char cmd;
	
	powerOnInit();

	// Main loop
	for(;;)
	{
		// Check volume control timeout (for display)
		if (volatileConfig.setVolume == ON)
		{
			if (readTimerDiff(volatileConfig.timeout_volume) > 5000)
				volatileConfig.setVolume = OFF;
		}
		
		// Switch according to machine status
		switch (volatileConfig.state)
		{
			// ----------------------------------------------------------------
			// DAC mode
			// ----------------------------------------------------------------
			case STATE_EXTERNAL_INPUT:
				if (userConfig.DigitalInput < DIGITAL_INPUT_MEZZANINE_USB)
					manageDigitalInput();

				if (volatileConfig.setVolume == ON)
					displayVolumeMode();
				else
					displayDACMode();

// Unneeded ; done in fpga
#if 0
        if ( (userConfig.DigitalInput == DIGITAL_INPUT_MEZZANINE_NET) ||
             (userConfig.DigitalInput == DIGITAL_INPUT_MEZZANINE_USB) )
        {
          // With USB/LAN we must check for a format change
          // since both are supported
          if (UsbLanFpgaGetDsdFlag())
          {
            DataFormatDSD();
          }
          else
          {
            DataFormatPCM();
          }
        }
        else
        {
          // With other links only PCM is available, so reconfigure the 
          // data format only once we switch to that link
          static int prev_DigitalInput = -1;

          if (prev_DigitalInput != userConfig.DigitalInput)
          {
            DataFormatPCM();
          }
          prev_DigitalInput = userConfig.DigitalInput;
        }
#endif

				break;
			
#if 0
			// ----------------------------------------------------------------
			// Error mode
			// ----------------------------------------------------------------
			case STATE_OVERCURRENT:
				if (volatileConfig.flagOvercurrent == CHECK_OVERCURRENT)
				{
					volatileConfig.timeout_overcurrent_long = readTimerCounter();
					volatileConfig.flagOvercurrent = OVERCURRENT;
				}
				if (readTimerDiff(volatileConfig.timeout_overcurrent) > 500)
				{
					if (test_overcurrent() == 0)
					{
						if (readTimerDiff(volatileConfig.timeout_overcurrent_long) > 10000)
						{
							// Stop protection
							EnableOutput();
							
							// Exit overcurrent state
              deassert_pre_mute();
							volatileConfig.flagOvercurrent = NO_OVERCURRENT;
							volatileConfig.state = volatileConfig.prevState;
						}
					}
					volatileConfig.timeout_overcurrent = readTimerCounter();
				}
				break;
#endif
			
			// ----------------------------------------------------------------
			// Standby mode
			// ----------------------------------------------------------------
			case STATE_STANDBY:
				// Manage start-up mode
				switch(userConfig.StartMode)
				{
					case STARTMODE_NORM:		// Stay in standby mode
					case STARTMODE_AUTO:		// Stay in standby mode
						break;
					
					case STARTMODE_LINK:		// Start the CD Player
						cmd = getTrigger();		// on Trigger event
						if (cmd == CMD_WAKE_UP)
							initSystem();
						break;
					
					default:					// Error: wrong value
						loadDefaultConfig();
						volatileConfig.state = STATE_STANDBY;
						break;
				}
				break;
			
			default:
				volatileConfig.state = STATE_STANDBY;
				break;
		}
		
#if 0
		// --------------------------------------------------------------------
		// Global tasks - main state machine
		// --------------------------------------------------------------------
		// Manage RS232 interface
		if (rs232_state == RS232_RECEIVING)
		{
			if (rs232_byte_arrived || DataRdyUART2())
			{
				receiveRS232(0);
				rs232_byte_arrived = 0;
			}
			else if (readTimerDiff(rs232_timeout) > 20)
				receiveRS232(1);
		}
#endif
		
		// Scan alternatively keypad, remote control, encoder and trigger.
		updateConfig (getCmd());
		
#if 0
		// --------------------------------------------------------------------
		// Check protections
		// --------------------------------------------------------------------
		// Inhibit overcurrent protection at startup
		if (volatileConfig.flagOvercurrent == OVERCURRENT_INHIBITED)
		{
			// After 5s at startup, enable overcurrent test
			if (readTimerDiff(timeout_overcurrent_inhibited) > 5000)
				volatileConfig.flagOvercurrent = NO_OVERCURRENT;
		}
		
		if (volatileConfig.flagOvercurrent == CHECK_OVERCURRENT)
		{
			if (test_overcurrent() == -1)
			{
				// Protection is active
				DisableOutput();
        assert_pre_mute();
				
				// Go into overcurrent state
				volatileConfig.prevState = volatileConfig.state;
				volatileConfig.state = STATE_OVERCURRENT;
				
				// Update screen refresh
				displayOvercurrent();
				
				// Initialize the timer to check periodically
				volatileConfig.timeout_overcurrent = readTimerCounter();
				volatileConfig.timeout_overcurrent_long = 
          volatileConfig.timeout_overcurrent;
				
				// Do not check overcurrent until protection stops
				volatileConfig.flagOvercurrent = OVERCURRENT;
			}
		}
#endif
		
#if 0
		// Check analog power supply
		if (volatileConfig.flagVoltage)
		{
			if (VOLTAGE_ERROR)
			{
				delay_ms(100);
				if (VOLTAGE_ERROR)
				{
					// Protection is active
					DisableOutput();
					PRE_MUTE = 1;
					
					standby(STB_NO_DISPLAY);
				}
			}
		}
#endif

#if 0		
		// Link feedback (slave pre-mute)
		if (LINK_FEEDBACK == 0)
		{
			// DEBUG
			// To be done !!!
		}
#endif

//    debug_display(volatileConfig.state);

	} // for(;;)
}


void debug_display(int state)
{
#if 0
  char dbuff[10];
  static int cnt = 0;

  cnt++;
  if (cnt > 9) 
    cnt = 0;

//  cnt = (cnt < 9) ? cnt++ : 0;
//  cnt = (cnt++) % 10;

  sprintf(dbuff, "%i %i%i%i", 
          cnt,
          state,
          userConfig.DigitalInput,
          DAC_status);
  fpd_write_string(2, 0, dbuff);

  fpd_write_string(2, 6, dbio_get_osc_22M_en() ? "1" : "0");
  fpd_write_string(2, 7, dbio_get_osc_24M_en() ? "1" : "0");
#endif

#if 0
  char dbuff[10];
  static int trig = 10;

  trig--;
  if (trig == 0)
  {
    sprintf(dbuff, "%02X %02X   ", getSonic2LLevel(), getSonic2RLevel());
    fpd_write_string(2, 0, dbuff);
    trig = 10;
  }
#endif

#if 0
	if (volatileConfig.state == STATE_EXTERNAL_INPUT)
  {
    rcKeyStruct key;

    readRCKey(&key);
    
    if (key.id != 0xFF)
    {
      char dbuff[10];
      sprintf(dbuff, "%02X", key.id);
      fpd_write_string(2, 0, dbuff);
    }
  }
#endif

#if 0
	if (volatileConfig.state == STATE_EXTERNAL_INPUT)
  {
    static int refresh = 10;

    refresh--;
//    if (refresh == 0)
    if (g_cnt_2 != 255)
    {
      char dbuff[10];

      sprintf(dbuff, "%03i %03i ", g_cnt_1, g_cnt_2);
      fpd_write_string(2, 0, dbuff);
      refresh = 10;
    }
  }
#endif
}


/*****************************************************************************/


void run_test(void)
{
#if 1
  {
    configIO();
    standbyIO();
    dispInit();
    dispPutLine1("Hello firm");

    while (1) ;
  }
#endif

	powerOnInit();
  initSystem();

  test_14();
}


/*****************************************************************************/


int main(void)
{
  hal_init();

//  run_test();
  run_system();

  error(ERR__END_OF_MAIN_REACHED);

  return 0;
}

	
/** E N D   O F   F I L E ****************************************************/
