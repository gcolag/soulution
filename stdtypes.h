
/*
 * File:         stdtypes.h
 * Based on:     
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 *
 * Created:      2015/05/06
 * Description:  Types definition
 *
 *
 * History :
 *
 *   2015/05/06  1.0  Initial release
 *
 */

/*****************************************************************************/

#ifndef __STDTYPES_H
#define	__STDTYPES_H

/*****************************************************************************/


#define int32_t   long int
#define uint32_t  unsigned long int
#define int16_t   int
#define uint16_t  unsigned int
#define int8_t    char
#define uint8_t   unsigned char


/*****************************************************************************/

#endif	/* __STDTYPES_H */


