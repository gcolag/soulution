
/*
 * File:         pcm1792
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  pcm1792 DAC driver
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include <i2c.h>
#include "pcm1792.h"

// HACK : Enable/Disable ChangeNotification interrupts needed.
//
// ChangeNotification interrupts will trig I2C com when
// the MUTE signal toggles, potentially in conflict with the
// I2C access below. 
//
#include "io.h"


/** D E F I N I T I O N S ****************************************************/


#define DAC_LEFT_I2C_ADDR    0x98 
#define DAC_RIGHT_I2C_ADDR   0x9A


/** P R I V A T E  P R O T O T Y P E S ***************************************/


static int WritePCM1792 (unsigned char dacID, 
                         unsigned char regId, 
                         unsigned char data);


/** F U N C T I O N S ********************************************************/


/*****************************************************************************
 * initPCM1792
 *
 * overview		:	Initialization function for operation of the DAC.
 *					- Right-justified data format
 *					- Digital filter bypassed
 *					- Stereo mode
 *
 * id			: 	DAC_ID_LEFT or DAC_ID_RIGHT (as defined in PCM1792.h)
 *
 * channel		: 	DAC_MONO_LEFT or DAC_MONO_RIGHT
 *					In case of monaural mode, specifies the channel selection.
 *
 *****************************************************************************/
void initPCM1792 (unsigned char id)
{
	// DF bypassed, 24-bit format, right-justified data.
	WritePCM1792(id, DAC_REG_18, 0x20);
	
	// DF bypassed, stereo input
//	if (volatileConfig.phasePolarity == PHASEPOLARITY_0)
		WritePCM1792(id, DAC_REG_19, 0x04);
//	else
//		WritePCM1792(id, DAC_REG_19, 0x84);
	
	// DF bypassed, 8*Fs oversampling
	WritePCM1792(id, DAC_REG_20, 0x10);
}


// Kept here for debug 
//
int g_last_pcm1792_cmd = 0;


void mutePCM1792 (unsigned char id)
{
  // set OPE bit
  //
  WritePCM1792(id, DAC_REG_19, 0x14);
  g_last_pcm1792_cmd = 1;
}


void unmutePCM1792 (unsigned char id)
{
  // clear OPE bit
  //
  WritePCM1792(id, DAC_REG_19, 0x04);
  g_last_pcm1792_cmd = 2;
}


/*****************************************************************************
* WritePCM1792
*
* overview		:	Writes a value to the specified register of DAC.
*
* dacId			:	DAC_ID_LEFT or DAC_ID_LEFT (as defined in PCM1792.h)
*
* regId			:	register to write.
*
* data			:	data to write.
*
******************************************************************************/
static int WritePCM1792 (unsigned char dacID, 
                         unsigned char regId,
                         unsigned char data)
{
  unsigned char address;
  int status;

  // HACK : Disable ChangeNotification interrupts.
  //
  // ChangeNotification interrupts will trig I2C com when
  // the MUTE signal toggles, potentially in conflict with the
  // I2C access below. Protect this section.
  //
  // Note : waiting for a better implementation (I2C ressource
  //        sharing handling)
  //
  io_disable_cn_int();

  if (dacID == DAC_ID_LEFT)
    address = DAC_LEFT_I2C_ADDR;
  else
    address = DAC_RIGHT_I2C_ADDR;

	IdleI2C1();								// wait until I2C module is idle
	StartI2C1();							// start transfer
	IdleI2C1();

	if (MasterWriteI2C1(address & 0xFE))	// io expander address, write operation
  {
    status = -3;
    goto end;
  }
	while (I2C1STATbits.TBF);				// wait till data is transmitted.
	IdleI2C1();
	if (I2C1STATbits.ACKSTAT)
  {
    status = -2;
    goto end;
  }
	if(MasterWriteI2C1(regId))				// access output port
  {
    status = -3;
    goto end;
  }
	while (I2C1STATbits.TBF);				// wait till data is transmitted.
	IdleI2C1();
	if (I2C1STATbits.ACKSTAT)
  {
    status = -2;
    goto end;
  }
	if(MasterWriteI2C1(data))				// write output port
  {
    status = -3;
    goto end;
  }
	while (I2C1STATbits.TBF);				// wait till data is transmitted.
	IdleI2C1();
	if (I2C1STATbits.ACKSTAT)
  {
    status = -2;
    goto end;
  }
	StopI2C1();
	IdleI2C1();								// wait until I2C module is idle
	
  status = 0;
end:
  // HACK : Enable ChangeNotification interrupts.
  //
  io_enable_cn_int();

	return 0;
}


/* END OF FILE ***************************************************************/
