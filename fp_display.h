
/*
 * File:         fp_display
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Front Pannel Display
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef _FP_DISPLAY_H_
#define _FP_DISPLAY_H_

/*****************************************************************************/


#include "GenericTypeDefs.h"


/*****************************************************************************/


void fpd_stby(void);
void fpd_init(void);
void fpd_write_char(UINT8 line, UINT8 pos, char character);
void fpd_write_string(UINT8 line, UINT8 pos, char* msg);
void fpd_clear(void);
void fpt_set_brightness(UINT8 brightness);


/*****************************************************************************/

#endif // _FP_DISPLAY_H_
