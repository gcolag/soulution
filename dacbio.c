
/*
 * File:         dacbio
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/19
 * Description:  DAC Board I/O
 *
 * History :
 *
 *   2015/04/19  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include "io.h"
#include "pca9555.h"


/*****************************************************************************/


#define DAC_BOARD_LEFT_PCA9554_ADDRESS   0x70
#define DAC_BOARD_RIGHT_PCA9554_ADDRESS  0x72


/*****************************************************************************/


#define DACBIO_BIT_UNUSED_PIN7               7
#define DACBIO_BIT_UNUSED_PIN6               6
#define DACBIO_BIT_UNUSED_PIN5               5
#define DACBIO_BIT_UNUSED_PIN4               4
#define DACBIO_BIT_UNMUTE_READBACK           3
#define DACBIO_BIT_MCU_UNMUTE                2
#define DACBIO_BIT_MCU_MUTE                  1
#define DACBIO_BIT_RST                       0


/*****************************************************************************/


static pca9555_dev_type  dac_left_dev_var;
static pca9555_dev_type *dac_left_dev = &dac_left_dev_var;

static pca9555_dev_type  dac_right_dev_var;
static pca9555_dev_type *dac_right_dev = &dac_right_dev_var;


/*****************************************************************************/


int  dacbio_init(void)
{
  dac_left_dev->addr = DAC_BOARD_LEFT_PCA9554_ADDRESS;
  dac_left_dev->shadow0 = 0x00;

  dac_right_dev->addr = DAC_BOARD_RIGHT_PCA9554_ADDRESS;
  dac_left_dev->shadow0 = 0x00;

  /*
   *  Input  0.7 I : NC
   *  Input  0.6 I : NC
   *  Input  0.5 I : NC
   *  Input  0.4 I : NC
   *  Input  0.3 I : UNMUTE readback
   *  Output 0.2 0 : UNUTE
   *  Output 0.1 0 : MUTE
   *  Output 0.0 0 : nRST
   */
  pca9555_reg_wr(dac_left_dev,
                 PCA9554_REG_OUTPUT_PORT,
                 0x00);
  pca9555_reg_wr(dac_left_dev,
                 PCA9554_REG_CONFIG_PORT,
                 0xF8);

  pca9555_reg_wr(dac_right_dev,
                 PCA9554_REG_OUTPUT_PORT,
                 0x00);
  pca9555_reg_wr(dac_right_dev,
                 PCA9554_REG_CONFIG_PORT,
                 0xF8);

  return 0;
}


/*****************************************************************************/


// Try to detect the presence of DAC board ; read write
// dummy value to the upper four bits of the ouput
// register.
//

static int dacbio_detect(pca9555_dev_type* dev)
{
  unsigned char saved_output_port;
  unsigned char data;
  int status;

  // Save previous output register value
  //
  pca9555_reg_rd(dev,
                 PCA9554_REG_OUTPUT_PORT,
                 &saved_output_port);

  // HACK : reread this register
  // don't know why but the very first I2C read returns
  // a wrong value (0x00) which then corrupt the output
  // register of the I/O expander, causing a reset of the
  // DAC (no left channel after power up symptoma)
  //
  pca9555_reg_rd(dev,
                 PCA9554_REG_OUTPUT_PORT,
                 &saved_output_port);

  // Read/Write 0xA0
  //
  data = (saved_output_port & 0x0F) | 0xA0;

  pca9555_reg_wr(dev,
                 PCA9554_REG_OUTPUT_PORT,
                 data);
  data = 0;
  pca9555_reg_rd(dev,
                 PCA9554_REG_OUTPUT_PORT,
                 &data);

  if ((data & 0xF0) != 0xA0)
  {
    status = -1;
    goto end;
  }

  // Read/Write 0x50
  //
  data = (saved_output_port & 0x0F) | 0x50;

  pca9555_reg_wr(dev,
                 PCA9554_REG_OUTPUT_PORT,
                 data);
  data = 0;
  pca9555_reg_rd(dev,
                 PCA9554_REG_OUTPUT_PORT,
                 &data);

  if ((data & 0xF0) != 0x50)
  {
    status = -1;
    goto end;
  }

  status = 0;
end:
  pca9555_reg_wr(dev,
                 PCA9554_REG_OUTPUT_PORT,
                 saved_output_port);
  return status;
}


int dacbio_left_detect(void)
{
  return dacbio_detect(dac_left_dev);
}


int dacbio_right_detect(void)
{
  return dacbio_detect(dac_right_dev);
}


/*****************************************************************************/


int  dacbio_get_int(void)
{
  return get_dac_int();
}


/*****************************************************************************/


void dacbio_left_assert_rst(void)
{
  pca9554_bit_clr(dac_left_dev,
                  DACBIO_BIT_RST);
}


void dacbio_left_deassert_rst(void)
{
  pca9554_bit_set(dac_left_dev,
                  DACBIO_BIT_RST);
}


void dacbio_left_assert_mcu_unmute(void)
{
  pca9554_bit_set(dac_left_dev,
                  DACBIO_BIT_MCU_UNMUTE);
}


void dacbio_left_deassert_mcu_unmute(void)
{
  pca9554_bit_clr(dac_left_dev,
                  DACBIO_BIT_MCU_UNMUTE);
}


void dacbio_left_assert_mcu_mute(void)
{
  pca9554_bit_set(dac_left_dev,
                  DACBIO_BIT_MCU_MUTE);
}


void dacbio_left_deassert_mcu_mute(void)
{
  pca9554_bit_clr(dac_left_dev,
                  DACBIO_BIT_MCU_MUTE);
}


int  dacbio_left_get_unmute_readback(void)
{
  return pca9554_bit_get(dac_left_dev, DACBIO_BIT_UNMUTE_READBACK);
}


/*****************************************************************************/


void dacbio_right_assert_rst(void)
{
  pca9554_bit_clr(dac_right_dev,
                  DACBIO_BIT_RST);
}


void dacbio_right_deassert_rst(void)
{
  pca9554_bit_set(dac_right_dev,
                  DACBIO_BIT_RST);
}


void dacbio_right_assert_mcu_unmute(void)
{
  pca9554_bit_set(dac_right_dev,
                  DACBIO_BIT_MCU_UNMUTE);
}


void dacbio_rightt_deassert_mcu_unmute(void)
{
  pca9554_bit_clr(dac_right_dev,
                  DACBIO_BIT_MCU_UNMUTE);
}


void dacbio_right_assert_mcu_mute(void)
{
  pca9554_bit_set(dac_right_dev,
                  DACBIO_BIT_MCU_MUTE);
}


void dacbio_right_deassert_mcu_mute(void)
{
  pca9554_bit_clr(dac_right_dev,
                  DACBIO_BIT_MCU_MUTE);
}


int  dacbio_right_get_unmute_readback(void)
{
  return pca9554_bit_get(dac_right_dev, DACBIO_BIT_UNMUTE_READBACK);
}


/*****************************************************************************/


void dacbio_assert_rst(void)
{
  dacbio_left_assert_rst();
  dacbio_right_assert_rst();
}


void dacbio_deassert_rst(void)
{
  dacbio_left_deassert_rst();
  dacbio_right_deassert_rst();
}


void dacbio_assert_mcu_unmute(void)
{
  dacbio_left_assert_mcu_unmute();
  dacbio_right_assert_mcu_unmute();
}


void dacbio_deassert_mcu_unmute(void)
{
  dacbio_left_deassert_mcu_unmute();
  dacbio_rightt_deassert_mcu_unmute();
}


void dacbio_assert_mcu_mute(void)
{
  dacbio_left_assert_mcu_mute();
  dacbio_right_assert_mcu_mute();
}


void dacbio_deassert_mcu_mute(void)
{
  dacbio_left_deassert_mcu_mute();
  dacbio_right_deassert_mcu_mute();
}


/*****************************************************************************/
