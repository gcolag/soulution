
/*
 * File:         SCF5742
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  4-digit 5x7 LED matrix display driver
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __SCF5742_H
#define __SCF5742_H

/** I M P O R T A N T ********************************************************/
// You must add the "USE_AND_OR" macro definition to your project build
// options under MPLAB C30 tab.
// Alternatively you can uncomment the line below.
/*****************************************************************************/
//#define USE_AND_OR


/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <string.h>
#include <spi.h>

#include "io.h"
#include "periph_spi.h"


/** D E F I N E S ************************************************************/
#define CS_Display_All			0x83FF
#define CS_Display1				0xFBFF
#define CS_Display2				0xF7FF
#define CS_Display3				0xEFFF
#define CS_Display4				0xDFFF
#define CS_Display5				0xBFFF

// Brightness Mode
#define BRIGHTNESS_LOW			0x00
#define BRIGHTNESS_MEDIUM		0x01
#define BRIGHTNESS_HIGH			0x02


/** V A R I A B L E S ********************************************************/


/** D E C L A R A T I O N S **************************************************/
void dispInit(void);
void dispBrightness(char Brightness);

void dispClearAll(void);

void dispClearLine1(void);
void dispPutLine1(const char *Text);
void dispPutLine1Left(const char *Text);
void dispPutLine1Right(const char *Text);

void dispClearLine2(void);
void dispPutLine2(const char *Text);

void dispClearLine3(void);
void dispPutLine3(const char *Text);


/** E N D   O F   F I L E ****************************************************/
#endif
