
/*
 * File:         mcp23s17
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/09
 * Description:  MCP23S17 I/O expander driver
 *
 * History :
 *
 *   2015/04/09  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include <p24fxxxx.h>
#include <PPS.h>

#include "mcp23s17.h"
#include "periph_spi.h"
#include "io.h"


/*****************************************************************************/


// HAL
//


static void spi1open( void )
{
  spi1_open(0);
}


static UINT8 Spi1PutGet( UINT8 dataOut )
{
  return spi1_write(dataOut);
}


static void assert_cs(void)
{
  FP_nCS = 0;
}


static void deassert_cs(void)
{
  FP_nCS = 1;
}


/*****************************************************************************/


#define CHIP_ADDR_BASE         0x40
#define SPI_WR                 0x00
#define SPI_RD                 0x01

// Register bank offset (to be OR-ed with register map)
// In this driver, we assume that IOCON.BANK = 0
#define BANK_B                 0x01

/* Register map */

#define REG_IODIR              0x00
#define REG_IPOL               0x02
#define REG_GPINTEN            0x04
#define REG_DEFVAL             0x06
#define REG_INTCON             0x08
#define REG_IOCON              0x0A
#define REG_GPPU               0x0C
#define REG_INTF               0x0E
#define REG_INTCAP             0x10
#define REG_GPIO               0x12
#define REG_OLAT               0x14


/*****************************************************************************/


typedef struct
{
	union
	{
		UINT8  uint8[2];
		UINT16 uint16;
	} gpio;
	union
	{
		UINT8  uint8[2];
		UINT16 uint16;
	} tris;
} MCP23S17_SHADOW;

MCP23S17_SHADOW		mcp23s17;


/*****************************************************************************/


static void  mcp23s17_write( UINT8 reg, UINT8 data );
//static UINT8 mcp23s17_read( UINT8 reg);
UINT8 mcp23s17_read( UINT8 reg);


/*****************************************************************************/


int mcp23s17_init(void)
{
	UINT8 a;
	
	spi1open();
	
	// Set default configuration: BANK=0, Hardware Address enabled
	mcp23s17_write( REG_IOCON , 0x08 );
	
	// Read back register to test chip presence
	a= mcp23s17_read( REG_IOCON );

  // Reflect default chip config
  mcp23s17.tris.uint16 = 0xFFFF;
  mcp23s17.gpio.uint16 = 0x0000;

	if (a != 0x08)
		return -1;

  return 0;
}


void mcp23s17_gpio_set( UINT16 pin_mask )
{
	MCP23S17_SHADOW		temp;

	// Setup gpio
	temp.gpio.uint16 = mcp23s17.gpio.uint16 | pin_mask;

	// Write register
	if( mcp23s17.gpio.uint8[0] != temp.gpio.uint8[0] )
	{
		mcp23s17_write( REG_GPIO, temp.gpio.uint8[0] );
		mcp23s17.gpio.uint8[0] = temp.gpio.uint8[0];
	}
	if( mcp23s17.gpio.uint8[1] != temp.gpio.uint8[1] )
	{
		mcp23s17_write( REG_GPIO | BANK_B, temp.gpio.uint8[1] );
		mcp23s17.gpio.uint8[1] = temp.gpio.uint8[1];
	}
}


void mcp23s17_gpio_clr( UINT16 pin_mask )
{
	MCP23S17_SHADOW		temp;

	// Setup gpio
	temp.gpio.uint16 = mcp23s17.gpio.uint16 & (~pin_mask);

	// Write register
	if( mcp23s17.gpio.uint8[0] != temp.gpio.uint8[0] )
	{
		mcp23s17_write( REG_GPIO, temp.gpio.uint8[0] );
		mcp23s17.gpio.uint8[0] = temp.gpio.uint8[0];
	}
	if( mcp23s17.gpio.uint8[1] != temp.gpio.uint8[1] )
	{
		mcp23s17_write( REG_GPIO | BANK_B, temp.gpio.uint8[1] );
		mcp23s17.gpio.uint8[1] = temp.gpio.uint8[1];
	}
}


void mcp23s17_gpio_clr_shadow( UINT16 pin_mask )
{
	mcp23s17.gpio.uint16 = mcp23s17.gpio.uint16 & (~pin_mask);
}


void mcp23s17_tris_set( UINT16 pin_mask )
{
	MCP23S17_SHADOW		temp;

	// Setup tris
	temp.tris.uint16 = mcp23s17.tris.uint16 | pin_mask;

	// Write register
	if( mcp23s17.tris.uint8[0] != temp.tris.uint8[0] )
	{
		mcp23s17_write( REG_IODIR, temp.tris.uint8[0] );
		mcp23s17.tris.uint8[0] = temp.tris.uint8[0];
	}
	if( mcp23s17.tris.uint8[1] != temp.tris.uint8[1] )
	{
		mcp23s17_write( REG_IODIR | BANK_B, temp.tris.uint8[1] );
		mcp23s17.tris.uint8[1] = temp.tris.uint8[1];
	}
}

void mcp23s17_tris_clr( UINT16 pin_mask )
{
	MCP23S17_SHADOW		temp;

	// Setup tris
	temp.tris.uint16 = mcp23s17.tris.uint16 & (~pin_mask);

	// Write register
	if( mcp23s17.tris.uint8[0] != temp.tris.uint8[0] )
	{
		mcp23s17_write( REG_IODIR, temp.tris.uint8[0] );
		mcp23s17.tris.uint8[0] = temp.tris.uint8[0];
	}
	if( mcp23s17.tris.uint8[1] != temp.tris.uint8[1] )
	{
		mcp23s17_write( REG_IODIR | BANK_B, temp.tris.uint8[1] );
		mcp23s17.tris.uint8[1] = temp.tris.uint8[1];
	}
}


unsigned int mcp23s17_gpio_get( void )
{
	MCP23S17_SHADOW		temp;
	temp.gpio.uint8[0] = mcp23s17_read( REG_GPIO          );
	temp.gpio.uint8[1] = mcp23s17_read( REG_GPIO | BANK_B );
	return temp.gpio.uint16;
}


/*****************************************************************************/


static void mcp23s17_write( UINT8 reg, UINT8 data )
{
  assert_cs();
	Spi1PutGet( CHIP_ADDR_BASE | SPI_WR );
	Spi1PutGet( reg );
	Spi1PutGet( data );
  deassert_cs();
}

//static UINT8 mcp23s17_read( UINT8 reg)
UINT8 mcp23s17_read( UINT8 reg)
{
  assert_cs();
	Spi1PutGet( CHIP_ADDR_BASE | SPI_RD );
	Spi1PutGet( reg );
	reg = Spi1PutGet( 0x00 );
  deassert_cs();
	return reg;
}


/*****************************************************************************/
