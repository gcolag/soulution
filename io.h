
/*
 * File:         io
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Inputs/Outputs configuration
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __IO_H__
#define __IO_H__

/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <inCap.h>
#include <ports.h>

#include "variables.h"
#include "config_mgr.h"
#include "config_boards.h"
#include "periph_timer.h"
#include "cmd_mgr.h"
#include "comparator.h"


/** D E F I N E S ************************************************************/
#define IO_OUTPUT				0
#define IO_INPUT				1

/*
// Inputs-Outputs
#define VOLTAGE_ERROR			PORTEbits.RE0==0
#define PSU_ENABLE				LATEbits.LATE1
*/
#define PSU_OFF					0
#define PSU_ON					1

/*
#define LED_CMD					LATBbits.LATB6
*/
#define LED_OFF					0
#define LED_ON					1

/*
#define DISP_NRST				LATBbits.LATB15

#define OVERCURRENT_ERROR		PORTAbits.RA14==0

#define TRIG_IN					PORTGbits.RG12
#define PRE_MUTE				LATGbits.LATG13
#define LINK_ON					LATGbits.LATG14
#define LINK_FEEDBACK			PORTGbits.RG9
*/

#define TIME_OUT_VOLTAGE		2000


/*****************************************************************************/


#define TRIG_IN              PORTBbits.RB0
#define PRE_MUTE             LATBbits.LATB1
#define LINK_ON              LATBbits.LATB3
#define LINK_FEEDBACK        PORTBbits.RB2

#define PSU_POWER_ON         LATDbits.LATD12
#define PSU_AUDIO_ON         LATAbits.LATA9

#define STATUS_LED           LATBbits.LATB12

#define SW_1                 PORTDbits.RD7
#define SW_2                 PORTFbits.RF0
#define SW_3                 PORTGbits.RG0
#define SW_4                 PORTGbits.RG1
#define SW_5                 PORTFbits.RF1

#define FP_ROTA_A            PORTBbits.RB14
//#define FP_ROTA_B            PORTAbits.RA9
#define FP_ROTA_B            PORTCbits.RC3
#define FP_ROTA_PUSH         PORTAbits.RA10

#define FP_ROTB_A            PORTAbits.RA15
#define FP_ROTB_B            PORTAbits.RA14
#define FP_ROTB_PUSH         PORTDbits.RD6

#define IR_PIN               PORTCbits.RC1

// SPI 3 USB_LAN
//
#define USB_LAN_NMR_nCS      LATGbits.LATG9
#define USB_LAN_CPLD_nCS     LATEbits.LATE8
#define USB_LAN_nINT         PORTEbits.RE9

// SPI 2 CPLD/S8/DIR
//
#define S8_nCS               LATDbits.LATD11
#define DIR_nCS              LATDbits.LATD0
#define CPLD_nCS             LATCbits.LATC14
#define DSP_nINT             PORTCbits.RC13

//#define S8_nIRQ              PORTAbits.RA9

// SPI 1 FP
//
#define FP_nRST              LATBbits.LATB15
#define FP_nCS               LATDbits.LATD14

#define FLASH_nCS            LATDbits.LATD13

#define DAC_nINT             PORTDbits.RD1

#define led_cmd_on()                 STATUS_LED = 1
#define led_cmd_off()                STATUS_LED = 0

#define psu_enable()                 PSU_POWER_ON = 1
#define psu_disable()                PSU_POWER_ON = 0

#define psu_audio_enable()           PSU_AUDIO_ON = 1
#define psu_audio_disable()          PSU_AUDIO_ON = 0

#define assert_flash_cs()            FLASH_nCS = 0
#define deassert_flash_cs()          FLASH_nCS = 1

#define get_key1()                   SW_1
#define get_key2()                   SW_2
#define get_key3()                   SW_3
#define get_key4()                   SW_4
#define get_key5()                   SW_5

#define get_rotA_A()                 FP_ROTA_A
#define get_rotA_B()                 FP_ROTA_B
#define get_encoder_A_push_button()  FP_ROTA_PUSH

#define get_rotB_A()                 FP_ROTB_A
#define get_rotB_B()                 FP_ROTB_B
#define get_encoder_B_push_button()  FP_ROTB_PUSH

#define deassert_fp_reset()          FP_nRST = 1

#define get_dac_int()                DAC_nINT

#define assert_usblan_cpld_cs()      USB_LAN_CPLD_nCS = 0
#define deassert_usblan_cpld_cs()    USB_LAN_CPLD_nCS = 1

#define link_get_trig_in()           TRIG_IN
#define link_assert_pre_mute()       PRE_MUTE = 1
#define link_deassert_pre_mute()     PRE_MUTE = 0
#define link_assert_link_on()        LINK_ON = 1
#define link_deassert_link_on()      LINK_ON = 0
#define link_get_link_feedback()     LINK_FEEDBACK

/** E X T E R N S ************************************************************/
extern unsigned int timeout_overcurrent_inhibited;


/** D E C L A R A T I O N S **************************************************/
void configIO(void);
void standbyIO(void);
void config_IO_interrupts (void);
void close_IO_interrupts (void);
char test_voltage(void);
char test_overcurrent(void);
unsigned char getTrigger(void);
unsigned char usbDetect(void);

void md_assert_cs(void);
void md_deassert_cs(void);

void io_disable_cn_int(void);
void io_enable_cn_int(void);


/** E N D   O F   F I L E ****************************************************/
#endif
