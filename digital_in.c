
/*
 * File:         digital_in
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Digital inputs management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include "cs8416.h"
#include "digital_in.h"


/** V A R I A B L E S ********************************************************/
unsigned char 			clock_Rate = CLOCKERROR;
char 					DAC_status = STATE_DAC_INIT;
static unsigned char 	timeCheckWM8805;


/** F U N C T I O N S ********************************************************/
/*****************************************************************************
 * manageDigitalInput
 *
 * overview		:	This function manages the DIR in DAC mode
 *					
 ****************************************************************************/
void manageDigitalInput (void)
{
	switch (DAC_status)
	{
		case STATE_DAC_INIT:
//			WM8805_status = WM8805_get_status();
			
			// Detect falling edge / locking on DIR interrupt
			DAC_status = STATE_DAC_UNLOCKED;
			timeCheckWM8805 = readTimerCounter();
			break;
			
		case STATE_DAC_UNLOCKED:
/*
			if ((WM8805_INT_PIN == 0) || (readTimerDiff(timeCheckWM8805) > 1000))
			{
				WM8805_status = WM8805_get_status();
				timeCheckWM8805 = readTimerCounter();
			}
			
			if (WM8805_status > FS_176_192)
				clock_Rate = CLOCKERROR;
			else
			{
				clock_Rate = WM8805_status;
				DAC_status = STATE_DAC_LOCKED;
			}
*/
    {
      unsigned short cs8416_status;
      int locked;

      cs8416_get_status(&cs8416_status);
      locked = (cs8416_status & 0x0080) != 0;

      if (locked)
				DAC_status = STATE_DAC_LOCKED;

			break;
    }
		case STATE_DAC_LOCKED:
			DataValidSonic2();
			DAC_status = STATE_DAC_PLAYING;	
			break;
			
		case STATE_DAC_PLAYING:
/*
			// Check the INT pin status
			if (WM8805_check_int_pin() != WM8805_INT_LOW)
			{
				DataNotValidSonic2();
				DAC_status = STATE_DAC_INIT;
			}
*/
    {
      unsigned short cs8416_status;
      int locked;

      cs8416_get_status(&cs8416_status);
      locked = (cs8416_status & 0x0080) != 0;

      if (!locked)
			{
				DataNotValidSonic2();
				DAC_status = STATE_DAC_INIT;
			}

			break;
    }
		default:
			break;
	}
}


/** E N D   O F   F I L E ****************************************************/
