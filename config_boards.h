
/*
 * File:         config_boards
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  I/O expanders control (mostly)
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __CONFIG_BOARDS_H
#define __CONFIG_BOARDS_H

/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <i2c.h>

#include "variables.h"
#include "PCM1792.h"
#include "periph_timer.h"
#include "config_mgr.h"
#include "pre_amp.h"
#include "io.h"
#include "WM8805.h"
#include "digital_in.h"
#include "SCF5742.h"
#include "sonic2.h" 

#include "dacbio.h"


/** D E F I N E S ************************************************************/
// Address of the I/O expander
#define ADDRESS_PCA9554_FPGA			(unsigned char)0x70
#define ADDRESS_PCA9554_STR				(unsigned char)0x72
#define ADDRESS_PCA9554_DAC				(unsigned char)0x74

#define CLEAR							(unsigned char)0x00
#define SET 							(unsigned char)0x01
#define TOGGLE							(unsigned char)0x02


#define MSK_OUTPUT_SPARE3				(unsigned char)0x80
#define MSK_OUTPUT_SPARE2				(unsigned char)0x40
#define MSK_OUTPUT_SPARE1				(unsigned char)0x20


/*
 * SPARE1 & SPARE3 lines are inverted on 106_11_300_0 board
 * compared to 106_03_200.
 */
#define INVERT_SPARE1_SPARE3_FOR_USB_LAN_MEZANINE


/*****************************************************************************/
#if 0
/*****************************************************************************/


#if defined(INVERT_SPARE1_SPARE3_FOR_USB_LAN_MEZANINE)
# define MSK_OUTPUT_CS_MEZ_IO_EXP  	MSK_OUTPUT_SPARE1
#else
# define MSK_OUTPUT_CS_MEZ_IO_EXP  	MSK_OUTPUT_SPARE3
#endif
#define SelectMezIOExp()     BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_OUTPUT_CS_MEZ_IO_EXP, PCA9554_CLEAR)
#define UnselectMezIOExp()   BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_OUTPUT_CS_MEZ_IO_EXP, PCA9554_SET)


#if defined(INVERT_SPARE1_SPARE3_FOR_USB_LAN_MEZANINE)
# define MSK_INPUT_USB_POWER			MSK_OUTPUT_SPARE3
#else
# define MSK_INPUT_USB_POWER			MSK_OUTPUT_SPARE1
#endif
#define GetUsbPower()     (ReadPCA9554(ADDRESS_PCA9554_STR, INPUT_REGISTER_PCA9554) & MSK_INPUT_USB_POWER ? 0 : 1);

#define MSK_OUTPUT_NMR_nUSB			MSK_OUTPUT_SPARE2
//#define SelectNetworkStreaming()  BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_OUTPUT_SPARE2, PCA9554_SET)
//#define SelectUSBStreaming()			BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_OUTPUT_SPARE2, PCA9554_CLEAR)

#define MSK_OUTPUT_SPDIF				(unsigned char)0x04
#define EnableOutputSPDIF()				BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_OUTPUT_SPDIF, PCA9554_SET)
#define DisableOutputSPDIF()			BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_OUTPUT_SPDIF, PCA9554_CLEAR)

#define MSK_OUTPUT_AES_EBU				(unsigned char)0x02
#define EnableOutputAES_EBU()			BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_OUTPUT_AES_EBU, PCA9554_SET)
#define DisableOutputAES_EBU()			BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_OUTPUT_AES_EBU, PCA9554_CLEAR)

#define MSK_OUTPUT_TOSLINK				(unsigned char)0x01
#define EnableOutputTOSLINK()			BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_OUTPUT_TOSLINK, PCA9554_CLEAR)
#define DisableOutputTOSLINK()			BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_OUTPUT_TOSLINK, PCA9554_SET)

#define MSK_RST_DIR						(unsigned char)0x08
#define EnableDIR()						BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_RST_DIR, PCA9554_SET)
#define ResetDIR()						BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_RST_DIR, PCA9554_CLEAR)

#define MSK_RST_MEZZANINE				(unsigned char)0x10
#define EnableMezzanine()				BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_RST_MEZZANINE, PCA9554_SET)
#define ResetMezzanine()				BitSetPCA9554(ADDRESS_PCA9554_STR, MSK_RST_MEZZANINE, PCA9554_CLEAR)

#define MSK_LED_DIGITAL					(unsigned char)0x01
#define LedDigitalOn()					BitSetPCA9554(ADDRESS_PCA9554_DAC, MSK_LED_DIGITAL, PCA9554_SET)
#define LedDigitalOff()					BitSetPCA9554(ADDRESS_PCA9554_DAC, MSK_LED_DIGITAL, PCA9554_CLEAR)

#define MSK_RST_PCM1792					(unsigned char)0x04
#define EnablePCM1792()					BitSetPCA9554(ADDRESS_PCA9554_DAC, MSK_RST_PCM1792, PCA9554_SET)
#define ResetPCM1792()					BitSetPCA9554(ADDRESS_PCA9554_DAC, MSK_RST_PCM1792, PCA9554_CLEAR)

#define MSK_OUTPUT_RELAY				(unsigned char)0x08
#define EnableOutput()     				BitSetPCA9554(ADDRESS_PCA9554_DAC,MSK_OUTPUT_RELAY,PCA9554_SET)
#define DisableOutput()					BitSetPCA9554(ADDRESS_PCA9554_DAC,MSK_OUTPUT_RELAY,PCA9554_CLEAR)

/*
#define MSK_RST_SONIC2					(unsigned char)0x20
#define EnableSonic2()					BitSetPCA9554(ADDRESS_PCA9554_DAC, MSK_RST_SONIC2, PCA9554_SET)
#define ResetSonic2()					BitSetPCA9554(ADDRESS_PCA9554_DAC, MSK_RST_SONIC2, PCA9554_CLEAR)
*/

#define MSK_DATA_VALID_SONIC2			(unsigned char)0x01
#define DataValidSonic2()				BitSetPCA9554(ADDRESS_PCA9554_FPGA, MSK_DATA_VALID_SONIC2, PCA9554_CLEAR)
#define DataNotValidSonic2()			BitSetPCA9554(ADDRESS_PCA9554_FPGA, MSK_DATA_VALID_SONIC2, PCA9554_SET)

#define MSK_LISTENINPUT					(unsigned char)0x06
#define LIS_CD_SACD						(unsigned char)0x00
#define LIS_RECEIVER					(unsigned char)0x02
#define LIS_MEZZANINE					(unsigned char)0x04
#define ChooseListenCDPlayer()			BitMaskPCA9554(ADDRESS_PCA9554_FPGA, MSK_LISTENINPUT, LIS_CD_SACD)
#define ChooseListenReceiver()			BitMaskPCA9554(ADDRESS_PCA9554_FPGA, MSK_LISTENINPUT, LIS_RECEIVER)
#define ChooseListenMezzanine()			BitMaskPCA9554(ADDRESS_PCA9554_FPGA, MSK_LISTENINPUT, LIS_MEZZANINE)

#define MSK_RECORDINPUT					(unsigned char)0x18
#define REC_CD_SACD						(unsigned char)0x00
#define REC_RECEIVER					(unsigned char)0x08
#define REC_MEZZANINE					(unsigned char)0x10
#define ChooseRecordCDPlayer()			BitMaskPCA9554(ADDRESS_PCA9554_FPGA, MSK_RECORDINPUT, REC_CD_SACD)
#define ChooseRecordReceiver()			BitMaskPCA9554(ADDRESS_PCA9554_FPGA, MSK_RECORDINPUT, REC_RECEIVER)
#define ChooseRecordMezzanine()			BitMaskPCA9554(ADDRESS_PCA9554_FPGA, MSK_RECORDINPUT, REC_MEZZANINE)

#define MSK_PHASEPOLARITY				(unsigned char)0x20
#define ChooseNormalPhase()				BitSetPCA9554(ADDRESS_PCA9554_FPGA, MSK_PHASEPOLARITY, PCA9554_SET)
#define ChooseReversePhase()			BitSetPCA9554(ADDRESS_PCA9554_FPGA, MSK_PHASEPOLARITY, PCA9554_CLEAR)

#define MSK_DATA_FORMAT					(unsigned char)0x40
#define DataFormatDSD()					BitSetPCA9554(ADDRESS_PCA9554_FPGA, MSK_DATA_FORMAT, PCA9554_CLEAR)
#define DataFormatPCM()					BitSetPCA9554(ADDRESS_PCA9554_FPGA, MSK_DATA_FORMAT, PCA9554_SET)


/*****************************************************************************/
#else
/*****************************************************************************/

#define nop()

// Chip select for USB_LAN board FPGA
#define SelectMezIOExp()            assert_usblan_cpld_cs()
#define UnselectMezIOExp()          deassert_usblan_cpld_cs()

#define GetUsbPower()              !dbio_get_usb_bus_detect()


#define SelectNetworkStreaming()    dbio_select_nmr()
#define SelectUSBStreaming()        dbio_select_usb()


// placeholder ; no more enable for this output
#define EnableOutputSPDIF()         nop()
#define DisableOutputSPDIF()        nop()

// placeholder ; no more enable for this output
#define EnableOutputAES_EBU()       nop()
#define DisableOutputAES_EBU()      nop()

// placeholder ; no more enable for this output
#define EnableOutputTOSLINK()       nop()
#define DisableOutputTOSLINK()      nop()

#define EnableDIR()                 dbio_deassert_dir_reset()
#define ResetDIR()                  dbio_assert_dir_reset()

#define EnableMezzanine()           dbio_deassert_usb_lan_reset()
#define ResetMezzanine()            dbio_assert_usb_lan_reset()


// placeholder ; no more LED on DSP board (replaces DIGITAL board)
#define LedDigitalOn()              nop()
#define LedDigitalOff()             nop()

#define EnablePCM1792()             dacbio_deassert_rst()
#define ResetPCM1792()              dacbio_assert_rst()

#define EnableOutput()              dacbio_deassert_main_mute()
#define DisableOutput()             dacbio_assert_main_mute()

#define DataValidSonic2()           dbio_s8_disable_forced_invalid_mode()
#define DataNotValidSonic2()        dbio_s8_force_invalid_mode()

#define ChooseListenReceiver()      dbio_select_dir()
#define ChooseListenMezzanine()     dbio_select_usblan()

#define ChooseRecordReceiver()
#define ChooseRecordMezzanine()

#define ChooseNormalPhase()
#define ChooseReversePhase()

#define DataFormatDSD()
#define DataFormatPCM()


/*****************************************************************************/
#endif
/*****************************************************************************/



/** D E C L A R A T I O N S **************************************************/
char initBoards (void);
void unlockI2C (void);
void configPhasePolarity (unsigned char phasePolarity);
char configListen (void);
void configRecord (void);


/** E N D   O F   F I L E ****************************************************/
#endif
