
/*
 * File:         log
 * Based on:     
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 *
 * Created:      2015/06/04
 * Description:  Message loggin facilities
 *
 * History :
 *
 *   2015/06/04  1.0  Initial release
 *
 */

/*****************************************************************************/

#ifndef __LOG_H__
#define __LOG_H__

/*****************************************************************************/


#include <stdarg.h>


/*****************************************************************************/


typedef void (*write_type)(char*, int);


/*****************************************************************************/


void log_init(write_type write_func);

void log_msg(char* msg);

void log_msg_fmt(char* format, ...);


/*****************************************************************************/

#endif // __LOG_H__
