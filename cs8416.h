
/*
 * File:         cs8416
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/18
 * Description:  CS8416 digital receiver driver
 *
 * History :
 *
 *   2015/04/18  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef	__CS8416_H
#define	__CS8416_H

/*****************************************************************************/


int cs8416_write(unsigned char reg, unsigned char data);
int cs8416_read(unsigned char reg, unsigned char* data);


/*****************************************************************************/


#define BIT00  			0x01
#define BIT01  			0x02
#define BIT02  			0x04
#define BIT03  			0x08
#define BIT04  			0x10
#define BIT05  			0x20
#define BIT06  			0x40
#define BIT07  			0x80


/*****************************************************************************/


// Enable automatic clock switching on PLL unlock. 
// OMCK clock input is automatically output on RMCK on PLL Unlock
#define CS8416_CFG_AUTO_CLOCK_SWITCHING    BIT00

// RMCK output frequency is 128*FS
#define CS8416_CFG_RMCK_256FS              0
#define CS8416_CFG_RMCK_128FS              BIT01

#define CS8416_CFG_AUDIO_SLAVE             0
#define CS8416_CFG_AUDIO_MASTER            BIT02

#define CS8416_CFG_AUDIO_LJ                0
#define CS8416_CFG_AUDIO_I2S               BIT03

// INT mode
#define CS8416_CFG_INT_ACTIVE_HIGH         0
#define CS8416_CFG_INT_ACTIVE_LOW          BIT04
#define CS8416_CFG_INT_OPEN_DRAIN          BIT05

/**
 * Setup a specified receiver
 * After setup, the device is in power down mode. 
 */
int cs8416_init(void);


/*****************************************************************************/


int cs8416_get_chip_revision(unsigned char * rev);


/*****************************************************************************/


typedef enum
{
	
	CS8416_INPUT_RXP0 = 0,
	CS8416_INPUT_RXP1,
	CS8416_INPUT_RXP2,
	CS8416_INPUT_RXP3,
	CS8416_INPUT_RXP4,
	CS8416_INPUT_RXP5,
	CS8416_INPUT_RXP6,
	CS8416_INPUT_RXP7,
	CS8416_INPUT_NONE
} CS8416_INPUT;

int cs8416_select_input(CS8416_INPUT input);


/*****************************************************************************/


int cs8416_write(unsigned char reg, unsigned char data);
int cs8416_read(unsigned char reg, unsigned char* data);


/*****************************************************************************/


/** 
 * Returns digital receiver status
 * Note: bits 6..3 are mutually exclusive
 * bits 8..15: input frequency ratio, as a 2.6 format (2-bit integer + 6-bit fractional part)
 *             ratio is defined as InFs / 96k
 *             value is meaningful only after the PLL has reached lock
 * bit7: locked (1) / unlocked (0)
 * bit6: PCM
 * bit5: IEC61937
 * bit4: DTS_LD
 * bit3: DTS_CD
 * bit2: reserved
 * bit1: digital silence
 * bit0: reserved
 */
int cs8416_get_status(unsigned short* status);


/*****************************************************************************/

#endif /* __CS8416_H */

