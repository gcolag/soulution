
/*
 * File:         rs232
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  RS232 control management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#ifndef __RS232_H__
#define __RS232_H__


/** I N C L U D E S **********************************************************/


#include <p24fxxxx.h>
#include <uart.h>
#include <string.h>

#include "variables.h"
#include "io.h"
#include "periph_timer.h"
#include "keys.h"						// For keys definition


/** D E F I N E S ************************************************************/
#define RS232_IDLE				0
#define RS232_RECEIVING			1

#define RS232_DISABLE_TX		LATBbits.LATB8=0
#define RS232_ENABLE_TX			LATBbits.LATB8=1

#define RS232_CTS				PORTEbits.RE3		// (PIC)RTS# <= (PC)REMOTE_CTS#
#define RS232_RTS				LATEbits.LATE4		// (PIC)CTS# => (PC)REMOTE_RTS#

#define RS232_DISABLE_RX		RS232_RTS=1
#define RS232_ENABLE_RX			RS232_RTS=0

#define RS232_TX_TIMEOUT		2000				// Delay in ms to wait for CTS#
#define RS232_RX_TIMEOUT		500					// Delay (not in ms!) before stopping receiving
													// Min. working with PC is 300 -> 500 is safer
													// If value is too small, only the 1st character is received

#define RS232_RX_BUFFER_SIZE	50


/** E X T E R N S ************************************************************/
extern unsigned int rs232_timeout;
extern unsigned char rs232_byte_arrived;
extern unsigned char rs232_data_arrived;
extern unsigned char rs232_state;


/** D E C L A R A T I O N S **************************************************/
void initRS232 (void);
void stopRS232 (void);
void sendRS232 (char *Text);
void receiveRS232 (unsigned char mode);
void readRS232 (char * str);


/** E N D   O F   F I L E ****************************************************/
#endif
