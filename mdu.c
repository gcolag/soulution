
/*
 * File:         mdu
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Module DSP Update
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include <stdio.h>
#include <string.h>

#include "stdtypes.h"
#include "mdu.h"

#include "log.h"
#include "SCF5742.h"


/*****************************************************************************/


#define DAC760_MAGIC 0x00DAC760


/*****************************************************************************/


static mdu_ops_type mdu_ops_var, *mdu_ops = &mdu_ops_var;


#define	SONIC_SPI_WR                            0x10
#define	SONIC_SPI_RD                            0x00


static unsigned char reg_read(unsigned char addr)
{
	unsigned char rxBuffer;

	mdu_ops->assert_cs();
	mdu_ops->spi_read_write(addr | SONIC_SPI_RD);
	mdu_ops->delay_ms(1);
	rxBuffer = mdu_ops->spi_read_write(0x00);
	mdu_ops->deassert_cs();
	
	return rxBuffer;
}

static void reg_write(unsigned char addr, unsigned char data)
{
	mdu_ops->assert_cs();
	mdu_ops->spi_read_write(addr | SONIC_SPI_WR);
	mdu_ops->delay_ms(1);
	mdu_ops->spi_read_write(data);
	mdu_ops->deassert_cs();
}

void spi_write_byte(unsigned char data)
{
  mdu_ops->assert_cs();
  mdu_ops->spi_read_write(data);
  mdu_ops->deassert_cs();
}

#define APP_SPI_REG_FIRMWARE_UPDATE             0x0D
#define APP_SPI_REG_FIRMWARE_UPDATE_STATUS      0x0E

#define BOOTLOADER_CMD_START_UPDATE             0x1

#define REG_SWR_REV               0x06
#define REG_PRD_ID                0x07
#define REG_SWR_REV_MINOR         0x08
#define REG_PRD_SID               0x09

#define REG_SWR_REV_MINOR         0x08


/*****************************************************************************/


// Returns only once nIRQ is high
//
static int gpio_get_irq(void)
{
  while (!mdu_ops->get_irq()) ;

  return 0;
}


/*****************************************************************************/


static int fwupdt_send_bytes(char* buff, int len)
{
  int i;

  for (i=0 ; i<len ; i++)
  {
    gpio_get_irq();
    spi_write_byte(buff[i]);
  }

  return 0;
}


/*****************************************************************************/
/*
 * Blind/unblind stuffs
 */


static uint32_t cipher_iv          = 0x435abfe9;
static uint32_t master_cipher_key  = 0x73fba333;
static int cipher_key_set_index;
static uint32_t prev_cipher_in;
static uint32_t prev_cipher_out;
static int first_call;


static void cipher_init(void)
{
  cipher_key_set_index = 0;
  first_call = 1;
}


static uint32_t cipher(uint32_t data_in, uint32_t key)
{
  uint32_t data_out;
  uint32_t key_set[] = { 0xffb492aa,
                         0x12938abc,
                         0x49b33bbb,
                         0x452999ae,
                         0xc7376259,
                         0x39ddbc79,
                         0xcf984b2b,
                         0xaacdfdea,
                         0x49334412,
                         0x37f43ff1 };
  int key_set_size;
  uint32_t cipher_key;
                           
  key_set_size = sizeof(key_set) / sizeof(uint32_t);

  cipher_key = key ^ (key_set[cipher_key_set_index] + 1);
  data_out = data_in ^ cipher_key;

  cipher_key_set_index = (cipher_key_set_index + 1) % key_set_size;

  return data_out;
}


static uint32_t load32(unsigned char* data)
{
  uint32_t res;

  res = 
    (((uint32_t)data[3]) << 24) |
    (((uint32_t)data[2]) << 16) |
    (((uint32_t)data[1]) << 8) |
    (((uint32_t)data[0]) << 0) ;

  return res;
}


static void save32(unsigned char* data_out, uint32_t data_in)
{
  data_out[0] = (unsigned char)(data_in >> 0);
  data_out[1] = (unsigned char)(data_in >> 8);
  data_out[2] = (unsigned char)(data_in >> 16);
  data_out[3] = (unsigned char)(data_in >> 24);
}


static void blind(unsigned char* data, int byte_len)
{
	uint32_t count;
  unsigned char* addr;
  unsigned char* dest;
  uint32_t cipher_in;
  uint32_t cipher_out;
	int	size;

  addr     = data;
  dest     = data;
  size     = 4;
  count = byte_len / size;

  if (first_call)
  {
    cipher_init();
    prev_cipher_out = cipher_iv;
    first_call = 0;
  }

	while (count-- > 0) 
  {
    cipher_in  = prev_cipher_out ^ load32(addr);
    cipher_out = cipher(cipher_in, master_cipher_key);
    prev_cipher_out = cipher_out;
    save32(dest, cipher_out);

		addr += size;
		dest += size;
	}
}


static void unblind(unsigned char* data, int byte_len)
{
	uint32_t count;
  unsigned char* addr;
  unsigned char* dest;
  uint32_t cipher_in;
  uint32_t cipher_out;
  uint32_t cipher_out_2;
	int	size;

  addr     = data;
  dest     = data;
  size     = 4;
  count = byte_len / size;

  if (first_call)
  {
    cipher_init();
    prev_cipher_in = cipher_iv;
    first_call = 0;
  }

	while (count-- > 0) 
  {
    cipher_in  = load32(addr);
    cipher_out = cipher(cipher_in, master_cipher_key);
    cipher_out_2 = (prev_cipher_in ^ cipher_out);
    save32(dest, cipher_out_2);
    prev_cipher_in = cipher_in;

		addr += size;
		dest += size;
	}
}


static void blind_unblind(unsigned char* data, int blk_size, int mode)
{
  if (mode == 0)
  {
    blind(data, blk_size);
  }
  else
  {
    unblind(data, blk_size);
  }
}


/*****************************************************************************/
/*
 * CRC-32 stuffs
 */

static const uint32_t crc_32_tab[] =
{
  0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL, 0x076dc419L,
  0x706af48fL, 0xe963a535L, 0x9e6495a3L, 0x0edb8832L, 0x79dcb8a4L,
  0xe0d5e91eL, 0x97d2d988L, 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L,
  0x90bf1d91L, 0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL,
  0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L, 0x136c9856L,
  0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL, 0x14015c4fL, 0x63066cd9L,
  0xfa0f3d63L, 0x8d080df5L, 0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L,
  0xa2677172L, 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
  0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L, 0x32d86ce3L,
  0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L, 0x26d930acL, 0x51de003aL,
  0xc8d75180L, 0xbfd06116L, 0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L,
  0xb8bda50fL, 0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L,
  0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL, 0x76dc4190L,
  0x01db7106L, 0x98d220bcL, 0xefd5102aL, 0x71b18589L, 0x06b6b51fL,
  0x9fbfe4a5L, 0xe8b8d433L, 0x7807c9a2L, 0x0f00f934L, 0x9609a88eL,
  0xe10e9818L, 0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
  0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL, 0x6c0695edL,
  0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L, 0x65b0d9c6L, 0x12b7e950L,
  0x8bbeb8eaL, 0xfcb9887cL, 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L,
  0xfbd44c65L, 0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L,
  0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL, 0x4369e96aL,
  0x346ed9fcL, 0xad678846L, 0xda60b8d0L, 0x44042d73L, 0x33031de5L,
  0xaa0a4c5fL, 0xdd0d7cc9L, 0x5005713cL, 0x270241aaL, 0xbe0b1010L,
  0xc90c2086L, 0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
  0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L, 0x59b33d17L,
  0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL, 0xedb88320L, 0x9abfb3b6L,
  0x03b6e20cL, 0x74b1d29aL, 0xead54739L, 0x9dd277afL, 0x04db2615L,
  0x73dc1683L, 0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L,
  0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L, 0xf00f9344L,
  0x8708a3d2L, 0x1e01f268L, 0x6906c2feL, 0xf762575dL, 0x806567cbL,
  0x196c3671L, 0x6e6b06e7L, 0xfed41b76L, 0x89d32be0L, 0x10da7a5aL,
  0x67dd4accL, 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
  0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L, 0xd1bb67f1L,
  0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL, 0xd80d2bdaL, 0xaf0a1b4cL,
  0x36034af6L, 0x41047a60L, 0xdf60efc3L, 0xa867df55L, 0x316e8eefL,
  0x4669be79L, 0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L,
  0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL, 0xc5ba3bbeL,
  0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L, 0xc2d7ffa7L, 0xb5d0cf31L,
  0x2cd99e8bL, 0x5bdeae1dL, 0x9b64c2b0L, 0xec63f226L, 0x756aa39cL,
  0x026d930aL, 0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
  0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L, 0x92d28e9bL,
  0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L, 0x86d3d2d4L, 0xf1d4e242L,
  0x68ddb3f8L, 0x1fda836eL, 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L,
  0x18b74777L, 0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL,
  0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L, 0xa00ae278L,
  0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L, 0xa7672661L, 0xd06016f7L,
  0x4969474dL, 0x3e6e77dbL, 0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L,
  0x37d83bf0L, 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
  0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L, 0xbad03605L,
  0xcdd70693L, 0x54de5729L, 0x23d967bfL, 0xb3667a2eL, 0xc4614ab8L,
  0x5d681b02L, 0x2a6f2b94L, 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL,
  0x2d02ef8dL
};


static uint32_t crc32_calculate(uint16_t *data, uint32_t len, uint32_t crc)
{
  uint32_t   crc_low;
  uint32_t   data_byte;
  uint32_t   byte_cnt;
  uint32_t   word_cnt;
  uint32_t   table_index;
  uint32_t   term1;
  uint32_t   term2;
  
  // Calculate CRC32 on a word array
  //
  for (byte_cnt = 0, word_cnt = 0;  byte_cnt < len;  byte_cnt ++)
  {
    // Get the upper or lower byte of data from the
    // word array
    //
    if ( byte_cnt & 0x0001 )
    {
      // Odd
      data_byte = data[word_cnt] >> 8;
      word_cnt++;
    }
    else
    {
      // Even
      data_byte = data[word_cnt];
    }

    // Calculate the first XOR term
    // Limit the table index to 256
    //
    crc_low = (uint32_t)crc;
    table_index = (crc_low ^ data_byte) & 0x00ff;
    term1 = crc_32_tab[table_index];

    // Calculate the second term
    // then update the CRC Maximal Length Code Register
    //
    term2 = crc >> 8;
    
    crc = term1 ^ term2;
  }
  
  return crc;
}


/*****************************************************************************/


int mdu_firmware_update(char* filename)
{
  int status;
  int32_t size_of_xfer;
  mdImageInfo image_info;

  unsigned char data_buffer[1024];
  int data_buffer_size = sizeof(data_buffer);
	uint32_t uCRC32 = 0;

  void* fd;
  int32_t firmware_size;
  int32_t header_size;

  int32_t ident;
  int32_t crc32_ref;
  int32_t magic;

  mdu_ops->report_status(MDU_START_PROCESS);

  memset(&image_info, 0, sizeof(mdImageInfo));

  if ((fd = mdu_ops->fopen(filename, "r")) == NULL)
  {
    status = -1;
    goto end;
  }

  firmware_size = mdu_ops->fgetsize(fd);
  mdu_ops->report_status(MDU_START_CHECK_FILE);

  // Calculate checksum with unblinding
  {
    int rlen;
    int32_t tlen;
    int progression;

    uCRC32 = 0;
    tlen = 0;
    cipher_init();

    rlen = mdu_ops->fread(fd, (unsigned char*)&ident, 4);
    tlen += rlen;
    rlen = mdu_ops->fread(fd, (unsigned char*)&crc32_ref, 4);
    tlen += rlen;
    rlen = mdu_ops->fread(fd, (unsigned char*)&magic, 4);
    tlen += rlen;
    header_size = tlen;

    while (1)
    {
      rlen = mdu_ops->fread(fd, data_buffer, data_buffer_size);
      blind_unblind(data_buffer, rlen, 1);

      progression = tlen / (firmware_size / 100);
      mdu_ops->progress_monitor(progression);

      if (rlen < 0)
      {
        mdu_ops->fclose(fd);
        status = -2;
        goto end;
      }
      uCRC32 = crc32_calculate((uint16_t *)data_buffer, rlen, uCRC32);
      tlen += rlen;
      if (tlen == firmware_size) break;

      if (rlen % 4)
      {
        log_msg_fmt("%i/%i", rlen, tlen);
        mdu_ops->delay_ms(3000);
      }
    }

    mdu_ops->frewind(fd);

    {
      uint32_t nDataLength;
      uint32_t nrPaddingBytes;
      unsigned char zero_padding_buffer[4];

      memset(zero_padding_buffer, 0, 4);
      nDataLength = firmware_size;
      nrPaddingBytes = nDataLength % 4;
      uCRC32 = crc32_calculate((uint16_t *)zero_padding_buffer,
                               nrPaddingBytes,
                               uCRC32);
    }
  }

  if (crc32_ref != uCRC32)
  {
    mdu_ops->report_status(MDU_WRONG_CRC);
    status = -1;
    goto end;
  }

  if (magic != DAC760_MAGIC)
  {
    mdu_ops->report_status(MDU_WRONG_MAGIC);
    status = -1;
    goto end;
  }

  image_info.nInfoSignature = BOOT_INFO_SIGNATURE;
  image_info.nImageChecksum = uCRC32;
  image_info.nImageLength   = firmware_size - header_size;

  size_of_xfer = sizeof(mdImageInfo) + image_info.nImageLength;

  // Cheat : print ERASING message a bit before so that there is
  // no delay visibile to the user between check and erase stages.
  //
  mdu_ops->report_status(MDU_ERASING);

  // Force firmware 0 (some firmware 1 don't have the update support)
  mdu_ops->assert_reset();
  mdu_ops->assert_cs();
  mdu_ops->delay_ms(100);
  mdu_ops->deassert_reset();
  mdu_ops->delay_ms(700);
  mdu_ops->deassert_cs();
  mdu_ops->delay_ms(1000);

  // Put Module in firmware update mode
  mdu_ops->md_force_invalid_mode();
  reg_write(APP_SPI_REG_FIRMWARE_UPDATE, BOOTLOADER_CMD_START_UPDATE);

  fwupdt_send_bytes((char*)&size_of_xfer, 4);
  fwupdt_send_bytes((char*)&image_info, sizeof(mdImageInfo));

  // Wait for flash to be erased
  mdu_ops->delay_ms(4000);

  mdu_ops->report_status(MDU_FLASHING);

  {
    int32_t rlen;
    int32_t tlen;

    tlen = 0;
    cipher_init();

    rlen = mdu_ops->fread(fd, (unsigned char*)&ident, 4);
    tlen += rlen;
    rlen = mdu_ops->fread(fd, (unsigned char*)&crc32_ref, 4);
    tlen += rlen;
    rlen = mdu_ops->fread(fd, (unsigned char*)&magic, 4);
    tlen += rlen;

    while (1)
    {
      rlen = mdu_ops->fread(fd, data_buffer, data_buffer_size);
      blind_unblind(data_buffer, rlen, 1);

      if (rlen < 0)
      {
        mdu_ops->fclose(fd);
        status = -3;
        goto end;
      }
      fwupdt_send_bytes((char*)&data_buffer, rlen);
      tlen += rlen;

      int progression;
      progression = tlen / (firmware_size / 100);
      mdu_ops->progress_monitor(progression);

      if (tlen == firmware_size) break;
    }
  }

  mdu_ops->fclose(fd);
  mdu_ops->delay_ms(100);
  status = 0;

  // Check Module status
  {
    int bootloader_status;

    bootloader_status = reg_read(APP_SPI_REG_FIRMWARE_UPDATE_STATUS);
    if (bootloader_status == 0)
      status = -4;
  }

  mdu_ops->assert_reset();
  mdu_ops->delay_ms(100);
  mdu_ops->deassert_reset();
  mdu_ops->delay_ms(700);

end:
  mdu_ops->md_disable_forced_invalid_mode();

  if (status == 0)
    mdu_ops->report_status(MDU_PROGRAMMING_SUCCESS);
  else
    mdu_ops->report_status(MDU_PROGRAMMING_FAILED);

  return status;
}


/*****************************************************************************/


void mdu_register(mdu_ops_type* p_mdu_ops)
{
  memcpy(mdu_ops, p_mdu_ops, sizeof(mdu_ops_type));
}


/*****************************************************************************/
