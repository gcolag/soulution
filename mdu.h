
/*
 * File:         mdu
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Module DSP Update
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __MDU_H
#define	__MDU_H

/*****************************************************************************/


#include "stdtypes.h"


/*****************************************************************************/


#define MODULE_DSP_FIRMWARE_FILENAME  "dac760md.bin"


/*****************************************************************************/


typedef struct mdu_ops_type {
  uint16_t (*spi_read_write)(uint16_t);

  void (*assert_cs)(void);
  void (*deassert_cs)(void);

  void (*assert_reset)(void);
  void (*deassert_reset)(void);

  int  (*get_irq)(void);

  void (*delay_ms)(unsigned int);

  void (*md_force_invalid_mode)(void);
  void (*md_disable_forced_invalid_mode)(void);

  void (*progress_monitor)(unsigned char);
  void (*report_status)(unsigned char);

  void* (*fopen)(char*, char*);
  void  (*fclose)(void*);
  int   (*fread)(void*, unsigned char*, int);
  int   (*frewind)(void*);
  int32_t (*fgetsize)(void*);
} mdu_ops_type;


#define BOOT_INFO_SIGNATURE  0x29a3167b
#define BOOT_INFO_VERSION_1  0x00000001
#define BOOT_INFO_VERSION_2  0x00000002


typedef struct _mdImageInfo
{
	/* Variables that describe boot info structure.  */
  uint32_t nInfoSignature;
  uint32_t nInfoVersion;
  uint32_t nInfoLength;
	
	/* Variables that describe the actual boot image.  */
  uint32_t nImageOffset;
  uint32_t nImageLength;
  uint32_t nImageChecksum;
	
	/* Variables that describe where the image is located.  */
  uint32_t nFlashAddress;
  uint32_t nFlashLength;
  uint32_t nFlashOffset;
	
	/* Variables reserved.  */
  uint32_t nReserved1;
  uint32_t nReserved2;
	
} mdImageInfo;


/*****************************************************************************/


#define MDU_PROGRAMMING_SUCCESS  0
#define MDU_PROGRAMMING_FAILED   1
#define MDU_START_PROCESS        2
#define MDU_START_CHECK_FILE     3
#define MDU_ERASING              4
#define MDU_FLASHING             5
#define MDU_WRONG_CRC            6
#define MDU_WRONG_MAGIC          7


/*****************************************************************************/


void mdu_register(mdu_ops_type* p_mdu_ops);
int  mdu_firmware_update(char* filename);


/*****************************************************************************/

#endif	/* __MDU_H */
