
/*
 * File:         variables
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Global variables
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include "variables.h"


/** V A R I A B L E S ********************************************************/
// Temporary data which doesn't need to be stored in flash memory
volatile volatileConfigStruct volatileConfig;

// userConfig contains the user settings which are stored in Flash.
// At startup these values are read from Flash and userConfig (Ram variable)
// is updated with the said settings.
// When going to standby the settings are read from userConfig and stored
// into the Flash memory.
// If the settings get corrupted, their values are restored from the code
// ('loadDefaultConfig' function).
volatile flashConfigStruct userConfig;


/** E N D   O F   F I L E ****************************************************/
