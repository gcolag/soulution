
/*
 * File:         pre_amp
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Volume control and input selection
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:
 *
 */

/** I N C L U D E S **********************************************************/


#include "pre_amp.h"


/** V A R I A B L E S ********************************************************/


/** F U N C T I O N S ********************************************************/
void initPreamp (void)
{
	if (userConfig.volumeMode != OFF_CTRL )
	{
      setSonic2VolumeReg(userConfig.volumeMode, 1);

		if (userConfig.volumeStart < userConfig.volumeMax)
			userConfig.volume = userConfig.volumeStart;
		else
			userConfig.volume = userConfig.volumeMax;

		setVolume(0);
	}
}


/*****************************************************************************
 * setVolume
 *
 * Overview			:	Change the volume value
 *
 * Params	value	:	-1: volume down
 *						 0: do not change volume
 *						+1: volume up
 *
 *****************************************************************************/
void setVolume (char value)
{
	if (value < 0)			// Volume down
	{
		if (userConfig.volume > 0)
			userConfig.volume--;
	}
	else if (value > 0)		// Volume up
	{
		if (userConfig.volume < userConfig.volumeMax)
			userConfig.volume++;
	}

	if (userConfig.volume == 0)
	{
		setSonic2LLevel(SONIC2_LEVEL_MUTE);
		delay_50us();
		setSonic2RLevel(SONIC2_LEVEL_MUTE);
	}
	else
	{
		if (userConfig.volumeMode == OFF_CTRL)
			userConfig.volume = userConfig.volumeMax;

		if (userConfig.balance < 10)
		{
			setSonic2LLevel(2*(MAX_SYS_VOLUME - userConfig.volume));
			delay_50us();
			setSonic2RLevel(2*(MAX_SYS_VOLUME - userConfig.volume + (2*(10 - userConfig.balance))));
		}
		else if (userConfig.balance > 10)
		{
			setSonic2LLevel(2*(MAX_SYS_VOLUME - userConfig.volume + (2*(userConfig.balance - 10))));
			delay_50us();
			setSonic2RLevel(2*(MAX_SYS_VOLUME - userConfig.volume));
		}
		else
		{
			setSonic2LLevel(2*(MAX_SYS_VOLUME - userConfig.volume));
			delay_50us();
			setSonic2RLevel(2*(MAX_SYS_VOLUME - userConfig.volume));
		}
	}
}


/*****************************************************************************
 * setMute
 *
 * Overview			:	Mute ON/OFF using a volume ramp on sonic module
 *
 * Params	value	:	1: mute is ON
 *                0: mute is OFF
 *
 *****************************************************************************/

/*
 * MUTE Operation info
 * The mute is made at two level :
 * - the S8
 * - the DACs
 * When switching between 22M/24M clocks, the DACs must be mutted
 * else they will do random plocs. The trouble is that the DACs
 * mute is made through I2C bus and is too slow.
 *
 * In the FPGA, when the MUTE signal comes, it is latched and will
 * keep the S8 muted until MUTE ACK signal issued by the PIC releases
 * it. This way, the PIC has the time to
 * - mute the DACs
 * - read  the requested 24M/24M clock setting
 * - write the new 24M/24M clock setting
 * - ack the MUTE
 * - unmute the DACs
 */

void dacs_mute(void)
{
  mutePCM1792(DAC_ID_LEFT);
  mutePCM1792(DAC_ID_RIGHT);
}


void dacs_unmute(void)
{
  dbio_assert_mute_ack();
  dbio_deassert_mute_ack();

  delay_850us();
  delay_850us();
  delay_850us();
  delay_850us();
  delay_850us();
  delay_850us();
  delay_850us();
  delay_850us();
  delay_850us();
  delay_850us();

  unmutePCM1792(DAC_ID_LEFT);
  unmutePCM1792(DAC_ID_RIGHT);
}


void setMute(T_ONorOFF state)
{
  int cur_level_left;
  int cur_level_right;
  int level_left;
  int level_right;
  int step = 4;
  int mute_level = 255;
  int volume;

  // Calculate cur_level_left/cur_level_right (= current module resigter values)
  // accorgind to userConfig settings
  //
  if (userConfig.volumeMode == OFF_CTRL)
    volume  = userConfig.volumeMax;
  else
    volume  = userConfig.volume;

  if (userConfig.balance < 10)
  {
    cur_level_left  = 2 * (MAX_SYS_VOLUME - volume);
    cur_level_right = 2 * (MAX_SYS_VOLUME - volume + (2*(10 - userConfig.balance)));
  }
  else if (userConfig.balance > 10)
  {
    cur_level_left  = 2 * (MAX_SYS_VOLUME - volume + (2*(userConfig.balance - 10)));;
    cur_level_right = 2 * (MAX_SYS_VOLUME - volume);
  }
  else
  {
    cur_level_left  = 2 * (MAX_SYS_VOLUME - volume);
    cur_level_right = 2 * (MAX_SYS_VOLUME - volume);
  }

  // Perform mute ON/OFF action
  //
  if (state == ON)
  {
    level_left  = cur_level_left;
    level_right = cur_level_right;
    while (1)
    {
      if (level_left  < mute_level) level_left  += step;
      if (level_right < mute_level) level_right += step;
      if (level_left  > mute_level) level_left   = mute_level;
      if (level_right > mute_level) level_right  = mute_level;

			setSonic2LLevel((unsigned char)level_left);
			setSonic2RLevel((unsigned char)level_right);

      if ((level_left == mute_level) && (level_right == mute_level)) break;
    }
    dacs_mute();
  }
  else
  {
    dacs_unmute();

    level_left  = mute_level;
    level_right = mute_level;
    while (1)
    {
      if (level_left  > cur_level_left)  level_left -= step;
      if (level_right > cur_level_right) level_right-= step;
      if (level_left  < cur_level_left)  level_left  = cur_level_left;
      if (level_right < cur_level_right) level_right = cur_level_right;

			setSonic2LLevel((unsigned char)level_left);
			setSonic2RLevel((unsigned char)level_right);

      if ( (level_left == cur_level_left) &&
           (level_right == cur_level_right) )
        break;
    }
  }
}


/*****************************************************************************
 * previewVolume
 *
 * Overview			:	Set the volume to a desired value
 *
 * Params	vol		:	10(mute)..69(-1dB)..70(0dB)
 *
 *****************************************************************************/
void previewVolume (unsigned char vol)
{
	if (userConfig.balance < 10)
	{
		setSonic2LLevel(2*(MAX_SYS_VOLUME - vol));
		setSonic2RLevel(2*(MAX_SYS_VOLUME - vol + (2*(10 - userConfig.balance))));
	}
	else if (userConfig.balance > 10)
	{
		setSonic2LLevel(2*(MAX_SYS_VOLUME - vol + (2*(userConfig.balance - 10))));
		setSonic2RLevel(2*(MAX_SYS_VOLUME - vol));
	}
	else
	{
		setSonic2LLevel(2*(MAX_SYS_VOLUME - vol));
		setSonic2RLevel(2*(MAX_SYS_VOLUME - vol));
	}
}


/*****************************************************************************
 * setBalance
 *
 * Overview			:	Change the balance value
 *
 * Params	value	:	-1: balance left
 *						 0: do not change balance
 *						+1: balance right
 *
 *****************************************************************************/
void setBalance (char value)
{
	if (value == -1)
	{
		if (userConfig.balance > 1)
			userConfig.balance--;
	}
	else
	{
		if (userConfig.balance < 19)
			userConfig.balance++;
	}
	setVolume(0);
}


/*****************************************************************************
 * selectInput
 *
 * Overview			:	Select a digital input
 *
 * Params	value	:	-1: select previous input
 *						 0: do not change input
 *						+1: select next input
 *
 *****************************************************************************/
void selectInput (char value)
{
	if (value > 0)
	{
		if (userConfig.DigitalInput < DIGITAL_INPUT_MEZZANINE_NET)
		{
			userConfig.DigitalInput++;
      if (userConfig.DigitalInput == DIGITAL_INPUT_MEZZANINE_USB)
        DataValidSonic2();
			configListen();
			configRecord();
		}
	}
	else if (value < 0)
	{
		if (userConfig.DigitalInput > DIGITAL_INPUT_SPDIF)
		{
			userConfig.DigitalInput--;
			configListen();
			configRecord();
		}
	}
}


/*****************************************************************************
 * selectNetworkInput
 *
 * Overview			:	Force Network input
 *                Used during DSP module update
 *
 *****************************************************************************/
static int userSelectedInput;

void selectNetworkInput(void)
{
  userSelectedInput = userConfig.DigitalInput;

  userConfig.DigitalInput = DIGITAL_INPUT_MEZZANINE_NET;
  configListen();
  configRecord();
}


/*****************************************************************************
 * selectUserSelectedInput
 *
 * Overview			:	Switch to input selected by user.
 *                This function is meant to restore selection after
 *                selectNetworkInput() call.
 *
 *****************************************************************************/
void selectUserSelectedInput(void)
{
  userConfig.DigitalInput =   userSelectedInput;
  configListen();
  configRecord();
}


/** E N D   O F   F I L E ****************************************************/
