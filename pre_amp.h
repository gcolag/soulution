
/*
 * File:         pre_amp
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Volume control and input selection
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#ifndef	__PRE_AMP
#define	__PRE_AMP


/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>

#include "sonic2.h" 
#include "variables.h"


/** D E F I N E S ************************************************************/
#define MAX_SYS_VOLUME		80


/** D E C L A R A T I O N S **************************************************/
void initPreamp (void);
void setVolume (char value);
void setMute (T_ONorOFF state);
void previewVolume (unsigned char vol);
void setBalance (char value);
void selectInput (char value);
void selectNetworkInput(void);
void selectUserSelectedInput(void);


/** E N D   O F   F I L E ****************************************************/
#endif
