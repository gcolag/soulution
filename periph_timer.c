
/*
 * File:         periph_timer
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Timers management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include "periph_timer.h"


/** V A R I A B L E S ********************************************************/
volatile unsigned int timerCounter;


/** I N T E R R U P T S ******************************************************/

// TIMER 1 ISR
void __attribute__((interrupt, no_auto_psv)) _T1Interrupt()
{
	timerCounter++;
	IFS0bits.T1IF = 0;
}


/** F U N C T I O N S ********************************************************/
/*************************************************************************************
 * initTimer
 *
 * Description	: This function initialiazes the Timer Peripherals
 *
 *************************************************************************************/
void initTimer(void)
{
	// ---------------------------------------------------------------------
	// Configuration of timer 1.
	// External source, EC oscillator 12MHz, PLL enabled.
	// Fosc = 12MHz-> Fcy = Fosc/2 = 6MHz.
	// Ftimer1 = Fcy / 8 (pre-scaler) = 750kHz.
	// period = 1ms * Ftimer1 = 750.
	// ---------------------------------------------------------------------
	OpenTimer1( T1_ON | T1_IDLE_CON | T1_GATE_OFF | T1_PS_1_8 |
//				T1_SYNC_EXT_OFF | T1_SOURCE_INT, 750);
				T1_SYNC_EXT_OFF | T1_SOURCE_INT, 1900);
				
	ConfigIntTimer1(T1_INT_ON | T1_INT_PRIOR_1);
	
	timerCounter = 0;
}

/*****************************************************************************
 * Function		:	resetTimerCounter
 *
 * Description	:	Reset 'timerCounter' value
 *
 *****************************************************************************/
void resetTimerCounter (void)
{
	timerCounter = 0;
}

/*************************************************************************************
 * readTimerCounter
 *
 * Description	:	This function returns the value of 'timerCounter'
 *
 *************************************************************************************/
unsigned int readTimerCounter (void)
{
	return timerCounter;
}

/*************************************************************************************
 * readTimerDiff
 *
 * Description	:	This function returns the time (in ms) elapsed between a given
 *					value and the timeCounter value at the moment
 *
 * Value		:	Reference of time (0..65535)
 *
 *************************************************************************************/
unsigned int readTimerDiff (unsigned int value)
{
	unsigned int diff;
	
	if (timerCounter >= value)
		diff = timerCounter - value;
	else
		diff = 65535 - (value - timerCounter);
	
	return diff;
}

/*************************************************************************************
 * delay_ms
 *
 * Description	:	This function waits for 'duration' milliseconds
 *
 * Duration		:	Wait length in ms (0..65535)
 *
 *************************************************************************************/
void delay_ms (unsigned int duration) 
{
	unsigned int goal = timerCounter;
	while ((timerCounter - goal) < duration);
}

void delay_50us (void)
{
	int i;
	
	for (i=0;i<41;i++)
	{
		Nop();
		Nop();
	}
}

void delay_850us (void)
{
	int i;
	
	for (i=0;i<339;i++)
	{
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
	}
}


/** E N D   O F   F I L E ****************************************************/
