
/*
 * File:         error
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Error notification
 *
 *  Display error code on status led and halt
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include "periph_timer.h"
#include "io.h"


/*****************************************************************************/


#define ERR_MASK 0x1F


/*****************************************************************************/


static void display_error(int code)
{
  int i;

  code = code & ERR_MASK;

  for (i=4 ; i>=0 ; i--)
  {
    led_cmd_on();

    if (code & (1<<i))
      delay_ms(250);
    else
      delay_ms(25);

    led_cmd_off();

    delay_ms(150);
  }
}


/*****************************************************************************/


void error(int code)
{
  while (1)
  {
    display_error(code);
    delay_ms(1000);
  }

}


/*****************************************************************************/


