
/*
 * File:         rc5
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Remote control management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef	__RC
#define	__RC

/** I M P O R T A N T ********************************************************/
// You must add the "USE_AND_OR" macro definition to your project build
// options under MPLAB C30 tab.
// Alternatively you can uncomment the line below.
/*****************************************************************************/
//#define USE_AND_OR


/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <inCap.h>

#include "variables.h"
#include "periph_timer.h"


/** I N C L U D E S **********************************************************/
#include "keys.h"			// For keys definition


#define IR_PSU		LATDbits.LATD11
//#define IR_PIN		PORTDbits.RD12

/** D E C L A R A T I O N S **************************************************/
void initRc(void);
void readRCKey(volatile rcKeyStruct * rc);
int  rc5_get_error(void);
int rc5_get_lastid(void);


/** E N D   O F   F I L E ****************************************************/
#endif
