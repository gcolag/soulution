
/*
 * File:         error
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Error notification
 *
 *  Display error code on status led and halt
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __ERROR_H
#define	__ERROR_H

/*****************************************************************************/


#define ERR__END_OF_MAIN_REACHED           1
#define ERR__DISPLAY_ILLEGAL_SOURCE_INDEX  2
#define ERR__CS8416_INIT                   3


/*****************************************************************************/


void error(int code);


/*****************************************************************************/

#endif	/* __ERROR_H */


