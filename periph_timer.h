
/*
 * File:         periph_timer
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Timers management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef	__PERIPH_TIMER
#define	__PERIPH_TIMER

/** I M P O R T A N T ********************************************************/
// You must add the "USE_AND_OR" macro definition to your project build
// options under MPLAB C30 tab.
// Alternatively you can uncomment the line below.
/*****************************************************************************/
//#define USE_AND_OR


/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <timer.h>


/** D E F I N E S ************************************************************/
#define delay_1us()		Nop();Nop();Nop();Nop();Nop();Nop()


/** D E C L A R A T I O N S **************************************************/
void initTimer (void);
void resetTimerCounter (void);
unsigned int readTimerCounter(void);
unsigned int readTimerDiff (unsigned int);

void delay_ms(unsigned int);
void delay_50us (void);
void delay_850us (void);


/** E N D   O F   F I L E ****************************************************/
#endif
