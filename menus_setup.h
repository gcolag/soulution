
/*
 * File:         menus_setup
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Menus management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#ifndef	__MENU_SETUP
#define	__MENU_SETUP


/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>

#include "variables.h"
#include "periph_timer.h"
#include "config_mgr.h"
#include "pre_amp.h"
#include "io.h"
#include "dbio.h"
#include "cmd_mgr.h"
#include "SCF5742.h"
#include "eeprom.h"
#include "digital_in.h"
#include "config_boards.h"


/** D E F I N I T I O N S ****************************************************/
#define TIME_OUT_MENU			10000

// Navigation in the menu
// volatileConfig.nextMenu
#define EXIT_MENU                  (unsigned char)0x00
#define MENU_POLARITY              (unsigned char)0x04
#define MENU_VOLUME_MODE           (unsigned char)0x05
#define MENU_START_VOLUME          (unsigned char)0x06
#define MENU_MAX_VOLUME            (unsigned char)0x07
#define MENU_BALANCE               (unsigned char)0x08
#define MENU_DIGITAL_OUT           (unsigned char)0x09
#define MENU_START_MODE            (unsigned char)0x0A
#define MENU_CLOCK_OUTPUT          (unsigned char)0x0B
#define MENU_BRIGHTNESS            (unsigned char)0x0C
#define MENU_UP_SAMPLING_FILTERS   (unsigned char)0x0D
#define MENU_DC_OFFSET_LEFT        (unsigned char)0x0E
#define MENU_DC_OFFSET_RIGHT       (unsigned char)0x0F
#define MENU_PHASE_EQ              (unsigned char)0x10
#define MENU_LOAD_DEFAULT          (unsigned char)0x11
#define MENU_VERSION               (unsigned char)0x12
#define MENU_MODULE_VERSION        (unsigned char)0x13
#define MENU_MODULE_UPDATE         (unsigned char)0x14
#define MENU_DEBUG                 (unsigned char)0x15

#define CURRENT_MENU               (unsigned char)0xFF


/** P U B L I C  P R O T O T Y P E S *****************************************/
void menuSetup(void);


/** E N D   O F   F I L E ****************************************************/
#endif
