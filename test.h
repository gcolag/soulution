
/*
 * File:         test
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Test module
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __TEST_H
#define	__TEST_H

/*****************************************************************************/


void test_1(void);
void test_2(void);
void test_3(void);
void test_4(void);
void test_5(void);
void test_6(void);
void test_7(void);
void test_8(void);
void test_9(void);
void test_10(void);
void test_11(void);
void test_12(void);
void test_13(void);
void test_14(void);
void test_15(void);
void test_16(void);
void test_17(void);
void test_18(void);
void test_19(void);
void test_20(void);


/*****************************************************************************/

#endif	/* __TEST_H */

