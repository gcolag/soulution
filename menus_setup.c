
/*
 * File:         menus_setup
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB S�rl
 *               Copyright ABC PCB S�rl 2015
 * Created:      2015/04/10
 * Description:  Menus management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:
 *
 */

/** I N C L U D E S **********************************************************/
#include "menus_setup.h"

#include <stdio.h>
#include "usb_fs.h"
#include "mdu.h"


/** V A R I A B L E S ********************************************************/
static unsigned int timeout_menu; 		// Auto-exit menu after a period of time


/** P R I V A T E  P R O T O T Y P E S ***************************************/
void menuPhasePolarity(void);
void menuVolumeMode(void);
void menuStartVolume(void);
void menuMaxVolume(void);
void menuBalance(void);
void menuDigitalOut(void);
void menuStartMode(void);
void menuClockOutput(void);
void menuBrightness(void);
void menuUpSamplingFilters(void);
void menuDcOffsetLeft(void);
void menuDcOffsetRight(void);
void menuPhaseEq(void);
void menuLoadDefault(void);
void applyDefaultConfig (void);
void menuVersion(void);
void menuModuleVersion(void);
void menuModuleUpdate(void);

//#define USE_DEBUG_MENU_ENTRY 1
#if defined(USE_DEBUG_MENU_ENTRY)
void menuDebug(void);
#endif

/** F U N C T I O N S ********************************************************/

void int2str(unsigned char value, char*msg, int pos)
{
  msg[pos+0] = (value/100) + '0';
  msg[pos+1] = ((value/10) % 10) + '0';
  msg[pos+2] = (value%10) + '0';
}

// *****************************************************************************
//
// Menu SETUP
//
// *****************************************************************************
void menuSetup(void)
{
	volatileConfig.prevState = volatileConfig.state;
	volatileConfig.state = STATE_SETUP;
#if defined(USE_DEBUG_MENU_ENTRY)
	volatileConfig.nextMenu = MENU_DEBUG;
#else
	volatileConfig.nextMenu = MENU_POLARITY;
#endif
	timeout_menu = readTimerCounter();

	// Initialize RC repeat delay
	rcRepeatDelay = RC_REPEAT_DELAY;

	dispClearLine3();

	while (volatileConfig.nextMenu != EXIT_MENU)
	{
		switch (volatileConfig.nextMenu)
		{
			case MENU_POLARITY:
				volatileConfig.displayRefresh = REFRESH;
				menuPhasePolarity();
				break;

			case MENU_VOLUME_MODE:
				volatileConfig.displayRefresh = REFRESH;
				menuVolumeMode();
				break;

			case MENU_START_VOLUME:
				volatileConfig.displayRefresh = REFRESH;
				menuStartVolume();
				break;

			case MENU_MAX_VOLUME:
				volatileConfig.displayRefresh = REFRESH;
				menuMaxVolume();
				break;

			case MENU_BALANCE:
				volatileConfig.displayRefresh = REFRESH;
				menuBalance();
				break;

			case MENU_DIGITAL_OUT:
				volatileConfig.displayRefresh = REFRESH;
				menuDigitalOut();
				break;

			case MENU_START_MODE:
				volatileConfig.displayRefresh = REFRESH;
				menuStartMode();
				break;

			case MENU_CLOCK_OUTPUT:
				volatileConfig.displayRefresh = REFRESH;
				menuClockOutput();
				break;

			case MENU_BRIGHTNESS:
				volatileConfig.displayRefresh = REFRESH;
				menuBrightness();
				break;

      case MENU_UP_SAMPLING_FILTERS:
				volatileConfig.displayRefresh = REFRESH;
				menuUpSamplingFilters();
				break;

      case MENU_DC_OFFSET_LEFT:
				volatileConfig.displayRefresh = REFRESH;
				menuDcOffsetLeft();
				break;

      case MENU_DC_OFFSET_RIGHT:
				volatileConfig.displayRefresh = REFRESH;
				menuDcOffsetRight();
				break;

      case MENU_PHASE_EQ:
				volatileConfig.displayRefresh = REFRESH;
				menuPhaseEq();
				break;

			case MENU_LOAD_DEFAULT:
				volatileConfig.displayRefresh = REFRESH;
				menuLoadDefault();
				break;

			case MENU_VERSION:
				volatileConfig.displayRefresh = REFRESH;
				menuVersion();
				break;

			case MENU_MODULE_VERSION:
				volatileConfig.displayRefresh = REFRESH;
				menuModuleVersion();
				break;

			case MENU_MODULE_UPDATE:
				volatileConfig.displayRefresh = REFRESH;
				menuModuleUpdate();
				break;

#if defined(USE_DEBUG_MENU_ENTRY)
			case MENU_DEBUG:
				volatileConfig.displayRefresh = REFRESH;
				menuDebug();
				break;
#endif

			default:
				volatileConfig.nextMenu = MENU_START_MODE;
				break;
		}
	}

	// Exit from menu
	volatileConfig.state = userConfig.SourceMode;
	writeConfigToFlash();
}


// *****************************************************************************
//
// Menu PHASE POLARITY
//
// *****************************************************************************
void menuPhasePolarity(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	unsigned char refreshPhase = NO_REFRESH;
	char subMenu = 0;
	char value = 0;
	unsigned char phasePolarity;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Save current settings
	phasePolarity = userConfig.phasePolarity;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("    POLARITY");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (phasePolarity == PHASEPOLARITY_0)
				{
					phasePolarity = PHASEPOLARITY_180;
					refreshPhase = REFRESH;
				}
			}
			if (value < 0)
			{
				if (phasePolarity == PHASEPOLARITY_180)
				{
					phasePolarity = PHASEPOLARITY_0;
					refreshPhase = REFRESH;
				}
			}

			switch (phasePolarity)
			{
				case PHASEPOLARITY_0:
					dispPutLine2("      0^");	// '^' displays '�'
//					dispPutLine2("       0");	// '^' displays '�'
					break;
				case PHASEPOLARITY_180:
					dispPutLine2("    180^");	// '^' displays '�'
//					dispPutLine2("     180");	// '^' displays '�'

				default:
					break;
			}

			if (refreshPhase)
			{
				// Apply new value
				configPhasePolarity(phasePolarity);
				refreshPhase = NO_REFRESH;
			}

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
        {
          // This is first menu ; do nothing
#if defined(USE_DEBUG_MENU_ENTRY)
          volatileConfig.nextMenu = MENU_DEBUG;
#endif
        }
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_VOLUME_MODE;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();
				}
				else
				{
					// Save setting
          userConfig.phasePolarity = phasePolarity;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
          configPhasePolarity(userConfig.phasePolarity);
					refreshValue = REFRESH;
					refreshPhase = REFRESH;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}

// *****************************************************************************
//
// Menu VOLUME MODE
//
// ****************************************************************************
static te_volume_ctrl_t prevVolMode = OFF_CTRL;

void menuVolumeMode(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	te_volume_ctrl_t volMode;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Load current settings
	volMode = userConfig.volumeMode;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1(" VOLUME-MODE");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (volMode == OFF_CTRL) {

					volMode = STD_CTRL;
            }
            else if (volMode == STD_CTRL) {
               volMode = LEEDH_CTRL;
            }
			}
			if (value < 0)
			{
				if (volMode == STD_CTRL) {

					volMode = OFF_CTRL;
            }
            else if (volMode == LEEDH_CTRL) {
               volMode = STD_CTRL;
            }
			}

			if (volMode == OFF_CTRL) {

				dispPutLine2("     OFF");
         }
			else if (volMode == STD_CTRL) {
				dispPutLine2("     STD");

         }
         else {
				dispPutLine2("   LEEDH");

         }

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_POLARITY;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_START_VOLUME;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
            if (subMenu == 0)
            {
               // Enter current setting
               subMenu = 1;
               led_cmd_on();
            }
            else
            {
               // Save setting
               userConfig.volumeMode = volMode;

               // Apply new setting
               if ((volMode == STD_CTRL) || (volMode == LEEDH_CTRL)) {
                  if ( prevVolMode == OFF_CTRL) {
                     userConfig.volume = userConfig.volumeStart;
                  }
                  setSonic2VolumeReg(volMode, 0);

               }
               //userConfig.volume = userConfig.volumeStart;
               prevVolMode = volMode;
               setVolume(0);

               // Return to menu
               subMenu = 0;
               led_cmd_off();
            }
            break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					volMode = userConfig.volumeMode;
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}

// *****************************************************************************
//
// Menu START VOLUME
//
// *****************************************************************************
void menuStartVolume(void)
{
	char line[] = "        ";
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	unsigned char refreshVolume = NO_REFRESH;
	char subMenu = 0;
	char value = 0;
	unsigned char volStart;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Load current settings
	volStart = userConfig.volumeStart;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("START-VOLUME");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (userConfig.volumeStart < 60)
				{
					userConfig.volumeStart++;
					refreshVolume = REFRESH;
				}
			}
			if (value < 0)
			{
				if (userConfig.volumeStart > 10)
				{
					userConfig.volumeStart--;
					refreshVolume = REFRESH;
				}
			}

			line[6] = (userConfig.volumeStart/10) + '0';
			line[7] = (userConfig.volumeStart%10) + '0';
			dispPutLine2(line);

			if (refreshVolume)
			{
				// Apply new value
				previewVolume(userConfig.volumeStart);
				refreshVolume = NO_REFRESH;
			}

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_VOLUME_MODE;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_MAX_VOLUME;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;

					// Change RC repeat delay
					rcRepeatDelay = RC_REPEAT_DELAY_FAST;

          led_cmd_on();
					previewVolume(userConfig.volumeStart);
				}
				else
				{
					// Return to current volume
					setVolume(0);

					// Return to menu
					subMenu = 0;

					// Change RC repeat delay
					rcRepeatDelay = RC_REPEAT_DELAY;

          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					userConfig.volumeStart = volStart;
					refreshValue = REFRESH;

					// Return to current volume
					setVolume(0);

					// Return to menu
					subMenu = 0;

					// Change RC repeat delay
					rcRepeatDelay = RC_REPEAT_DELAY;

          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}


// *****************************************************************************
//
// Menu MAX VOLUME
//
// *****************************************************************************
void menuMaxVolume(void)
{
	char line[] = "        ";
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	unsigned char volMax;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Backup current settings
	volMax = userConfig.volumeMax;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("  MAX-VOLUME");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (userConfig.volumeMax < MAX_SYS_VOLUME)
					userConfig.volumeMax++;
			}
			if (value < 0)
			{
				if (userConfig.volumeMax > 20)
					userConfig.volumeMax--;
			}

			line[6] = (userConfig.volumeMax/10) + '0';
			line[7] = (userConfig.volumeMax%10) + '0';
			dispPutLine2(line);

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_START_VOLUME;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_BALANCE;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;

					// Change RC repeat delay
					rcRepeatDelay = RC_REPEAT_DELAY_FAST;

          led_cmd_on();
				}
				else
				{
					// Check if current volume is over new max value
					if (userConfig.volume > userConfig.volumeMax)
					{
						userConfig.volume = userConfig.volumeMax;
						setVolume(0);
					}

					// Return to current volume or set new max volume
					if (userConfig.volumeMode == OFF_CTRL)
					{
						userConfig.volume = userConfig.volumeMax;
						setVolume(0);
					}

					// Return to menu
					subMenu = 0;

					// Change RC repeat delay
					rcRepeatDelay = RC_REPEAT_DELAY;

          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					userConfig.volumeMax = volMax;
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;

					// Change RC repeat delay
					rcRepeatDelay = RC_REPEAT_DELAY;

          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}

// *****************************************************************************
//
// Menu BALANCE
//
// *****************************************************************************
void menuBalance(void)
{
	char line[] = "        ";
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	unsigned char refreshVolume = NO_REFRESH;
	char subMenu = 0;
	char value = 0;
	unsigned char bal;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Backup current settings
	bal = userConfig.balance;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("     BALANCE");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (userConfig.balance < 19)
				{
					userConfig.balance++;
					refreshVolume = REFRESH;
				}
			}
			if (value < 0)
			{
				if (userConfig.balance > 1)
				{
					userConfig.balance--;
					refreshVolume = REFRESH;
				}
			}

			if (userConfig.balance < 10)
			{
				line[4] = '<';
				line[5] = '-';
				line[6] = ' ';
				line[7] = (10 - userConfig.balance) + '0';
			}
			else if (userConfig.balance > 10)
			{
				line[4] = '-';
				line[5] = '>';
				line[6] = ' ';
				line[7] = (userConfig.balance - 10) + '0';
			}
			else	// userConfig.balance = 10
			{
				line[4] = ' ';
				line[5] = ' ';
				line[6] = ' ';
				line[7] = '0';
			}
			dispPutLine2(line);

			if (refreshVolume)
			{
				// Apply new value
				setVolume(0);
				refreshVolume = NO_REFRESH;
			}

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_MAX_VOLUME;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_DIGITAL_OUT;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;

					// Change RC repeat delay
					rcRepeatDelay = RC_REPEAT_DELAY_FAST;

          led_cmd_on();
				}
				else
				{
					// Return to menu
					subMenu = 0;

					// Change RC repeat delay
					rcRepeatDelay = RC_REPEAT_DELAY;

          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					userConfig.balance = bal;
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;

					// Change RC repeat delay
					rcRepeatDelay = RC_REPEAT_DELAY;

          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}

// *****************************************************************************
//
// Menu DIGITAL OUT
//
// *****************************************************************************
void menuDigitalOut(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	T_ONorOFF digOut;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Load current settings
	digOut = userConfig.DigitalOutput;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1(" DIGITAL-OUT");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (digOut == OFF)
					digOut = ON;
			}
			if (value < 0)
			{
				if (digOut == ON)
					digOut = OFF;
			}

			if (digOut == ON)
				dispPutLine2("      ON");
			else
				dispPutLine2("     OFF");

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_BALANCE;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_START_MODE;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();
				}
				else
				{
					// Save setting
					userConfig.DigitalOutput = digOut;

					// Apply new setting
					configRecord();

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					digOut = userConfig.DigitalOutput;
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}

// *****************************************************************************
//
// Menu START MODE
//
// *****************************************************************************
void menuStartMode(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	unsigned char startMode;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Load current settings
	startMode = userConfig.StartMode;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("  START-MODE");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (startMode != STARTMODE_AUTO)
					startMode++;
			}
			if (value < 0)
			{
				if (startMode != STARTMODE_NORM)
					startMode--;
			}

			switch (startMode)
			{
				case STARTMODE_NORM:
					dispPutLine2("    NORM");
					break;

				case STARTMODE_LINK:
					dispPutLine2("    LINK");
					break;

				case STARTMODE_AUTO:
					dispPutLine2("    AUTO");
					break;

				default:
					break;
			}

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
				{
					volatileConfig.nextMenu = MENU_DIGITAL_OUT;
				}
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
//					volatileConfig.nextMenu = MENU_BRIGHTNESS;
					volatileConfig.nextMenu = MENU_CLOCK_OUTPUT;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();
				}
				else
				{
					// Save setting
					userConfig.StartMode = startMode;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					startMode = userConfig.StartMode;
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}

// *****************************************************************************
//
// Menu CLOCK OUTPUT
//
// *****************************************************************************
void menuClockOutput(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	unsigned char outputState;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Load current settings
	outputState = userConfig.ClockOutput;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("CLOCK-OUTPUT");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (outputState != CLOCK_OUTPUT_48K)
					outputState++;
			}
			if (value < 0)
			{
				if (outputState != CLOCK_OUTPUT_44K)
					outputState--;
			}

			switch (outputState)
			{
				case CLOCK_OUTPUT_44K:
					dispPutLine2(" 44.1KHz");
					break;

				case CLOCK_OUTPUT_48K:
					dispPutLine2("   48KHz");
					break;

				default:
					break;
			}

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_START_MODE;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_BRIGHTNESS;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();

				}
				else
				{
					// Save setting
					userConfig.ClockOutput = outputState;

					// Apply new setting
					configListen();

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					outputState = userConfig.ClockOutput;
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}

// *****************************************************************************
//
// Menu BRIGHTNESS
//
// *****************************************************************************
void menuBrightness(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	unsigned char brightness;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Load current settings
	brightness = userConfig.Brightness;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("  BRIGHTNESS");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (brightness != BRIGHTNESS_HIGH)
				{
					brightness++;
					dispBrightness(brightness);
				}
			}
			if (value < 0)
			{
				if (brightness != BRIGHTNESS_LOW)
				{
					brightness--;
					dispBrightness(brightness);
				}
			}

			switch (brightness)
			{
				case BRIGHTNESS_LOW:
					dispPutLine2("     LOW");
					break;

				case BRIGHTNESS_MEDIUM:
					dispPutLine2("  MEDIUM");
					break;

				case BRIGHTNESS_HIGH:
					dispPutLine2("    HIGH");
					break;

				default:
					break;
			}

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
//					volatileConfig.nextMenu = MENU_START_MODE;
					volatileConfig.nextMenu = MENU_CLOCK_OUTPUT;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
          if (volatileConfig.factory_mode)
            volatileConfig.nextMenu = MENU_UP_SAMPLING_FILTERS;
          else
            volatileConfig.nextMenu = MENU_LOAD_DEFAULT;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();
				}
				else
				{
					// Save setting
					userConfig.Brightness = brightness;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					brightness = userConfig.Brightness;
					dispBrightness(brightness);
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}


// *****************************************************************************
//
// Menu UP SAMPLING FILTERS
//
// *****************************************************************************
void menuUpSamplingFilters(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	unsigned char upSamplingFilters;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Load current settings
	upSamplingFilters = userConfig.UpSamplingFilters;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("  FILTERS   ");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (upSamplingFilters != SONIC2_UP_SAMPLING_FILTERS_MAX)
				{
					upSamplingFilters++;
          setUpsamplingFilters(upSamplingFilters);
				}
			}
			if (value < 0)
			{
				if (upSamplingFilters != SONIC2_UP_SAMPLING_FILTERS_MIN)
				{
					upSamplingFilters--;
          setUpsamplingFilters(upSamplingFilters);
				}
			}

			switch (upSamplingFilters)
			{
        case 0:
//					dispPutLine2("       0");
          // Lin phase 160 taps
					dispPutLine2("LIN  160");
					break;

				case 1:
//					dispPutLine2("       1");
          // Min phase 160 taps
					dispPutLine2("MIN  160");
					break;

				case 2:
//					dispPutLine2("       2");
          // Lin phase apodyzing 160 taps
					dispPutLine2("LINA 160");
					break;

				case 3:
//					dispPutLine2("       3");
          // Min phase apodyzing 160 taps
					dispPutLine2("MINA 160");
					break;

				case 4:
//					dispPutLine2("       4");
          // Lin phase 84 taps
					dispPutLine2("LIN  84 ");
					break;

				case 5:
//					dispPutLine2("       5");
          // Min phase 84 taps
					dispPutLine2("MIN  84 ");
					break;

				case 6:
//					dispPutLine2("       6");
          // Lin phase apodyzing 84 taps
					dispPutLine2("LINA 84 ");
					break;

				case 7:
//					dispPutLine2("       7");
          // Min phase apodyzing 84 taps
					dispPutLine2("MINA 84 ");
					break;

				default:
					break;
			}

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_BRIGHTNESS;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_DC_OFFSET_LEFT;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();
				}
				else
				{
					// Save setting
					userConfig.UpSamplingFilters = upSamplingFilters;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					upSamplingFilters = userConfig.UpSamplingFilters;
          setUpsamplingFilters(upSamplingFilters);
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}


// *****************************************************************************
//
// Menu DC OFFSET
//
// *****************************************************************************
void menuDcOffsetLeft(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	char DcOffset;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Load current settings
	DcOffset = userConfig.DcOffsetLeft;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("  DC OFFS L ");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (DcOffset != SONIC2_DC_OFFSET_MAX)
				{
					DcOffset++;
          setDcOffsetLeft(DcOffset);
				}
			}
			if (value < 0)
			{
				if (DcOffset != SONIC2_DC_OFFSET_MIN)
				{
					DcOffset--;
          setDcOffsetLeft(DcOffset);
				}
			}

      {
        char saveDcOffset;
        static char dcOffsetStr[9]		= "        ";
        int visible = 0;
        int signpos;
        char sign;
        int DcOffsetInt;
        int DcOffsetFract;

        saveDcOffset = DcOffset;

        dcOffsetStr[0] = ' ';
        dcOffsetStr[1] = ' ';
        dcOffsetStr[2] = ' ';

        if (DcOffset < 0)
        {
          DcOffset = - DcOffset;
          sign = '-';
        }
        else
        {
          sign = ' ';
        }

        DcOffsetInt = DcOffset >> 1;
        DcOffsetFract = DcOffset % 2;

        if (DcOffsetInt < 10)
        {
          signpos = 2;
        }
        else if (DcOffsetInt < 100)
        {
          signpos = 1;
        }
        else
        {
          signpos = 0;
        }
        dcOffsetStr[signpos] = sign;

        if (DcOffsetInt/100)
        {
          dcOffsetStr[1] = (DcOffsetInt/100) + '0';
          visible = 1;
        }
        if (visible || (DcOffsetInt/10))
        {
          dcOffsetStr[2] = (DcOffsetInt/10) + '0';
        }
        dcOffsetStr[3] = (DcOffsetInt%10) + '0';
        dcOffsetStr[4] = '.';
        if (DcOffsetFract)
          dcOffsetStr[5] = '5';
        else
          dcOffsetStr[5] = '0';
        dcOffsetStr[6] = 'm';
        dcOffsetStr[7] = 'V';
        dcOffsetStr[8] = 0;

        dispPutLine2(dcOffsetStr);

        DcOffset = saveDcOffset;
      }

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
                  volatileConfig.nextMenu = MENU_UP_SAMPLING_FILTERS;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_DC_OFFSET_RIGHT;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();
				}
				else
				{
					// Save setting
					userConfig.DcOffsetLeft = DcOffset;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					DcOffset = userConfig.DcOffsetLeft;
          setDcOffsetLeft(DcOffset);
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}


void menuDcOffsetRight(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	char DcOffset;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Load current settings
	DcOffset = userConfig.DcOffsetRight;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("  DC OFFS R ");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (DcOffset != SONIC2_DC_OFFSET_MAX)
				{
					DcOffset++;
         setDcOffsetRight(DcOffset);
				}
			}
			if (value < 0)
			{
				if (DcOffset != SONIC2_DC_OFFSET_MIN)
				{
					DcOffset--;
          setDcOffsetRight(DcOffset);
				}
			}

      {
        char saveDcOffset;
        static char dcOffsetStr[9]		= "        ";
        int visible = 0;
        int signpos;
        char sign;
        int DcOffsetInt;
        int DcOffsetFract;

        saveDcOffset = DcOffset;

        dcOffsetStr[0] = ' ';
        dcOffsetStr[1] = ' ';
        dcOffsetStr[2] = ' ';

        if (DcOffset < 0)
        {
          DcOffset = - DcOffset;
          sign = '-';
        }
        else
        {
          sign = ' ';
        }

        DcOffsetInt = DcOffset >> 1;
        DcOffsetFract = DcOffset % 2;

        if (DcOffsetInt < 10)
        {
          signpos = 2;
        }
        else if (DcOffsetInt < 100)
        {
          signpos = 1;
        }
        else
        {
          signpos = 0;
        }
        dcOffsetStr[signpos] = sign;

        if (DcOffsetInt/100)
        {
          dcOffsetStr[1] = (DcOffsetInt/100) + '0';
          visible = 1;
        }
        if (visible || (DcOffsetInt/10))
        {
          dcOffsetStr[2] = (DcOffsetInt/10) + '0';
        }
        dcOffsetStr[3] = (DcOffsetInt%10) + '0';
        dcOffsetStr[4] = '.';
        if (DcOffsetFract)
          dcOffsetStr[5] = '5';
        else
          dcOffsetStr[5] = '0';
        dcOffsetStr[6] = 'm';
        dcOffsetStr[7] = 'V';
        dcOffsetStr[8] = 0;

        dispPutLine2(dcOffsetStr);

        DcOffset = saveDcOffset;
      }

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
          volatileConfig.nextMenu = MENU_DC_OFFSET_LEFT;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_PHASE_EQ;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();
				}
				else
				{
					// Save setting
					userConfig.DcOffsetRight = DcOffset;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
					DcOffset = userConfig.DcOffsetRight;
          setDcOffsetRight(DcOffset);
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}


void menuPhaseEq(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	char PhaseEq;

	volatileConfig.nextMenu = CURRENT_MENU;

	// Load current settings
	PhaseEq = userConfig.PhaseEq;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("  PHASE EQ  ");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if ((value > 0) || (value < 0))
      {
        PhaseEq = PhaseEq ? 0 : 1;
        setPhaseEq(PhaseEq);
      }

			if (PhaseEq == 1)
				dispPutLine2("      ON");
			else
				dispPutLine2("     OFF");

			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
          volatileConfig.nextMenu = MENU_DC_OFFSET_RIGHT;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_LOAD_DEFAULT;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();
				}
				else
				{
					// Save setting
          userConfig.PhaseEq = PhaseEq;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Reload the previous setting
          PhaseEq = userConfig.PhaseEq;
          setPhaseEq(PhaseEq);
					refreshValue = REFRESH;

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}


// *****************************************************************************
//
// Menu LOAD DEFAULT
//
// *****************************************************************************
void menuLoadDefault(void)
{
	unsigned char cmd = NO_CMD;
	unsigned char refreshValue = REFRESH;
	char subMenu = 0;
	char value = 0;
	unsigned char reset = 0;

	volatileConfig.nextMenu = CURRENT_MENU;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("LOAD-DEFAULT");
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		if (refreshValue == REFRESH)
		{
			if (value > 0)
			{
				if (reset == 0x00)
					reset = 0x01;
			}
			if (value < 0)
			{
				if (reset == 0x01)
					reset = 0x00;
			}

			switch (reset)
			{
				case 0x00:
					dispPutLine2("      NO");
					break;

				case 0x01:
					dispPutLine2("     YES");
					break;

				default:
					break;
			}

			value = 0;
			refreshValue = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				if (subMenu == 0)
          if (volatileConfig.factory_mode)
            volatileConfig.nextMenu = MENU_PHASE_EQ;
          else
            volatileConfig.nextMenu = MENU_BRIGHTNESS;
				else
				{
					value = -1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_RIGHT:
				if (subMenu == 0)
					volatileConfig.nextMenu = MENU_VERSION;
				else
				{
					value = 1;
					refreshValue = REFRESH;
				}
				break;

			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();
				}
				else
				{
					if (reset == 0x01)
					{
						// Load default configuration
						loadDefaultConfig();			// Reset userConfig variables

						// Save config in EEPROM
						writeConfigToFlash();

						// Apply new settings
						applyDefaultConfig();
					}

					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			case CMD_MENU:
				if (subMenu == 0)
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
				else
				{
					// Return to menu
					subMenu = 0;
          led_cmd_off();
				}
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (subMenu == 0)
		{
			if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
				volatileConfig.nextMenu = EXIT_MENU;
		}
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}

void applyDefaultConfig (void)
{
	// Phase Polarity
	configPhasePolarity(userConfig.phasePolarity);

	// Outputs
	EnableOutputSPDIF();
	EnableOutputTOSLINK();
	EnableOutputAES_EBU();
	EnableOutput();

	// Listen Input & Record Input
	configListen();
	configRecord();

	// Brightness
	dispBrightness(BRIGHTNESS_HIGH);

  // Sonic8 filters
  setUpsamplingFilters(SONIC2_UP_SAMPLING_FILTERS_MIN);

  // Sonic8 DC offset
  setDcOffsetLeft(0);
  setDcOffsetRight(0);

  // Sonic8 phase_eq
  setPhaseEq(0);
}

// *****************************************************************************
//
// Menu VERSION
//
// *****************************************************************************
void menuVersion(void)
{
	unsigned char cmd = NO_CMD;

	volatileConfig.nextMenu = CURRENT_MENU;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1(SOFT_VERSION_STRING);
			dispClearLine2();
			volatileConfig.displayRefresh = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				volatileConfig.nextMenu = MENU_LOAD_DEFAULT;
				break;

			case CMD_RIGHT:
				volatileConfig.nextMenu = MENU_MODULE_VERSION;
				break;

			case CMD_MENU:
				// Exit from menu
				volatileConfig.nextMenu = EXIT_MENU;
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
			volatileConfig.nextMenu = EXIT_MENU;
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}


// *****************************************************************************
//
// Menu MODULE VERSION
//
// *****************************************************************************
void menuModuleVersion(void)
{
	unsigned char cmd = NO_CMD;

	volatileConfig.nextMenu = CURRENT_MENU;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
      char dbuff[20];

      sprintf(dbuff, "MOD VE %02X/%02X",
              (unsigned char)getSonic2Version(),
              (unsigned char)getSonic2VersionMinor());
			dispPutLine1(dbuff);

      sprintf(dbuff, "ID %02X/%02X",
              (unsigned char)getSonic2ID(),
              (unsigned char)getSonic2SID());
			dispPutLine2(dbuff);

			volatileConfig.displayRefresh = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				volatileConfig.nextMenu = MENU_VERSION;
				break;

			case CMD_RIGHT:
				volatileConfig.nextMenu = MENU_MODULE_UPDATE;
				// Do nothing
				break;

			case CMD_MENU:
				// Exit from menu
				volatileConfig.nextMenu = EXIT_MENU;
				break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
			volatileConfig.nextMenu = EXIT_MENU;
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}


// *****************************************************************************
//
// Menu MODULE VERSION
//
// *****************************************************************************
void menuModuleUpdate(void)
{
	unsigned char cmd = NO_CMD;
	static int firm_detected = 0;

	volatileConfig.nextMenu = CURRENT_MENU;

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
			dispPutLine1("  MD Update ");
			dispClearLine2();
      firm_detected =
        DetectFileForModuleDspFirmwareUpdate(MODULE_DSP_FIRMWARE_FILENAME);
      if (firm_detected == 1)
      {
        dispPutLine2("img ok  ");
      }
      else
      {
        dispPutLine2("no img  ");
      }

			volatileConfig.displayRefresh = NO_REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				volatileConfig.nextMenu = MENU_MODULE_VERSION;
				break;

			case CMD_RIGHT:
#if defined(USE_DEBUG_MENU_ENTRY)
				volatileConfig.nextMenu = MENU_DEBUG;
#endif
				// Do nothing
				break;

			case CMD_MENU:
				// Exit from menu
				volatileConfig.nextMenu = EXIT_MENU;
				break;

			case CMD_ENTER:
        if (firm_detected == 1)
        {
          led_cmd_on();
          mdu_firmware_update(MODULE_DSP_FIRMWARE_FILENAME);
          led_cmd_off();
          timeout_menu = readTimerCounter();
          volatileConfig.displayRefresh = REFRESH;
          volatileConfig.nextMenu = MENU_MODULE_VERSION;
        }
        break;

			default:
				break;
		}

		// Exit from menu with timeout
		if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
			volatileConfig.nextMenu = EXIT_MENU;
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);
}


#if defined(USE_DEBUG_MENU_ENTRY)

extern int g_cnif_cnt;
extern int g_last_pcm1792_cmd;
//extern int g_device_type;
extern int g_keep_dac_muted;

void flashSectorErase(dword address);
byte flashReadStatus(void);
void flashReadData(dword address, byte* buffer, word len);
void flashWriteData(dword address, byte* buffer, word len);

void menuDebug(void)
{
	unsigned char cmd = NO_CMD;
  char subMenu = 0;

	volatileConfig.nextMenu = CURRENT_MENU;

  dispClearLine1();
  dispClearLine2();

	do
	{
		if (volatileConfig.displayRefresh == REFRESH)
		{
      char dbuff[20];
      unsigned char id_buff[10];

//      sprintf(dbuff, "irq = %i     ", dbio_get_s8_nirq());
//      sprintf(dbuff, "%i/%i      ", g_cnif_cnt, g_last_pcm1792_cmd);

      sprintf(dbuff, "%i/%i %i   ", g_cnif_cnt, g_last_pcm1792_cmd,
              link_get_trig_in());

			dispPutLine1(dbuff);

			volatileConfig.displayRefresh = REFRESH;
		}

		cmd = getCmd();

		if (cmd != NO_CMD)
			timeout_menu = readTimerCounter();

		switch(cmd)
		{
			case CMD_LEFT:
				volatileConfig.nextMenu = MENU_MODULE_UPDATE;
				break;

			case CMD_RIGHT:
        volatileConfig.nextMenu = MENU_POLARITY;
				break;

#if 0
			case CMD_MENU:
				// Exit from menu
				volatileConfig.nextMenu = EXIT_MENU;
				break;

			case CMD_ENTER:
      {
        led_cmd_on();

        g_keep_dac_muted = g_keep_dac_muted ? 0 : 1;

        dispClearLine1();
        if (g_keep_dac_muted)
        {
          dispPutLine1("MUTED   ");

          mutePCM1792(DAC_ID_LEFT);
          mutePCM1792(DAC_ID_RIGHT);
        }
        else
        {
          dispPutLine1("NORM USE");

          unmutePCM1792(DAC_ID_LEFT);
          unmutePCM1792(DAC_ID_RIGHT);
        }

        delay_ms(1000);
        dispClearLine1();
        led_cmd_off();

        break;
      }
#endif

#if 0
			case CMD_ENTER:
				if (subMenu == 0)
				{
					// Enter current setting
					subMenu = 1;
          led_cmd_on();
				}
				else
				{
          g_keep_dac_muted = g_keep_dac_muted ? 0 : 1;
				}

        if (g_keep_dac_muted)
        {
          dispPutLine2("MUTED   ");
          dbio_s8_force_invalid_mode();
        }
        else
        {
          dispPutLine2("UNMUTED ");
          dbio_s8_disable_forced_invalid_mode();
        }

				break;
#endif

			case CMD_MENU:
				if (subMenu == 0)
        {
					// Exit from menu
					volatileConfig.nextMenu = EXIT_MENU;
        }
				else
				{
					// Return to menu
          dispClearLine2();
					subMenu = 0;
          led_cmd_off();
				}
				break;


			default:
				break;
		}

		// Exit from menu with timeout
//		if (readTimerDiff(timeout_menu) > TIME_OUT_MENU)
//			volatileConfig.nextMenu = EXIT_MENU;
	}
	while (volatileConfig.nextMenu == CURRENT_MENU);

  dispClearLine1();
}

#endif


/** E N D   O F   F I L E ****************************************************/
