
/*
 * File:         WM8805
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  I2C Digital Input Receiver (DIR) driver
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef	__WM8805
#define	__WM8805

/** I M P O R T A N T   N O T I C E ******************************************
 *	Upon power-up, the I2C/SDA signal (tied to SDI/HWM pin of the WM8805)
 *	must be help high to select the software mode control.
 *****************************************************************************/


/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <i2c.h>

#include "periph_timer.h"


/** D E F I N E S ************************************************************/
#define WM8805_I2C_ADDR			0x77	// address of I2C slave device

// WM8805 Interrupt Pin
#define WM8805_INT_PIN			PORTEbits.RE8
#define WM8805_INT_LOW			0x00
#define WM8805_INT_TOGGLE		0x55
#define WM8805_INT_HIGH			0x01

// WM8805 Status
#define FS_44_48   				0x01
#define FS_88_96   				0x02
#define FS_176_192 				0x03
#define FS_UNKNOWN				0xFD
#define UNLOCKED  				0xFE
#define DIR_ERROR				0xFF

// WM8805 Interrupts
#define WM8805_INT_UNLOCK		0x01
#define WM8805_INT_NON_AUDIO	0x10
#define WM8805_INT_REC_FREQ		0x80



/** E X T E R N S ************************************************************/
extern unsigned char WM8805_status;


/** D E C L A R A T I O N S **************************************************/
char WM8805_init (void);
unsigned char WM8805_get_interrupt(void);
unsigned char WM8805_check_int_pin(void);
unsigned char WM8805_get_status(void);
char WM8805_RX_select (unsigned char channel);


/** E N D   O F   F I L E ****************************************************/
#endif
