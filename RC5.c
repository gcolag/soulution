
/*
 * File:         rc5
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Remote control management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include "rc5.h"
#include "io.h"


/** D E F I N E S ************************************************************/
#define	STATE_START		0x00
#define	STATE_1D			0x01
#define	STATE_1M			0x02
#define	STATE_0M			0x03
#define	STATE_0D			0x04

// Timeouts for RC5 receiver
//#define	TIMEOUT_2MS			187		// 2ms * 65535 / Ttimer4
//#define	TIMEOUT_1_2MS		112		// 1200us, si sup => 1800us, sinon 900us

#define	TIMEOUT_2MS			500		// 2ms * 65535 / Ttimer4
#define	TIMEOUT_1_2MS		300		// 1200us, si sup => 1800us, sinon 900us

//#define	TIMEOUT_2MS			374		// 2ms * 65535 / Ttimer4
//#define	TIMEOUT_1_2MS		224		// 1200us, si sup => 1800us, sinon 900us

/*
 0xEA  : 234
 0x1C0 : 448
 */

/** V A R I A B L E S ********************************************************/
static unsigned char 	state;
static unsigned char 	bitCount;
static char 			error;
static unsigned int 	timeout;
static unsigned int 	buffer;
static rcKeyStruct		key_RC5;
static unsigned char	oldToggle;

static char				newRcKey;
static int				timeout2;

int g_keyid;
/** D E C L A R A T I O N S **************************************************/
static void Decode_RC(void);


/** P R I V A T E  P R O T O T Y P E S ***************************************/
void mOpenCapture5(unsigned int p1, unsigned int p2);


/** I N T E R R U P T S ******************************************************/

int g_status;

// IC5 ISR - RC5 signal detection
void __attribute__((interrupt, no_auto_psv)) _IC5Interrupt()
{
#if 0
  g_status = g_status ? 0 : 1;
  STATUS_LED  = g_status;
#endif

	Decode_RC();
	
	// Clear IC5 interrupt flag.
	IFS2bits.IC5IF = 0; 
}


/** F U N C T I O N S ********************************************************/

// This function is used for compatibility with PIC18 functions.
void mOpenCapture5(unsigned int edge, unsigned int interrupt)
{
	//--------------------------------------------------------------------------
	// If CloseCapture5 is omitted the MCU Resets at the second RC5 frame. 
	// A  work-around is to call CloseCapture5 function which sets 
  // IC5CONbits.ICM = 0.
	//--------------------------------------------------------------------------
	
#if 0
	// Close Capture5
	IC5CONbits.ICM = 0;
	
	if (edge == IC_EVERY_FALL_EDGE)
		// Open input capture IC5, detect every falling edge
		IC5CON = 0b0000000010000010;
	else
		// Open input capture IC5, detect every rising edge
		IC5CON = 0b0000000010000011;
#else
	// Close Capture5
	IC5CON1bits.ICM = 0;

	if (edge == IC_EVERY_FALL_EDGE)
  {
		// Open input capture IC5, detect every falling edge
		IC5CON1 = 0b0000010000000010;
		IC5CON2 = 0b0000000000000000;
  }
	else
  {
		// Open input capture IC5, detect every rising edge
		IC5CON1 = 0b0000010000000011;
		IC5CON2 = 0b0000000000000000;
  }
#endif
	
	if (interrupt == IC_INT_ON)
	{
		// Enable Capture5 interrupt
	    IFS2bits.IC5IF = 0;		// Clear IF bit
	    IPC9bits.IC5IP = 5;		// Assigning Interrupt Priority
	    IEC2bits.IC5IE = 1;		// Interrupt Enable
	}
	else
	{
		// Disable Capture5 interrupt
	    IFS2bits.IC5IF = 0;		// Clear IF bit
	    IPC9bits.IC5IP = 5;		// Assigning Interrupt Priority
	    IEC2bits.IC5IE = 0;		// Interrupt Enable
	}
}

/*****************************************************************************/
/* initRC
 *
 * Description	:	
 *					This function initializes the ECCP2 and Timer1 peripherals used for
 *					the Remote Control.
 */
/*****************************************************************************/
void initRc(void) 
{
	// Init variables
	newRcKey = 1;
	oldToggle = 0;
	
	mOpenCapture5(IC_EVERY_FALL_EDGE, IC_INT_ON);
	
	IFS2bits.IC5IF = 0; 
	
	// Works with TMR2 as a time base, TMR2 interrupt disabled.
	// ------------------------------------------------------------------------
	// Configuration of timer 2.
	// Internal source is Fosc = 12MHz (EC)
	// Fcy = Fosc/2 = 6MHz. Ftmr2 = Fcy / 64 = 93.75 kHz 
  // -> T = 1/93.75kHz = 10.67us.
	// Ttmr2 = 16406 * 64 / 6MHz = 175ms
	// ------------------------------------------------------------------------
	OpenTimer2(T2_ON | T2_IDLE_CON | T2_GATE_OFF | T2_PS_1_64 | T2_SOURCE_INT, 
             0xFFFF);
	
	key_RC5.dev = NO_KEY;
	key_RC5.lastdev = NO_KEY;
	key_RC5.id = NO_KEY;
	key_RC5.lastid = NO_KEY;
	key_RC5.count = 0;
	
	timeout2 = readTimerCounter();
}

/*****************************************************************************/
/* readRCKey
 *
 * Description	:	
 *					This function updates the remote state (which button is pressed
 *					and how many times it is pressed).
 *
 * *rc			:	Address of the data which contains the new values.
 */
/*****************************************************************************/
void readRCKey (volatile rcKeyStruct * rc)
{
	// Disable global interrupts
	__asm__ volatile("disi #0x3FFF");
	
	if (readTimerDiff(timeout2)  > 300)		// No RC5 command received for 300ms 
//	if (readTimerDiff(timeout2)  > 780)		// No RC5 command received for 300ms 
//	if (readTimerDiff(timeout2)  > 111)		// No RC5 command received for 300ms 
                                        // -> the next is a new one
	{
		if (key_RC5.lastid == 0x20)		// DEBUG
			Nop();
		
		newRcKey = 1;
		rc->dev = key_RC5.lastdev;
		rc->lastid = key_RC5.lastid;		// Get last command after key released
		key_RC5.lastdev = NO_KEY;
		key_RC5.lastid = NO_KEY;
		timeout2 = readTimerCounter();
	}
	else
	{
//		rc->lastdev = NO_KEY;
		rc->lastid = NO_KEY;
		rc->dev = key_RC5.dev;
	}
	
	rc->id = key_RC5.id;
	rc->count = key_RC5.count;
	key_RC5.dev = NO_KEY;
	key_RC5.id = NO_KEY;
		
	// Enable global interrupts
	__asm__ volatile("disi #0x0000");
}


int g_rc_timer;

/*****************************************************************************/
/* Decode_RC
 *
 * Description	:	
 *					This function decodes the RC5 code by detecting rising or falling
 *					edges on the ECCP2 input pin, and the time elapsed between two
 *					changing edges.
 */
/*****************************************************************************/
static void Decode_RC(void)
{
	unsigned char deviceId;
	unsigned char toggleState;
	unsigned char keyId;
	unsigned int temp;

	error = 0;
	bitCount = 0;
	buffer = 0x0000;
	state = STATE_START;

/*
  timeout = ReadTimer2();
  char dbuff2[20];
  sprintf(dbuff2, "%04X", timeout);
  fpd_write_string(0, 8, dbuff2);
*/

//  g_rc_timer = ReadTimer2();

	do
	{
		switch (state)
		{
			case STATE_0M:
				// Store 0 bit
				buffer &= ~((unsigned int)(1) << (14 - bitCount));
	
				if (++bitCount == 13)
					break;
	
				timeout = ReadTimer2();
				
				// Attente du flanc
				while((IR_PIN != 0) && ((ReadTimer2() - timeout) < TIMEOUT_2MS));
	
				if (IR_PIN != 0)
					error = -1;
				
				temp = ReadTimer2();
//        g_rc_timer = temp - timeout;

				if ((temp - timeout) > TIMEOUT_1_2MS)
					// 1800us
					state = STATE_1M;
				else
					// 900us
					state = STATE_0D;
				break;



			case STATE_0D:
				timeout = ReadTimer2();
				
				// Attente du flanc montant
				while((IR_PIN != 1) && ((ReadTimer2() - timeout) < TIMEOUT_1_2MS));
	
				if (IR_PIN != 1)
					error = -2;
	
				state = STATE_0M;
				break;



			case STATE_1D:
				timeout = ReadTimer2();
				
				// Attente du flanc
				while((IR_PIN != 0) && ((ReadTimer2() - timeout) < TIMEOUT_1_2MS));
				
				if (IR_PIN != 0)
					error = -3;

				state = STATE_1M;
				break;



			case STATE_1M:
				// Store 1 bit
				buffer |= (unsigned int)(1) << (14 - bitCount++);
	
				timeout = ReadTimer2();
				
				// Attente du flanc montant
				while((IR_PIN != 1) && ((ReadTimer2() - timeout) < TIMEOUT_2MS));
	
				if (IR_PIN != 1)
					error = -4;
	
				temp = ReadTimer2();
				if ((temp - timeout) > TIMEOUT_1_2MS)
					// 1800us
					state = STATE_0M;
				else
					// 900us
					state = STATE_1D;
				break;



			case STATE_START:
				timeout = ReadTimer2();
	
				// Attente du flanc montant
#if 1
				while((IR_PIN != 1) && ((ReadTimer2() - timeout) < TIMEOUT_2MS));
#else
        // DEBUG
        // Used for calibration
        //
				while ((IR_PIN != 1) && ((ReadTimer2() - timeout) < 2000));

        temp = ReadTimer2();
        g_rc_timer = temp - timeout;
        error = -10;
        break;
#endif

				if (IR_PIN != 1)
					error = -5;
	
				temp = ReadTimer2();
//        g_rc_timer = temp - timeout;

				if ((temp - timeout) > TIMEOUT_1_2MS)
					// 1800us
					state = STATE_0M;
				else
					// 900us
					state = STATE_1D;
				break;
			
			default:
				break;
		}

	} while ((error == 0) && (bitCount < 13));
	
#if 0
  if (error)
  {
    while (IR_PIN != 0) ;
    temp = ReadTimer2();
    g_rc_timer = temp - timeout;
  }
#endif

	if (error == 0)
	{
		// Device ID	---xxxxx------ 
		deviceId 	= ((buffer >> 8) & 0x1F);
		
		// Toggle state	--x-----------
		toggleState	= ((buffer >> 13) & 0x01);
		
		// Key ID		--------xxxxxx
		keyId 		= ((buffer >> 2) & 0x3F);
    g_keyid = keyId;

		if (newRcKey)
			key_RC5.count = 0;
		else
		{
			if (oldToggle != toggleState)
				key_RC5.count = 0;
			else
				key_RC5.count++;
		}
		oldToggle = toggleState;
		newRcKey = 0;
		timeout2 = readTimerCounter();
		
		// Save key ID
		key_RC5.dev = deviceId;
		key_RC5.lastdev = deviceId;
		key_RC5.id = keyId;
		key_RC5.lastid = keyId;
	}
}


/*****************************************************************************/


int rc5_get_error(void)
{
  return error;
}


int rc5_get_lastid(void)
{
  return key_RC5.lastid;
}


/** E N D   O F   F I L E ****************************************************/

