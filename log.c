
/*
 * File:         log
 * Based on:
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 *
 * Created:      2015/06/04
 * Description:  Message logging facilities
 *
 * History :
 *
 *   2015/06/04  1.0  Initial release
 *
 */

/*****************************************************************************/


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

// FIXME
// declared in stdio.h but not seen ???
//
//int vsnprintf(char *, size_t, const char *, _Va_list);


/*****************************************************************************/


typedef void (*write_type)(char*, int);

static write_type g_write_func = (write_type)NULL;


/*****************************************************************************/


void log_init(write_type write_func)
{
  g_write_func = write_func;
}


/*****************************************************************************/


void log_msg(char* msg)
{
  if (g_write_func != NULL)
  {
    g_write_func(msg, strlen(msg));
//    g_write_func("\r\n", 2);
  }
}


/*****************************************************************************/


void log_msg_fmt(char* format, ...)
{
  va_list ap;
  const int dbuff_size = 60;
  char dbuff[dbuff_size];
//  int add_cr = 0;
  int len;

  va_start(ap, format);
  vsnprintf(dbuff, dbuff_size-1, format, ap);
  va_end(ap);

  len = strlen(dbuff);

#if 0
  if (dbuff[len-1] == '\n')
  {
    if (len > 1)
      add_cr = 1;
    else if (dbuff[len-2] != '\r')
      add_cr = 1;
  }
  if (add_cr)
    strcat(dbuff, "\r");
#endif

#if 1
  if (len > 0)
    if (dbuff[len-1] == '\n')
      dbuff[len-1] = 0;
#endif

  log_msg(dbuff);
}


/*****************************************************************************/
