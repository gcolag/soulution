
/*
 * File:         pca9555
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/16
 * Description:  PCA9555 driver
 *
 * History :
 *
 *   2015/04/16  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef	__PCA9555_H
#define	__PCA9555_H

/*****************************************************************************/


#include <p24fxxxx.h>
#include <i2c.h>


/*****************************************************************************/


#define PCA9555_REG_INPUT_PORT_0    0x00
#define PCA9555_REG_INPUT_PORT_1    0x01
#define PCA9555_REG_OUTPUT_PORT_0   0x02
#define PCA9555_REG_OUTPUT_PORT_1   0x03
#define PCA9555_REG_POL_INV_PORT_0  0x04
#define PCA9555_REG_POL_INV_PORT_1  0x05
#define PCA9555_REG_CONFIG_PORT_0   0x06
#define PCA9555_REG_CONFIG_PORT_1   0x07


#define PCA9554_REG_INPUT_PORT      0x00
#define PCA9554_REG_OUTPUT_PORT     0x01
#define PCA9554_REG_POL_INV_PORT    0x02
#define PCA9554_REG_CONFIG_PORT     0x03


/*****************************************************************************/


typedef struct pca9555_dev_type
{
  unsigned char addr;
  unsigned char shadow0;
  unsigned char shadow1;
} pca9555_dev_type;


/*****************************************************************************/


int  pca9555_reg_wr(pca9555_dev_type* dev,
                    unsigned char reg,
                    unsigned char data);
int  pca9555_reg_rd(pca9555_dev_type* dev,
                    unsigned char reg, 
                    unsigned char* data);
void pca9555_bit_set(pca9555_dev_type* dev, unsigned char bit);
void pca9555_bit_clr(pca9555_dev_type* dev, unsigned char bit);
int  pca9555_bit_get(pca9555_dev_type* dev, unsigned char bit);

void pca9554_bit_set(pca9555_dev_type* dev, unsigned char bit);
void pca9554_bit_clr(pca9555_dev_type* dev, unsigned char bit);
int  pca9554_bit_get(pca9555_dev_type* dev, unsigned char bit);


/*****************************************************************************/

#endif /* __PCA9555_H */

