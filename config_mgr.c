
/*
 * File:         config_mgr
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Init, running & standby management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:
 *
 */

/** I N C L U D E S **********************************************************/


#include "cpld_dsp_board.h"
#include "dbio.h"
#include "periph_spi.h"
#include "cs8416.h"
#include "mcp23s17.h"
#include "fp_display.h"
#include "dacbio.h"

#include "keypad.h"
#include "config_mgr.h"


/** V A R I A B L E S ********************************************************/


static unsigned int timeout_skip;


/** F U N C T I O N S ********************************************************/
/*****************************************************************************
 * powerOnInit
 *
 * Description	:	This function initializes the MCU
 *
 *****************************************************************************/
void powerOnInit(void)
{
	volatileConfig.state = STATE_INIT;

	standbyIO();

	// FRC divider disabled (12MHz input to PLL -> output = 4 * 12MHz = 48MHz)
	// and Fosc = 48MHz / 2 = 24MHz.
	CLKDIVbits.RCDIV = 0;

	// Initialize timers and dedicated interrupts
	initTimer();

  led_cmd_on();

	// Initialize the keypad and remote
	initKeypad();
	initRc();

	// Init RS232 interface
	initRS232();

	// Load saved configuration
	loadConfigFromFlash();


  volatileConfig.factory_mode = 0;
  if ((get_key5() == 0) && (get_encoder_A_push_button() == 0))
	{
    // To enter the factory menu
    // press the encoder + the PROGRAM key
    // while plugging the CD760 in.
    volatileConfig.factory_mode = 1;
		initSystem();								// Start the system
		volatileConfig.state = STATE_EXTERNAL_INPUT;
	}
  else if ( (userConfig.versionSoft == SOFT_VERSION) &&
            (userConfig.StartMode == STARTMODE_AUTO))
  {
    // If userConfig has consistent values and start mode is set on AUTO
		initSystem();								// Start the system
  }
	else
	{
		volatileConfig.flagVoltage = 0; // Don't check voltage while in standby mode
		volatileConfig.state = STATE_STANDBY; // Go to standby mode
	}

}


/*****************************************************************************
 * loadDefaultConfig
 *
 * Description			: This function loads the default userConfig parameters.
 *
 *****************************************************************************/
void loadDefaultConfig (void)
{
//	userConfig.SourceMode			= SRC_CD_PLAYER;
	userConfig.SourceMode			= SRC_EXTERNAL_INPUT;
	userConfig.DigitalInput			= DIGITAL_INPUT_SPDIF;
//	userConfig.DigitalInput			= DIGITAL_INPUT_AES_EBU;
	userConfig.DigitalOutput		= ON;
	userConfig.volumeMode			= STD_CTRL;
	userConfig.volume				= 40;
	userConfig.volumeStart			= 10;
	userConfig.volumeMax			= 80;
	userConfig.balance				= 10;
	userConfig.StartMode			= STARTMODE_NORM;
  userConfig.ClockOutput    = CLOCK_OUTPUT_48K;
	userConfig.Brightness			= BRIGHTNESS_HIGH;
	userConfig.phasePolarity    = PHASEPOLARITY_0;
	userConfig.UpSamplingFilters    = SONIC2_UP_SAMPLING_FILTERS_MIN;
	userConfig.DcOffsetLeft     = 0;
	userConfig.DcOffsetRight    = 0;
	userConfig.PhaseEq          = 1;
	userConfig.versionSoft			= SOFT_VERSION;
}


/*****************************************************************************
 * initSystem
 *
 * Description: This function starts the CD Player after the Power button is
 *						  pressed from Standby state.
 *
 *****************************************************************************/

#define POWER_SUPPLY_POWER_ON_HACK 1

void initSystem(void)
{
	unsigned int timeout_trigger;
	char err;

	// Power ON
	configIO();
  psu_enable();
#if defined(POWER_SUPPLY_POWER_ON_HACK)
  delay_ms(2000);
  psu_disable();
  delay_ms(2000);
  psu_enable();
#endif

  led_cmd_off();

	// Trigger management
	if (userConfig.StartMode == STARTMODE_LINK)
	{
    link_assert_pre_mute();
		link_assert_link_on();
		timeout_trigger = readTimerCounter();
	}

	// Wait for all the voltages to be correct
#if defined(POWER_SUPPLY_POWER_ON_HACK)
	delay_ms(5000);
#else
	delay_ms(1000);
#endif

	// Load saved configuration
	loadConfigFromFlash();

	// Test EEPROM data validity
	if (userConfig.versionSoft != SOFT_VERSION)
	{
    led_cmd_on();
		delay_ms(500);
    led_cmd_off();

		// Data in EEPROM not valid -> Load default configuration
		loadDefaultConfig();
		writeConfigToFlash();
	}


   if ((userConfig.volumeMode == STD_CTRL) || (userConfig.volumeMode == LEEDH_CTRL)) {

      setSonic2VolumeReg(userConfig.volumeMode, 1);
   }

//   setVolume(0);

	// Initialize the display
	dispInit();
	dispBrightness(userConfig.Brightness);

  if (volatileConfig.factory_mode)
  {
    dispPutLine1("FACTORY MODE");
		delay_ms(2000);
  }

	switch (userConfig.StartMode)
	{
		case STARTMODE_LINK:
			if (link_get_trig_in() == 0)		// Slave mode
			{
				dispPutLine1("LINK-CONNECT");
				dispPutLine2("    MUTE");
			}
			else					// Master mode
			{
				dispPutLine1("760 STARTING");
				dispPutLine2("    LINK");
			}
			break;

		case STARTMODE_AUTO:
      dispPutLine1("760 STARTING");
			dispPutLine2("    AUTO");
			break;

		case STARTMODE_NORM:
		default:
      dispPutLine1("760 STARTING");
			dispPutLine2("    NORM");
			break;
	}

  // Additionnal delay ; the MR-MOD takes a long time to boot and
  // before it is up, the MUTE line is not configured. If the
  // Network input is select, this will cause a plops.
  // -> wait enough here to be sure that the MR-MOD MUTE line is
  //    accurate.
  // Tests show that :
  // - with 4000ms delay we have a plop
  // - with 5000ms delay we have no plop
  // -> choose 6000ms for security
  //
	delay_ms(6000);

	// Enable I2C communication port, Master, 100kHz
	OpenI2C1(I2C_ON | I2C_SLW_DIS, 230);

/*
	if (test_voltage())
	{
		dispPutLine1(" PWR-FAILURE");
		dispPutLine2("     OFF");
		delay_ms(1000);
		asm("RESET");	// RESET
	}
*/

/*
	// Initialize the CD drive
	VmkInit();

	delay_ms(1000);

	// Init CD tray & door
	if (Init_CD_Tray())
	{
		// Problem with the CD tray
		dispPutLine1("  PB CD TRAY");
		dispClearLine2();
    vmk_set_rdy()
		VMK_SET_RDY = 0;
		PSU_ENABLE = PSU_OFF;
		while(1)
		{
			if (KEY3 == 0)	// Key POWER
			{
				dispClearLine1();
				dispPutLine2("     OFF");
				delay_ms(2000);
				asm("RESET");	// RESET
			}
		}
	}
	// Tray is closed
*/

	// Configure boards and check I2C
	err = initBoards();
	if (err == -1)
	{
		unlockI2C();
		dispPutLine2("I2C FAIL");
		delay_ms(5000);
	}
	else if (err == -2)
	{
		dispPutLine2("S8 ERROR");
		delay_ms(5000);
	}

  if (dacbio_left_detect() != 0)
  {
		dispPutLine2("DACL ERR");
		delay_ms(5000);
  }

  if (dacbio_right_detect() != 0)
  {
		dispPutLine2("DACR ERR");
		delay_ms(5000);
  }

#if 1
	// Trigger management
	if (userConfig.StartMode == STARTMODE_LINK)
	{
		while ( (readTimerDiff(timeout_trigger) < 5000) &&
            (link_get_link_feedback() == 0) ) ;
    link_deassert_pre_mute();	// Unmute the preamplifier
		delay_ms(50);

    // Slave should have released the feedback
		if (link_get_link_feedback() == 0)
		{
			dispPutLine1("  LINK-ERROR");
			delay_ms(5000);
		}
	}
#endif

	// Initialize encoder
	initEncoderA();
	initEncoderB();

	// Enable external interrupts
	config_IO_interrupts();

  // Configure Sonic
  setUpsamplingFilters(userConfig.UpSamplingFilters);
  setDcOffsetLeft(userConfig.DcOffsetLeft);
  setDcOffsetRight(userConfig.DcOffsetRight);
  setPhaseEq(userConfig.PhaseEq);

	// Update the general state
	volatileConfig.state = userConfig.SourceMode;

	// Initialize configuration variables
	volatileConfig.prevState					= STATE_STANDBY;
	volatileConfig.setVolume					= OFF;
	volatileConfig.flagVoltage					= 1;		// Check voltage from now
	volatileConfig.flagOvercurrent				= OVERCURRENT_INHIBITED;
	volatileConfig.timeout_overcurrent			= 0;
	volatileConfig.timeout_overcurrent_long		= 0;
	volatileConfig.trig_in_detected				= 0;
	volatileConfig.mute                   = 0;

	volatileConfig.timeout_volume				= 0;
	volatileConfig.timeout_usb_detect			= 0;

	DAC_status									= STATE_DAC_INIT;
	clock_Rate									= CLOCKERROR;

	resetTimerCounter();
	timeout_overcurrent_inhibited				= 0;
	timeout_skip								= 0;

	// Clear the display
	dispClearAll();
}


/*****************************************************************************
 * standby
 *
 * Description			: Enter Standby mode
 *
 *****************************************************************************/
void standby(unsigned char mode)
{
	volatileConfig.state = STATE_STANDBY;
	volatileConfig.flagVoltage = 0;	// Don't check voltage while in standby mode

  io_disable_cn_int();
  if (volatileConfig.mute == 0)
  {
    setMute(ON);
  }
	// Mute output
	DisableOutput();
	ResetPCM1792();
  dbio_assert_dit_reset();
  link_assert_pre_mute();

	dispClearAll();

	if (mode == STB_NORMAL)
	{
		// Trigger management
		if (userConfig.StartMode == STARTMODE_LINK)
		{
      link_deassert_link_on();
			if (link_get_trig_in() == 1)	// Master triggered OFF
			{
				dispPutLine1("POWERING-OFF");
				dispPutLine2("    MUTE");
			}
			else
      {
				dispPutLine2("     OFF");	// Display "OFF"
      }
		}
		else
    {
			dispPutLine2("     OFF");		// Display "OFF"
		}
    dispClearLine3();
		delay_ms(1000);
	}

	// Disable external interrupts
	close_IO_interrupts();

	if (mode == STB_NORMAL)
	{
		// Save last configuration
		writeConfigToFlash();
  }

  // The analog MUTE relays are already active, so first power
  // the main supply down, and then only configure the chips
  // in standby mode.
  // The delay here is critical : if we put the chips in standby
  // too early after the main supply has been shutdown down, there
  // is a risk to re-enable the analog MUTE relays.
  //
	psu_disable();
	delay_ms(2000);
  dbio_standby();
  fpd_stby();
	standbyIO();

	if (mode == STB_NORMAL)
	{
		// Wait to prevent immediate power-on
		delay_ms(4000);
	}
	else
	{
		led_cmd_on();
		delay_ms(500);
		led_cmd_off();
		delay_ms(500);
		led_cmd_on();
		delay_ms(500);
		led_cmd_off();
		delay_ms(500);
		led_cmd_on();
		delay_ms(500);
		led_cmd_off();
		delay_ms(500);
		led_cmd_on();
		delay_ms(500);
		led_cmd_off();
		delay_ms(500);
	}

  led_cmd_on();
}


/*****************************************************************************
 * updateConfig
 *
 * Description	:	This function updates the configuration depending on the
 *					command passed in parameter
 *
 * Cmd			:	The command which has to be executed
 *
 * Returns		:	0
 *
 *****************************************************************************/
unsigned char updateConfig (unsigned char Cmd)
{
	switch (Cmd)
	{
		case CMD_MUTE:
      if (volatileConfig.mute == 0)
      {
        setMute(ON);
//        DataNotValidSonic2();
//        DisableOutput();
        volatileConfig.mute = 1;
      }
      else
      {
        setMute(OFF);
//        DataValidSonic2();
//        EnableOutput();
        volatileConfig.mute = 0;
      }
			break;

		case CMD_STANDBY:
			standby(STB_NORMAL);
			break;

		case CMD_WAKE_UP:
			initSystem();
      configListen();
			break;

		case CMD_VOL_CTL:
			if (volatileConfig.setVolume == OFF)
			{
				volatileConfig.setVolume = ON;
				volatileConfig.timeout_volume = readTimerCounter();
			}
			else
				volatileConfig.setVolume = OFF;
			break;

		case CMD_SEL_INPUT_MINUS:
			selectInput(-1);
			break;

		case CMD_SEL_INPUT_PLUS:
			selectInput(1);
			break;

		case CMD_VOL_DOWN:
			if (volatileConfig.setVolume == OFF)
				volatileConfig.setVolume = ON;
			setVolume(-1);
			volatileConfig.timeout_volume = readTimerCounter();
			break;

		case CMD_VOL_UP:
			if (volatileConfig.setVolume == OFF)
				volatileConfig.setVolume = ON;
			setVolume(1);
			volatileConfig.timeout_volume = readTimerCounter();
			break;

		case CMD_BAL_LEFT:
			setBalance(-1);
			volatileConfig.timeout_volume = readTimerCounter();
			break;

		case CMD_BAL_RIGHT:
			setBalance(1);
			volatileConfig.timeout_volume = readTimerCounter();
			break;

		case CMD_MENU:
			RS232_DISABLE_RX;
			menuSetup();
			RS232_ENABLE_RX;
			dispClearAll();
			break;

		case CMD_SRC_AES:
			userConfig.DigitalInput = DIGITAL_INPUT_AES_EBU;
			configListen();
			configRecord();
			break;

		case CMD_SRC_SPDIF:
			userConfig.DigitalInput = DIGITAL_INPUT_SPDIF;
			configListen();
			configRecord();
			break;

		case CMD_SRC_OPT:
			userConfig.DigitalInput = DIGITAL_INPUT_TOSLINK;
			configListen();
			configRecord();
			break;

		case CMD_SRC_USB:
			userConfig.DigitalInput = DIGITAL_INPUT_MEZZANINE_USB	;
			configListen();
			configRecord();
			break;

		case CMD_SRC_NET:
			userConfig.DigitalInput = DIGITAL_INPUT_MEZZANINE_NET	;
			configListen();
			configRecord();
			break;

		case NO_CMD:
		default:
			break;
	}
	return 0;
}


/** E N D   O F   F I L E ****************************************************/
