
/*
 * File:         keypad
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB S�rl
 *               Copyright ABC PCB S�rl 2015
 * Created:      2015/04/10
 * Description:  Keypad management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include "io.h"
#include "encoder.h"
#include "keypad.h"


/** V A R I A B L E S ********************************************************/


static volatile	unsigned char stateKeypad;		// Used to detect pressed buttons
static volatile	unsigned int waitCounter; 		// Timeout for a pressed button
static volatile unsigned char pushedKey; 			// Button currently pressed
static volatile keyStruct key_keypad;
volatile unsigned char mskRepeatKey;// Mask containing repeat mode of each key


/** D E C L A R A T I O N S **************************************************/


static void Decode_Keypad (void);


/** I N T E R R U P T S ******************************************************/

/*****************************************************************************
 * _T4Interrupt
 *
 * Description			: Timer 4 interrupt routine.
 *****************************************************************************/
void __attribute__((interrupt, no_auto_psv)) _T4Interrupt()
{
	Decode_Keypad();
	
	// Clear timer 4 interrupt flag.
	IFS1bits.T4IF = 0;
}


/** F U N C T I O N S ********************************************************/
/*****************************************************************************
 * initKeypad
 *
 * Description	:	- Resets the key detection system
 *					- Sets the key repeat delay to the default value
 *					- Sets repeat mode to 0 (all keys use repeat delay)
 *
 *****************************************************************************/
void initKeypad(void)
{	
	pushedKey				=	NO_KEY;
	key_keypad.id			=	NO_KEY;
	key_keypad.lastid		=	NO_KEY;
	key_keypad.count		=	0x0000;
	mskRepeatKey 			=	0x00;
	stateKeypad				= 	WAIT_FOR_PRESS;
	
	// -------------------------------------------------------------------------
	// Timer 4 is used for key detection. 
	// -------------------------------------------------------------------------
	// Configuration of timer 4 to generate an interrupt every 10ms.
	// Internal source is Fosc = 12MHz (EC) * 4 (PLL) = 48 MHz. 
	// Fcy = Fosc/2 = 24MHz. Ttmr4 = 65536 * 8 / Fcy = 22ms.
	// Internal source is Fosc = 12MHz (EC). 
	// Fcy = Fosc/2 = 6MHz. Ttmr4 = 16500 * 8 / Fcy = 22ms.
	// -------------------------------------------------------------------------
	OpenTimer4 (T4_ON | T4_IDLE_CON | T4_GATE_OFF | T4_PS_1_8 | T4_SOURCE_INT, 16500);
				
	ConfigIntTimer4 (T4_INT_ON | T4_INT_PRIOR_2);
}

/*****************************************************************************
 * readKeypad
 *
 * Description	:	This function updates the keypad state (which button is 
 *					pressed and how many times it is pressed).
 *
 * *keypad		:	Address of the data which contains the new values.
 *
 *****************************************************************************/
void readKeypad (keyStruct * keypad)
{
	// Disable global interrupts
	__asm__ volatile("disi #0x3FFF");
	
	keypad->id = key_keypad.id;
	keypad->count = key_keypad.count;
	keypad->lastid = key_keypad.lastid;
	
	key_keypad.lastid = NO_KEY;
	
	if ((mskRepeatKey | key_keypad.id) == key_keypad.id) 		// If mskRepeat bit is '0' (repeated),
		key_keypad.id = NO_KEY;						// after the next T4 interrupt:
												// - key_keypad.id will contain the key value
												// - key_keypad.count will be incremented
		
	// Enable global interrupts
	__asm__ volatile("disi #0x0000");
}

/*****************************************************************************
 * Decode_Keypad
 *
 * Description			: Decode the keypad state.
 *****************************************************************************/
void Decode_Keypad (void)
{
	unsigned char currentPress;

//	currentPress = ((unsigned char)PORTB >> 1) | 0xF1;		// RB2-3-4
//	currentPress &= (0xFE | encoder_get_push_button());   // Encoder push button

  currentPress = 
    (keypad_get_key3() << 3) | 
    (keypad_get_key2() << 2) | 
    (keypad_get_key1() << 1) | 
    (encoder_get_A_push_button() << 0) ;
  currentPress |= 0xF0;

	// Action � entreprendre
	switch (stateKeypad)
	{
		// Cas le plus probable: pas de touche appuy�e
		case WAIT_FOR_PRESS:
			if (currentPress != NO_KEY)
			{
				key_keypad.count = 0x0000;
				pushedKey = currentPress;
				key_keypad.id = pushedKey;
				waitCounter = readTimerCounter();
				
				if ((mskRepeatKey | pushedKey) == pushedKey)
					stateKeypad = REPEAT_PRESS;				// mskRepeat bit = 0
				else 
					stateKeypad = NO_REPEAT_PRESS;			// mskRepeat bit = 1
			}
		break;

		case REPEAT_PRESS:
			if (currentPress != pushedKey)					// Key released
			{
				key_keypad.id = NO_KEY;
				key_keypad.lastid = pushedKey;
				stateKeypad = WAIT_FOR_PRESS;
			}
			else if (readTimerDiff(waitCounter) > KEY_REPEAT_DELAY)
			{
				waitCounter = readTimerCounter();
				key_keypad.id	= pushedKey;
				key_keypad.count++;							// Incremented every KEY_REPEAT_DELAY ms.
			}
		break;
	
		case NO_REPEAT_PRESS:
			key_keypad.count++;								// Incremented every T4 interrupt (10ms).
			if (currentPress != pushedKey)				// Key released
			{
				key_keypad.id = NO_KEY;
				key_keypad.lastid = pushedKey;
				stateKeypad = WAIT_FOR_PRESS;
			} 
		break;
		
		default:
			break;
	}
}


/** E N D   O F   F I L E ****************************************************/
