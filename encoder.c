
/*
 * File:         encoder
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Encoder management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */
 
/** I N C L U D E S **********************************************************/


#include <incap.h>

#include "encoder.h"


/** V A R I A B L E S ********************************************************/


static volatile unsigned int	previousValueA;
static volatile unsigned int	currentValueA;
static volatile char			directionA;				// Direction of the encoder

static volatile unsigned int	previousValueB;
static volatile unsigned int	currentValueB;
static volatile char			directionB;				// Direction of the encoder

static volatile unsigned int timerEncoderA;			// Time of each edge
static volatile unsigned int timerRefEncoderA;		// time of the first edge
static volatile unsigned int counterEncoderA;	// Counter of the number of pulse
static volatile unsigned int counterRefEncoderA;// Reference of the number of pulse

static volatile unsigned int timerEncoderB;			// Time of each edge
static volatile unsigned int timerRefEncoderB;		// time of the first edge
static volatile unsigned int counterEncoderB;	// Counter of the number of pulse
static volatile unsigned int counterRefEncoderB;// Reference of the number of pulse


/** I N T E R R U P T S ******************************************************/
// IC1 ISR - Encoder signal detection
void __attribute__((interrupt, no_auto_psv)) _IC1Interrupt()
{
	captureEncoderIsrA();
	
	// Clear IC1 interrupt flag -> clear both flags (IC1 & IC2) in captureEncoderIsr ???
	IFS0bits.IC1IF = 0; 
}

// IC2 ISR - Encoder signal detection
void __attribute__((interrupt, no_auto_psv)) _IC2Interrupt()
{
	captureEncoderIsrA();
	
	// Clear IC2 interrupt flag.
	IFS0bits.IC2IF = 0; 
}

void __attribute__((interrupt, no_auto_psv)) _IC3Interrupt()
{
	captureEncoderIsrB();
	
	// Clear IC1 interrupt flag -> clear both flags (IC1 & IC2) in captureEncoderIsr ???
	IFS2bits.IC3IF = 0; 
}

void __attribute__((interrupt, no_auto_psv)) _IC4Interrupt()
{
	captureEncoderIsrB();
	
	// Clear IC1 interrupt flag -> clear both flags (IC1 & IC2) in captureEncoderIsr ???
	IFS2bits.IC4IF = 0; 
}

/** F U N C T I O N S ********************************************************/

// This function is used for compatibility with PIC18 functions.
void mOpenCapture1(unsigned int p1)
{
	// Use CloseCapture1() or IC1CONbits.ICM = 0 to close the peripheral.
//	IC1CONbits.ICM = 0;
	IC1CON1bits.ICM = 0;

//	OpenCapture1(IC_IDLE_CON | IC_TIMER2_SRC | IC_INT_1CAPTURE | p1);
	OpenCapture1(IC_IDLE_CON | IC_TIMER2_SRC | IC_INT_1CAPTURE | p1, 0);
	ConfigIntCapture1(IC_INT_ON | IC_INT_PRIOR_3);
}

// This function is used for compatibility with PIC18 functions.
void mOpenCapture2(unsigned int p1)
{
	// Use CloseCapture2() or IC2CONbits.ICM = 0 to close the peripheral.
//	IC2CONbits.ICM = 0;
	IC2CON1bits.ICM = 0;

//	OpenCapture2(IC_IDLE_CON | IC_TIMER2_SRC | IC_INT_1CAPTURE | p1);
	OpenCapture2(IC_IDLE_CON | IC_TIMER2_SRC | IC_INT_1CAPTURE | p1, 0);
	ConfigIntCapture2(IC_INT_ON | IC_INT_PRIOR_3);
}

// This function is used for compatibility with PIC18 functions.
void mOpenCapture3(unsigned int p1)
{
	// Use CloseCapture3() or IC1CONbits.ICM = 0 to close the peripheral.
	IC3CON1bits.ICM = 0;

	OpenCapture3(IC_IDLE_CON | IC_TIMER2_SRC | IC_INT_1CAPTURE | p1, 0);
	ConfigIntCapture3(IC_INT_ON | IC_INT_PRIOR_3);
}

// This function is used for compatibility with PIC18 functions.
void mOpenCapture4(unsigned int p1)
{
	// Use CloseCapture4() or IC2CONbits.ICM = 0 to close the peripheral.
	IC4CON1bits.ICM = 0;

	OpenCapture4(IC_IDLE_CON | IC_TIMER2_SRC | IC_INT_1CAPTURE | p1, 0);
	ConfigIntCapture4(IC_INT_ON | IC_INT_PRIOR_3);
}

/*****************************************************************************
 * initEncoder
 *
 * Description	:	Initialize the Capture modules for the encoder
 *
 *****************************************************************************/
void initEncoderA(void)
{
  currentValueA = (encoder_get_rot_A_B() << 9) | (encoder_get_rot_A_A() << 8);
	previousValueA = currentValueA;

	switch (currentValueA)
	{
		case 0x0000:
			mOpenCapture2(IC_EVERY_RISE_EDGE);
			mOpenCapture1(IC_EVERY_RISE_EDGE);
			break;

		case 0x0100:
			mOpenCapture2(IC_EVERY_RISE_EDGE);
			mOpenCapture1(IC_EVERY_FALL_EDGE);
			break;
		
		case 0x0200:
			mOpenCapture2(IC_EVERY_FALL_EDGE);
			mOpenCapture1(IC_EVERY_RISE_EDGE);
			break;

		case 0x0300:
			mOpenCapture2(IC_EVERY_FALL_EDGE);
			mOpenCapture1(IC_EVERY_FALL_EDGE);
			break;

		default:
			break;
	}
	directionA = IDLE;
	counterEncoderA = 0;
	timerEncoderA = readTimerCounter();
}

void initEncoderB(void)
{
  currentValueB = (encoder_get_rot_B_B() << 9) | (encoder_get_rot_B_A() << 8);
	previousValueB = currentValueB;

	switch (currentValueB)
	{
		case 0x0000:
			mOpenCapture4(IC_EVERY_RISE_EDGE);
			mOpenCapture3(IC_EVERY_RISE_EDGE);
			break;

		case 0x0100:
			mOpenCapture4(IC_EVERY_RISE_EDGE);
			mOpenCapture3(IC_EVERY_FALL_EDGE);
			break;
		
		case 0x0200:
			mOpenCapture4(IC_EVERY_FALL_EDGE);
			mOpenCapture3(IC_EVERY_RISE_EDGE);
			break;

		case 0x0300:
			mOpenCapture4(IC_EVERY_FALL_EDGE);
			mOpenCapture3(IC_EVERY_FALL_EDGE);
			break;

		default:
			break;
	}
	directionB = IDLE;
	counterEncoderB = 0;
	timerEncoderB = readTimerCounter();
}

/*****************************************************************************
 * readEncoderKey
 *
 * Description	:	This function updates the encoder state.
 *
 * *encoder		:	Address of the data which contains the new values.
 *
 *****************************************************************************/
void readEncoderKeyA (encoderStruct * encoder)
{
	// Disable global interrupts
	__asm__ volatile("disi #0x3FFF");
	
	encoder->speed = 1;
	encoder->counter = 1;
	encoder->timer = 0;
	encoder->direction = directionA;
	directionA = IDLE;
		
	// Enable global interrupts
	__asm__ volatile("disi #0x0000");
}

void readEncoderKeyB (encoderStruct * encoder)
{
	// Disable global interrupts
	__asm__ volatile("disi #0x3FFF");
	
	encoder->speed = 1;
	encoder->counter = 1;
	encoder->timer = 0;
	encoder->direction = directionB;
	directionB = IDLE;
		
	// Enable global interrupts
	__asm__ volatile("disi #0x0000");
}

/*****************************************************************************
 * captureEncoderIsr
 *
 * Description	:	Interrupt Service Routine for Capture Counter Mode
 *
 *****************************************************************************/
void captureEncoderIsrA(void)
{
  currentValueA = (encoder_get_rot_A_B() << 9) | (encoder_get_rot_A_A() << 8);

	if (IFS0bits.IC1IF || IFS0bits.IC2IF)
	{
		switch (previousValueA)
		{
			case 0x0000:
				switch (currentValueA)
				{
					case 0x0100:
						directionA = CCW;	// Clockwise rotation
						mOpenCapture2(IC_EVERY_RISE_EDGE);
						mOpenCapture1(IC_EVERY_FALL_EDGE);
						break;
					
					case 0x0200:
						directionA = CW;		// Not clockwise rotation
						mOpenCapture2(IC_EVERY_FALL_EDGE);
						mOpenCapture1(IC_EVERY_RISE_EDGE);
						break;

					default:
						break;
				}
				break;

			case 0x0200:
				switch (currentValueA)
				{
					case 0x0000:
						directionA = CCW;
						mOpenCapture2(IC_EVERY_RISE_EDGE);
						mOpenCapture1(IC_EVERY_RISE_EDGE);
						break;

					case 0x0300:
						directionA = CW;
						mOpenCapture2(IC_EVERY_FALL_EDGE);
						mOpenCapture1(IC_EVERY_FALL_EDGE);
						break;
					
					default:
						break;
				}
				break;

			case 0x0100:
				switch (currentValueA)
				{
					case 0x0000:
						directionA = CW;
						mOpenCapture2(IC_EVERY_RISE_EDGE);
						mOpenCapture1(IC_EVERY_RISE_EDGE);
						break;

					case 0x0300:
						directionA = CCW;
						mOpenCapture2(IC_EVERY_FALL_EDGE);
						mOpenCapture1(IC_EVERY_FALL_EDGE);
						break;
					
					default:
						break;
				}
				break;

			case 0x0300:
				switch (currentValueA)
				{
					case 0x0100:
						directionA = CW;
						mOpenCapture2(IC_EVERY_RISE_EDGE);
						mOpenCapture1(IC_EVERY_FALL_EDGE);
						break;
					
					case 0x0200:
						directionA = CCW;
						mOpenCapture2(IC_EVERY_FALL_EDGE);
						mOpenCapture1(IC_EVERY_RISE_EDGE);
						break;

					default:
						break;
				}
				break;
			
			default:
				break;
		}
		previousValueA = currentValueA;		
	}
}


void captureEncoderIsrB(void)
{
  currentValueB = (encoder_get_rot_B_B() << 9) | (encoder_get_rot_B_A() << 8);

	if (IFS2bits.IC3IF || IFS2bits.IC4IF)
	{
		switch (previousValueB)
		{
			case 0x0000:
				switch (currentValueB)
				{
					case 0x0100:
						directionB = CCW;	// Clockwise rotation
						mOpenCapture4(IC_EVERY_RISE_EDGE);
						mOpenCapture3(IC_EVERY_FALL_EDGE);
						break;
					
					case 0x0200:
						directionB = CW;		// Not clockwise rotation
						mOpenCapture4(IC_EVERY_FALL_EDGE);
						mOpenCapture3(IC_EVERY_RISE_EDGE);
						break;

					default:
						break;
				}
				break;

			case 0x0200:
				switch (currentValueB)
				{
					case 0x0000:
						directionB = CCW;
						mOpenCapture4(IC_EVERY_RISE_EDGE);
						mOpenCapture3(IC_EVERY_RISE_EDGE);
						break;

					case 0x0300:
						directionB = CW;
						mOpenCapture4(IC_EVERY_FALL_EDGE);
						mOpenCapture3(IC_EVERY_FALL_EDGE);
						break;
					
					default:
						break;
				}
				break;

			case 0x0100:
				switch (currentValueB)
				{
					case 0x0000:
						directionB = CW;
						mOpenCapture4(IC_EVERY_RISE_EDGE);
						mOpenCapture3(IC_EVERY_RISE_EDGE);
						break;

					case 0x0300:
						directionB = CCW;
						mOpenCapture4(IC_EVERY_FALL_EDGE);
						mOpenCapture3(IC_EVERY_FALL_EDGE);
						break;
					
					default:
						break;
				}
				break;

			case 0x0300:
				switch (currentValueB)
				{
					case 0x0100:
						directionB = CW;
						mOpenCapture4(IC_EVERY_RISE_EDGE);
						mOpenCapture3(IC_EVERY_FALL_EDGE);
						break;
					
					case 0x0200:
						directionB = CCW;
						mOpenCapture4(IC_EVERY_FALL_EDGE);
						mOpenCapture3(IC_EVERY_RISE_EDGE);
						break;

					default:
						break;
				}
				break;
			
			default:
				break;
		}
		previousValueB = currentValueB;		
	}
}


/** E N D   O F   F I L E ****************************************************/
