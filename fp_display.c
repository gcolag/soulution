
/*
 * File:         fp_display
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Front Pannel Display
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include <p24fxxxx.h>
#include <PPS.h>

#include "hdsp250x.h"


/*****************************************************************************/


typedef struct
{
  hdsp250x_dev_type* hdsp250x_dev;
  int pos;
} dev_info_type;


dev_info_type g_dev_info_var, *g_dev_info = &g_dev_info_var;
hdsp250x_dev_type g_hdsp250x_dev_var, *g_hdsp250x_dev = &g_hdsp250x_dev_var;


/*****************************************************************************/


// Display definition
// Define number of display modules and layout
//


#define NUMBER_OF_DISPLAY_MODULES 4


static dev_info_type* get_device(UINT8 line, UINT8 pos)
{
  hdsp250x_dev_type* hdsp250x_dev;

  g_dev_info->hdsp250x_dev = g_hdsp250x_dev;
  hdsp250x_dev = g_dev_info->hdsp250x_dev;

  g_dev_info->pos = pos;

  switch (line)
  {
    case 0 :
      if (pos < 8)
      {
        hdsp250x_dev->dev_index = 3;
      }
      else
      {
        hdsp250x_dev->dev_index = 2;
        g_dev_info->pos -= 8;
      }
      break;

    case 1:
      hdsp250x_dev->dev_index = 1;
      break;

    case 2:
      hdsp250x_dev->dev_index = 0;
      break;

    default:
      hdsp250x_dev->dev_index = 255;
  }

  return g_dev_info;
}


/*****************************************************************************/


void fpd_stby(void)
{
  hdsp250x_standby();
}


void fpd_init(void)
{
  hdsp250x_init();

  // Program User Defined Charset for all devices
  //
  {
    hdsp250x_dev_type* hdsp250x_dev;
    int i;

    g_dev_info->hdsp250x_dev = g_hdsp250x_dev;
    hdsp250x_dev = g_dev_info->hdsp250x_dev;

    for (i=0 ; i<NUMBER_OF_DISPLAY_MODULES ; i++)
    {
      hdsp250x_dev->dev_index = i;
      hdsp250x_init_udc(hdsp250x_dev);
    }
  }
}


void fpd_write_char(UINT8 line, UINT8 pos, char character)
{
  dev_info_type* dev_info;

  dev_info = get_device(line, pos);
  hdsp250x_write_char(dev_info->hdsp250x_dev, dev_info->pos, character);
}


void fpd_write_string(UINT8 line, UINT8 pos, char* msg)
{
  int i;

#if 0
  if ((line == 0) || (line == 1))
  {
    static int cnt=0;
    char dbuff[10];

    sprintf(dbuff, "%i", cnt++);
    fpd_write_string(2, 4, dbuff);
  }
#endif

  i = 0;
  while (msg[i] != 0)
  {
    fpd_write_char(line, pos+i, msg[i]);
    i++;
  }
}


void fpd_clear(void)
{
  fpd_write_string(0, 0, "                ");
  fpd_write_string(1, 0, "        ");

// FIXME : last line kept for debug
// uncomment in production release
//  fpd_write_string(2, 0, "        ");
}


void fpt_set_brightness(UINT8 brightness)
{
  int i;
  hdsp250x_dev_type hdsp250x_dev_var, *hdsp250x_dev = &hdsp250x_dev_var;

  for (i=0 ; i<NUMBER_OF_DISPLAY_MODULES ; i++)
  {
    hdsp250x_dev->dev_index = i;
    hdsp250x_set_brightness(hdsp250x_dev, brightness);
  }
}


/*****************************************************************************/

