
/*
 * File:         util.c
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/05/15
 * Description:  Toolbox
 *
 * History :
 *
 *   2015/05/15  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


void iToStr(int val, char* buff)
{
  int digit_0;
  int digit_1;
  int digit_2;
  int i = 0;

  if (val < 0)
  {
    val = -val;
    buff[i++] = '-';
  }

  digit_2 = (val / 100);
  digit_1 = (val % 100) / 10;
  digit_0 = (val %10);

  buff[i++] = '0' + digit_2;
  buff[i++] = '0' + digit_1;
  buff[i++] = '0' + digit_0;
  buff[i++] = 0;
}


/*****************************************************************************/
