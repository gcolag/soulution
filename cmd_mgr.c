
/*
 * File:         cmd_mgr
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Command manager
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:
 *
 */

/** I N C L U D E S **********************************************************/
#include "cmd_mgr.h"


/** V A R I A B L E S ********************************************************/


// To scan alternatively keypad, RC5 and the encoder
static unsigned char next2Scan = 0;

// To detect long and short press on encoder
static unsigned char encoderButton = STATE_ENCODER_IDLE;

unsigned char rcRepeatDelay;

extern unsigned char ProcessRC5(void);
extern unsigned char ProcessKeypad(void);
extern unsigned char processEncoderA(void);
extern unsigned char processEncoderB(void);
extern unsigned char processRS232(void);


/** F U N C T I O N S ********************************************************/
/*****************************************************************************
 * getCmd
 *
 * Description	:	This function looks for any action on the keypad, the remote
 *					or the encoder - depending on the value of next2Scan - and returns
 *					the corresponding command.
 *
 * Returns		:	Command code or NO_CMD, as defined in "cmd_mgr.h"
 *
 *****************************************************************************/
unsigned char getCmd(void)
{
	unsigned char cmd = NO_CMD;

	if (rs232_data_arrived)
	{
		cmd = processRS232();
		rs232_data_arrived = 0;
	}
	else
	{
		switch (next2Scan)
		{
         case 0:		// Keypad
            cmd = ProcessKeypad();
            next2Scan++; 			// next: scan RC5
            break;

         case 1:		// RC5
            cmd = ProcessRC5();
            if (volatileConfig.state == STATE_STANDBY)
            {
               if (userConfig.StartMode == STARTMODE_LINK)
                  next2Scan = 4;	// next: scan trigger
               else
                  next2Scan = 0;	// next: scan keypad
            }
            else
               next2Scan++;		// next: scan encoder
            break;

         case 2:	 	// Encoder A
            cmd = processEncoderA();
            next2Scan++;		// next: scan trigger
            break;

         case 3:	 	// Encoder B
            cmd = processEncoderB();
            if (userConfig.StartMode == STARTMODE_LINK)
               next2Scan++;		// next: scan trigger
            else
               next2Scan = 0;		// next: scan keypad
            break;

         case 4:		// Trigger
            //				cmd = getTrigger();
            next2Scan = 0;			// next: scan keypad
            break;

         default:
            next2Scan = 0;			// next: scan keypad
            break;
      }
   }
   return cmd;
}

/*****************************************************************************
 * ProcessKeypad
 *
 * Description	:	This function returns a command if a button on the keypad is
 *					pressed
 *
 * Returns		:	Command code or NO_CMD, as defined in "cmd_mgr.h"
 *
 *****************************************************************************/
unsigned char ProcessKeypad(void)
{
	keyStruct keypad;

	switch(volatileConfig.state)
	{
		case STATE_EXTERNAL_INPUT:		// DAC mode
			// Configure repeated keys then read the keypad state
			readKeypad(&keypad);

			switch (keypad.id)
			{
				case KEY_MUTE:
					if (keypad.count == 0)
						return CMD_MUTE;
					break;

				case KEY_PROGRAM:
					if (keypad.count == 0)
						return CMD_MENU;
					break;

				case KEY_POWER:
					if (keypad.count == 0)
						return CMD_STANDBY;
					break;

				case KEY_ENCODER:
					if (encoderButton == STATE_ENCODER_IDLE)
					{
						if (keypad.count == 2)	// After 2s
						{
							encoderButton = STATE_ENCODER_PUSHED;
							if (userConfig.volumeMode != OFF_CTRL)
								return CMD_VOL_CTL;
						}
					}
					break;

				case NO_KEY:
					if (keypad.lastid == KEY_ENCODER)
						encoderButton = STATE_ENCODER_IDLE;
					break;

				default:
					break;
			}
			break;

		case STATE_SETUP:				// SETUP mode
			// Configure repeated keys then read the keypad state
			mskRepeatKey &= 0xFE;		// ENCODER BUTTON: no repeat
			readKeypad(&keypad);

			switch (keypad.id)
			{
				case KEY_ENCODER:
					if (keypad.count == 0)
						return CMD_ENTER;
					break;

				case KEY_PROGRAM:
					if (keypad.count == 0)
						return CMD_MENU;
					break;

				default:
					break;
			} // switch (keypad.id)
			break;

		case STATE_STANDBY:				// STANDBY mode
			// Configure repeated keys then read the keypad state
			mskRepeatKey = 0x00;
			readKeypad(&keypad);

			if (keypad.id == KEY_POWER)
			{
				if (keypad.count == 0)
					return CMD_WAKE_UP;
			}
			break;

		case STATE_OVERCURRENT:
			if (keypad.id == KEY_POWER)
			{
				if (keypad.count == 0)
					return CMD_STANDBY;
			}
			break;

		case STATE_INIT:
			mskRepeatKey = 0x00;
			readKeypad(&keypad);

			if (keypad.id == KEY_OPEN_CLOSE)
			{
				if (keypad.count == 0)
					return CMD_OPEN_CLOSE;
			}
			break;

		default:
			break;
	}

	return NO_CMD;
}

/*****************************************************************************
 * ProcessRC5
 *
 * Description	:	This function returns a command if a button on the remote
 *					control is pressed
 *
 * Returns		:	Command code or NO_CMD, as defined in "cmd_mgr.h"
 *
 *****************************************************************************/
unsigned char ProcessRC5(void)
{
	volatile rcKeyStruct RC5;

	readRCKey(&RC5);						// Get RC5 pressed key

	if (RC5.dev == CODE_SOULUTION_DAC760)
	{
		switch (volatileConfig.state)
		{
			case STATE_EXTERNAL_INPUT:			// DAC mode
				switch (RC5.id)
				{
					case KEY_RC5_BACKWARD:
						if (volatileConfig.setVolume == ON)
							return CMD_BAL_LEFT;
						else
						{
							if (RC5.count == 0)
								return CMD_SEL_INPUT_MINUS;
						}
						break;

					case KEY_RC5_FORWARD:
						if (volatileConfig.setVolume == ON)
							return CMD_BAL_RIGHT;
						else
						{
							if (RC5.count == 0)
								return CMD_SEL_INPUT_PLUS;
						}
						break;

					case KEY_RC5_ENTER:
						if (RC5.count == 10)
						{
							if (userConfig.volumeMode != OFF_CTRL)
								return CMD_VOL_CTL;
						}
						break;

					case KEY_RC5_PROGRAM:
						if (RC5.count == 0)
							return CMD_MENU;
						break;

					case KEY_RC5_POWER_OFF:
					case KEY_RC5_POWER:
						if (RC5.count == 0)
							return CMD_STANDBY;
						break;

					default:
						break;
				} // switch (RC5.id)
				break;

			case STATE_SETUP:					// SETUP mode
				switch (RC5.id)
				{
          case KEY_RC5_MUTE:
						if (RC5.count == 0)
              return CMD_MUTE;
					break;

					case KEY_RC5_ENTER:
						if (RC5.count == 0)
							return CMD_ENTER;
						break;

					case KEY_RC5_BACKWARD:
						if (RC5.count % rcRepeatDelay == 0)
							return CMD_LEFT;
						break;

					case KEY_RC5_FORWARD:
						if (RC5.count % rcRepeatDelay == 0)
							return CMD_RIGHT;
						break;

					case KEY_RC5_PROGRAM:
						if (RC5.count == 0)
							return CMD_MENU;
						break;

					default:
						break;
				} // switch (RC5.id)
				break;

			case STATE_STANDBY:					// STANDBY mode
				if ((RC5.id == KEY_RC5_POWER) || (RC5.id == KEY_RC5_POWER_ON))
				{
					if (RC5.count == 0)
						return CMD_WAKE_UP;
				}
				break;

			case STATE_OVERCURRENT:				// OVERCURRENT mode
				if ((RC5.id == KEY_RC5_POWER) || (RC5.id == KEY_RC5_POWER_OFF))
				{
					if (RC5.count == 0)
						return CMD_STANDBY;
				}
				break;

			default:
				break;
		} // switch (volatileConfig.state)
	}
	else if (RC5.dev == CODE_SOULUTION_720)
	{
		switch (volatileConfig.state)
		{
			case STATE_EXTERNAL_INPUT:			// DAC mode
				switch (RC5.id)
				{
          case KEY_RC5_MUTE:
						if (RC5.count == 0)
              return CMD_MUTE;
            break;

					case KEY_RC5_VOL_UP:
						if (userConfig.volumeMode != OFF_CTRL)
							return CMD_VOL_UP;
						break;

					case KEY_RC5_VOL_DOWN:
						if (userConfig.volumeMode != OFF_CTRL)
							return CMD_VOL_DOWN;
						break;

					default:
						break;
				} // switch (RC5.id)
				break;

			default:
				break;
		}
	}

	return NO_CMD;
}

/*****************************************************************************
 * processEncoder
 *
 * Description	:	This function returns a command if the encoder is pressed
 *
 * Returns		:	Command code or NO_CMD, as defined in "cmd_mgr.h"
 *
 *****************************************************************************/
unsigned char processEncoderA(void)
{
	encoderStruct encoder;

	readEncoderKeyA(&encoder);
	switch(volatileConfig.state)
	{
		case STATE_EXTERNAL_INPUT:		// DAC mode
      switch (encoder.direction)
      {
        case CW:
          return CMD_SEL_INPUT_MINUS;
        case CCW:
          return CMD_SEL_INPUT_PLUS;
        default:
          return NO_CMD;
      }

		case STATE_SETUP:				// SETUP mode
			switch(encoder.direction)
			{
				case CW:
					return CMD_LEFT;
				case CCW:
					return CMD_RIGHT;
				default:
					return NO_CMD;
			} // switch(encodeur.direction)
			break;

		case STATE_STANDBY:				// STANDBY mode
		default:
			break;
	} // switch (volatileConfig.state)

	return NO_CMD;
}


unsigned char processEncoderB(void)
{
	encoderStruct encoder;

	readEncoderKeyB(&encoder);
	switch(volatileConfig.state)
	{
		case STATE_EXTERNAL_INPUT:		// DAC mode
				switch (encoder.direction)
				{
					case CW:
						return CMD_VOL_DOWN;
					case CCW:
						return CMD_VOL_UP;
					default:
						return NO_CMD;
				}

		case STATE_STANDBY:				// STANDBY mode
		default:
			break;

	} // switch (volatileConfig.state)

	return NO_CMD;
}


/*****************************************************************************
 * processRS232
 *
 * Description	:	This function returns a command when a message comes from
 *                RS232
 *
 * Returns		:	Command code or NO_CMD, as defined in "cmd_mgr.h"
 *
 *****************************************************************************/
unsigned char processRS232 (void)
{
	char message[50];
	unsigned char cmd;
	unsigned char done=0;
	unsigned char i=0;

	readRS232 ((char *)message);

	// Remove any character after the command number
	while(!done)
	{
		if ((message[i] < 0x30) || (message[i] > 0x39))
		{
			message[i] = 0;
			done = 1;
		}
		else
			i++;
	}

	if (i != 0)
	{
		cmd = (unsigned char)atoi(message);


		switch (cmd)
		{
			case RS232_GET_POWER:
				if (volatileConfig.state == STATE_STANDBY)
					sendRS232("PWR STANDBY");
				else
					sendRS232("PWR ON");
				break;

			case RS232_GET_STATE:
				break;

			case RS232_GET_INPUT:
				// SRC_EXTERNAL_INPUT
				{
					switch (userConfig.DigitalInput)
					{
						case DIGITAL_INPUT_SPDIF:
							sendRS232("SPDIF");
							break;

						case DIGITAL_INPUT_AES_EBU:
							sendRS232("AES/EBU");
							break;

						case DIGITAL_INPUT_TOSLINK:
							sendRS232("TOSLINK");
							break;

						case DIGITAL_INPUT_MEZZANINE_USB:
							sendRS232("USB");
							break;

						case DIGITAL_INPUT_MEZZANINE_NET:
							sendRS232("NETWORK");
							break;

						default:
							break;
					}
				}
				break;

			case RS232_GET_VERSION:
				sendRS232(SOFT_VERSION_STRING);
				break;

			case RS232_CMD_POWER_ON:
				if (volatileConfig.state == STATE_STANDBY)
					return CMD_WAKE_UP;
				break;

			case RS232_CMD_POWER_OFF:
				if (volatileConfig.state != STATE_STANDBY)
					return CMD_STANDBY;
				break;

			case RS232_CMD_SRC_AES:
					return CMD_SRC_AES;
				break;

			case RS232_CMD_SRC_SPDIF:
					return CMD_SRC_SPDIF;
				break;

			case RS232_CMD_SRC_OPT:
					return CMD_SRC_OPT;
				break;

			case RS232_CMD_SRC_USB:
					return CMD_SRC_USB;
				break;

			case RS232_CMD_SRC_NET:
					return CMD_SRC_NET;
				break;

			case RS232_CMD_SRC_UP:
					if (volatileConfig.state != STATE_CD_PLAYER)
					return CMD_SEL_INPUT_PLUS;
				break;

			case RS232_CMD_SRC_DOWN:
					if (volatileConfig.state != STATE_CD_PLAYER)
					return CMD_SEL_INPUT_MINUS;
				break;

			case RS232_CMD_VOL_UP:
					if (userConfig.volumeMode != OFF_CTRL)
					return CMD_VOL_UP;
				break;

			case RS232_CMD_VOL_DOWN:
					if (userConfig.volumeMode != OFF_CTRL)
					return CMD_VOL_DOWN;
				break;

			case RS232_CMD_BAL_LEFT:
					if (userConfig.volumeMode != OFF_CTRL)
					return CMD_BAL_LEFT;
				break;

			case RS232_CMD_BAL_RIGHT:
					if (userConfig.volumeMode != OFF_CTRL)
					return CMD_BAL_RIGHT;
				break;

			default:
				break;
		}
	}
	return NO_CMD;
}

/** E N D   O F   F I L E ****************************************************/
