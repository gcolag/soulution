
/*
 * File:         variables
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Global variables
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:
 *
 */

/*****************************************************************************/

#ifndef   __VARIABLES_H
#define   __VARIABLES_H

/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>


/** D E F I N E S ************************************************************/
// volatileConfig.displayRefresh
#define NO_REFRESH                        0x00
#define REFRESH                           0x01

// volatileConfig.playStatus
#define PLAY_IDLE                         0x00
#define PLAY_NOW                          0x01
#define PLAY_CMD_SENT                     0x02

#define NO_OVERCURRENT                    0x00
#define OVERCURRENT                       0x01
#define CHECK_OVERCURRENT                 0x02
#define OVERCURRENT_INHIBITED             0x03


/** T Y P E S ****************************************************************/
typedef enum ONorOFF {OFF, ON} T_ONorOFF;
typedef enum TE_VOLUME_CTRL_T {OFF_CTRL, STD_CTRL, LEEDH_CTRL} te_volume_ctrl_t;

// User configuration structure
typedef struct
{
   unsigned char    SourceMode;
   unsigned char   DigitalInput;
   T_ONorOFF      DigitalOutput;

   // Volume control
   // volume = 0(mute)..1(-79dB)..79(-1dB)..80(0dB)
   te_volume_ctrl_t      volumeMode;
   unsigned char   volume;
   unsigned char   volumeStart;   // (10..40)
   unsigned char   volumeMax;      // (20..80)

   // balance = 1(<- 9dB)..10(< C >)..19(9dB ->)
   unsigned char   balance;

   unsigned char   StartMode;
   unsigned char   ClockOutput;
   unsigned char   Brightness;
   unsigned int   versionSoft;

   unsigned char   phasePolarity;

   unsigned char UpSamplingFilters;
   unsigned char DcOffsetLeft;
   unsigned char DcOffsetRight;
   unsigned char PhaseEq;
} flashConfigStruct;


// Temporary configuration structure
typedef struct
{
   // General state
   volatile unsigned char   state;
   volatile unsigned char   prevState;
   volatile T_ONorOFF      setVolume;
   volatile unsigned char   flagVoltage;
   volatile unsigned char   flagOvercurrent;
   volatile unsigned int   timeout_overcurrent;
   volatile unsigned int   timeout_overcurrent_long;
   volatile unsigned char   trig_in_detected;
   volatile unsigned char   mute;

   // Menu navigation
   volatile unsigned char   nextMenu;
   volatile unsigned char    displayRefresh;

   // 1 : factory mode ; 0 : normal mode
   volatile unsigned char    factory_mode;

   // Other timeouts
   volatile unsigned int   timeout_volume;
   volatile unsigned int   timeout_usb_detect;
} volatileConfigStruct;


/** V A R I A B L E S ********************************************************/
// "Volatile" data which doesn't need to be stored in flash memory
extern volatile volatileConfigStruct volatileConfig;
extern volatile flashConfigStruct userConfig;


/** E N D   O F   F I L E ****************************************************/

#endif
