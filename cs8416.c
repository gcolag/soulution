
/*
 * File:         cs8416
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/18
 * Description:  CS8416 digital receiver drive
 *
 * History :
 *
 *   2015/04/18  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include <p24fxxxx.h>
#include <PPS.h>

#include "GenericTypeDefs.h"
#include "periph_spi.h"
#include "io.h"
#include "cs8416.h"


/*****************************************************************************/


// HAL
//


static UINT8 spi_read_write(UINT8 dataOut)
{
  return spi2_write(dataOut);
}


static void assert_cs(void)
{
  // HACK : Disable ChangeNotification interrupts.
  //
  // ChangeNotification interrupts will trig I2C com when
  // the MUTE signal toggles, potentially in conflict with the
  // I2C access below. Protect this section.
  //
  // Note : waiting for a better implementation (I2C ressource
  //        sharing handling)
  //
  io_disable_cn_int();

  DIR_nCS = 0;
}


static void deassert_cs(void)
{
  DIR_nCS = 1;

  // HACK : Enable ChangeNotification interrupts.
  //
  io_enable_cn_int();
}


/*****************************************************************************/


int cs8416_write(unsigned char reg, unsigned char data)
{
  unsigned char chip_addr = 0x20;

  assert_cs();
  spi_read_write(chip_addr);
  spi_read_write(reg);
  spi_read_write(data);
  deassert_cs();

  return 0;
}


int cs8416_read(unsigned char reg, unsigned char* data)
{
  unsigned char chip_addr = 0x20;

  assert_cs();
  spi_read_write(chip_addr);
  spi_read_write(reg);
  deassert_cs();

  assert_cs();
  spi_read_write(chip_addr | 0x01);
  *data = spi_read_write(0x00);
  deassert_cs();

  return 0;
}


/*****************************************************************************/


// Register map
//
#define CS8416_CTRL_0                    0x00
#define CS8416_CTRL_1                    0x01
#define CS8416_CTRL_2                    0x02
#define CS8416_CTRL_3                    0x03
#define CS8416_CTRL_4                    0x04
#define CS8416_SERIAL_AUDIO_DATA_FORMAT  0x05
#define CS8416_RECEIVER_ERROR_MASK       0x06
#define CS8416_INTERRUPT_MASK            0x07
#define CS8416_INTERRUPT_MODE_MSB        0x08
#define CS8416_INTERRUPT_MODE_LSB        0x09
#define CS8416_RECEIVER_CHANNEL_STATUS   0x0A
#define CS8416_AUDIO_FORMAT_DETECT       0x0B
#define CS8416_RECEIVER_ERROR            0x0C
#define CS8416_INTERRUPT_STATUS          0x0D
#define CS8416_ORR                       0x18
#define CS8416_ID_VERSION                0x7F


/*****************************************************************************/


int cs8416_init(void)
{
  unsigned char reg;
  unsigned char control4;
  unsigned char config = 
	  CS8416_CFG_AUTO_CLOCK_SWITCHING | 
    CS8416_CFG_RMCK_256FS | 
    CS8416_CFG_AUDIO_MASTER | 
    CS8416_CFG_AUDIO_I2S | 
    CS8416_CFG_INT_ACTIVE_LOW;

	// Try to detect device
	if (cs8416_read(CS8416_ID_VERSION, &reg))
		return -1;
	if ((reg & 0xF0) != 0x20)
    return -1;

	reg = 0;
	if (config & CS8416_CFG_AUTO_CLOCK_SWITCHING)
		reg |= BIT07; // SWCLK
	if (config & CS8416_CFG_RMCK_128FS)
		reg |= BIT01;	// RMCKF
	if (config & CS8416_CFG_INT_ACTIVE_LOW)
		reg |= BIT04;
	if (config & CS8416_CFG_INT_OPEN_DRAIN)
		reg |= BIT05;
	if (cs8416_write(CS8416_CTRL_1, reg))
		return -1;

	// GPO0: default to GND
	if (cs8416_write(CS8416_CTRL_2, 0))
		return -1;

#if 1
	// GPO1 & GPO2: default to GND
	if (cs8416_write(CS8416_CTRL_3, 0))
		return -1;
#else
	// GPO1 : NVERR
  // GPO2 : GND
	if (cs8416_write(CS8416_CTRL_3, 0x60))
		return -1;
#endif

	// default: device in running mode, select input 2 (RCA)
	control4 = BIT07 | (CS8416_INPUT_RXP2 << 3);
	if (cs8416_write(CS8416_CTRL_4, control4))
		return NULL;

	// audio output port
	reg = 0;
	if (config & CS8416_CFG_AUDIO_MASTER)
		reg |= BIT07;
	if (config & CS8416_CFG_AUDIO_I2S)
		reg |= BIT02 | BIT00;
	if (cs8416_write(CS8416_SERIAL_AUDIO_DATA_FORMAT, reg))
		return -1;
	
	// receiver error mask: unlock status
	if(cs8416_write(CS8416_RECEIVER_ERROR_MASK, BIT04))
		return -1;

	// Interrupt mask: none (can be configured using cs8416_setup_int_mask)
	if(cs8416_write(CS8416_INTERRUPT_MASK, 0))
		return -1;

	// Interrupt mode: level active
	if (cs8416_write(CS8416_INTERRUPT_MODE_MSB, 0x7F))
		return -1;
	if (cs8416_write(CS8416_INTERRUPT_MODE_LSB, 0))
		return -1;
	
  return 0;
}


/*****************************************************************************/


int cs8416_get_chip_revision(unsigned char * rev)
{
	if (rev == NULL)
		return -1;
			
	if (cs8416_read(CS8416_ID_VERSION, rev))
		return -1;

	*rev &= 0x0F;

	return 0;
}


/*****************************************************************************/


int cs8416_select_input(CS8416_INPUT input)
{
	unsigned char  control4;

  control4 = BIT07 | (input << 3);
 
	if (cs8416_write(CS8416_CTRL_4, control4))
		return -1;

	return 0;
}


/*****************************************************************************/


/** 
 * Returns digital receiver status
 * Note: bits 6..3 are mutually exclusive
 * bits 8..15: input frequency ratio, as a 2.6 format (2-bit integer + 6-bit fractional part)
 *             ratio is defined as InFs / 96k
 *             value is meaningful only after the PLL has reached lock
 * bit7: locked (1) / unlocked (0)
 * bit6: PCM
 * bit5: IEC61937
 * bit4: DTS_LD
 * bit3: DTS_CD
 */
int cs8416_get_status(unsigned short* status)
{
  unsigned char reg;
	unsigned short stat = 0;

	cs8416_read(CS8416_INTERRUPT_STATUS, &reg);

	// check lock status
	if (cs8416_read(CS8416_RECEIVER_ERROR, &reg))
		return -1;
	if (reg & BIT04)
	{
		// PLL out of lock
		*status = 0;
		return 0;
	}
	
	// Read format
	if (cs8416_read(CS8416_AUDIO_FORMAT_DETECT, &reg))
		return -1;
	stat = BIT07 | (reg & 0b01111010);
	
	// Read ORR value
	if (cs8416_read(CS8416_ORR, &reg))
		return -1;
	stat |= ((unsigned short) reg) << 8;

	*status = stat;

	return 0;
}


/*****************************************************************************/
