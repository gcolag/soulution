
/*
 * File:         cmd_mgr
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Project:      Soulution DAC760
 * Description:  Command manager
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef	__CMD_MGR
#define	__CMD_MGR

/** I M P O R T A N T ********************************************************/
// You must add the "USE_AND_OR" macro definition to your project build
// options under MPLAB C30 tab.
// Alternatively you can uncomment the line below.
/*****************************************************************************/
//#define USE_AND_OR


/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <stdlib.h>
#include <uart.h>

#include "variables.h"
#include "periph_timer.h"
#include "config_mgr.h"
#include "io.h"
#include "keypad.h"
#include "RC5.h"
#include "encoder.h"
#include "rs232.h"


/** D E F I N E S ************************************************************/
// List of commands
#define	NO_CMD								0x00
#define CMD_PLAY							0x01
#define CMD_PAUSE							0x02
#define CMD_STOP							0x03
#define CMD_PREVIOUS						0x04
#define CMD_NEXT							0x05
#define CMD_FAST_REV						0x06
#define CMD_FAST_FW							0x07
#define CMD_STOP_SEARCH						0x08

#define CMD_MENU							0x09
#define CMD_LEFT							0x0A
#define CMD_RIGHT							0x0B
#define CMD_ENTER							0x0C

#define CMD_WAKE_UP							0x10
#define CMD_STANDBY							0x11

#define CMD_OPEN_CLOSE						0x12
#define CMD_CLOSE_PLAY						0x13

#define CMD_VOL_CTL							0x14
#define CMD_VOL_DOWN						0x15
#define CMD_VOL_UP							0x16

#define CMD_SEL_INPUT_MINUS					0x17
#define CMD_SEL_INPUT_PLUS					0x18

#define CMD_BAL_LEFT						0x19
#define CMD_BAL_RIGHT						0x20

#define CMD_CD_MODE							0x21
#define CMD_DAC_MODE						0x22
#define CMD_SRC_AES							0x23
#define CMD_SRC_SPDIF						0x24
#define CMD_SRC_OPT							0x25
#define CMD_SRC_USB							0x26
#define CMD_REPEAT_MODE						0x27
#define CMD_TIME_MODE						0x28
#define CMD_SRC_NET							0x29

#define CMD_MUTE							0x2A


// State describers for keypad, remote and encoder controls
#define	STATE_SEARCH_IDLE					0x01
#define	STATE_SEARCH_1						0x02
#define	STATE_SEARCH_2						0x03
#define	STATE_SEARCH_3						0x04

#define	STATE_ENCODER_IDLE					0x05
#define	STATE_ENCODER_PUSHED				0x06

// Constants for repeated keys delays
#define	KEYPAD_REPEAT_DELAY_FAST			4
#define	KEYPAD_REPEAT_DELAY_MEDIUM			5
#define	KEYPAD_REPEAT_DELAY_LOW				6
#define	KEYPAD_REPEAT_DELAY					KEYPAD_REPEAT_DELAY_LOW

#define	RC_REPEAT_DELAY_FAST				1
#define	RC_REPEAT_DELAY_MEDIUM				3
#define	RC_REPEAT_DELAY_LOW					4
#define	RC_REPEAT_DELAY						RC_REPEAT_DELAY_MEDIUM


/** E X T E R N S ************************************************************/
extern unsigned char rcRepeatDelay;


/** D E C L A R A T I O N S **************************************************/
unsigned char getCmd(void);


/** E N D   O F   F I L E ****************************************************/

#endif
