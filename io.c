
/*
 * File:         io
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Inputs/Outputs configuration
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:
 *
 */

/** I N C L U D E S **********************************************************/


#include <p24fxxxx.h>
#include <PPS.h>


#include "io.h"
#include "dbio.h"


/** V A R I A B L E S ********************************************************/
unsigned int timeout_overcurrent_inhibited;


/** I N T E R R U P T S ******************************************************/

// INT1 ISR - DIR (WM8805) interrupt line
void __attribute__((interrupt, no_auto_psv)) _INT1Interrupt()
{
	IFS1bits.INT1IF = 0;
}


// INT3 ISR - Overcurrent detection (PCA9554 interrupt line)
void __attribute__((interrupt, no_auto_psv)) _INT3Interrupt()
{
	// Check if overcurrent test is inhibited
	if (volatileConfig.flagOvercurrent != OVERCURRENT_INHIBITED)
	{
		// Check overcurrent now
		volatileConfig.flagOvercurrent = CHECK_OVERCURRENT;
	}

	IFS3bits.INT3IF = 0;
}


void dacs_mute(void);
void dacs_unmute(void);


int g_cnif_cnt = 0;

// Input Change notification
void __attribute__((interrupt, no_auto_psv)) _CNInterrupt()
{
  // last_mute_event allow to detect a missed mute action.
  //
  // last_mute_event is used to store the last mute event.
  // As this interrupt handler uses SPI and I2C bus, they may be
  // concurent accees to these buses. To avoid this, the main_loop
  // disables this interrupt during any bus access.
  // Consequently, if two interrupt occure during this time
  // (mute followed by unmute), only the second one will be execute.
  //
  static int last_mute_event = -1;

  g_cnif_cnt++;
	IFS1bits.CNIF = 0;

  if (volatileConfig.mute == 0)
  {
    if (DSP_nINT == 0)
    {
      last_mute_event = 0;
      dacs_mute();
    }
    else
    {
      if (last_mute_event != 0)
        dacs_mute();

      last_mute_event = 1;

      if (dbio_get_usb_lan_n22M_24M())
        dbio_select_24M();
      else
        dbio_select_22M();

      dacs_unmute();
    }
  }
  else
  {
    if (DSP_nINT == 1)
    {
      if (dbio_get_usb_lan_n22M_24M())
        dbio_select_24M();
      else
        dbio_select_22M();
    }
  }
}


static void initIOChangeNotififactionInt(void)
{
  CNEN1 = 0x0002;
  ConfigIntCN(INT_ENABLE | INT_PRI_1);
}


void io_disable_cn_int(void)
{
  ConfigIntCN(INT_DISABLE);
}


void io_enable_cn_int(void)
{
  ConfigIntCN(INT_ENABLE | INT_PRI_1);
}

/** F U N C T I O N S ********************************************************/
/*****************************************************************************
 * configIO
 *
 * Description	:	This function configures the microcontroller I/O
 *
 *****************************************************************************/
void configIO(void)
{
	// Configure all IOs as digital.
	AD1PCFG = 0xffff;


  LATA  = 0b0000000000000000;
  TRISA = 0b1111111111111111;

  // B15 : FP_nRST          (Output) (Init val = 0)
  // B12 : STATUS_LED       (Output) (Init val = 0)
  // B3  : LINK_ON          (Output) (Init val = 0)
  // B2  : LINK_FEEDBACK    (Input)
  // B1  : PRE_MUTE         (Output) (Init val = 1)
  // B0  : TRIG_IN          (Input)  [pull-up]
  LATB  = 0b0000010000000010;
  TRISB = 0b0110101111110101;
  CNPU1bits.CN2PUE = 1;

  // C13 : DSP_nINT         (Input)
  // C14 : CPLD_nCS         (Output) (Init val = 1)
  LATC  = 0b0100000000000000;
  TRISC = 0b1011111111111111;

  // D14 : FP_nCS           (Output) (Init val = 1)
  // D13 : FLASH_nCS        (Output) (Init val = 1)
  // D12 : PSU_POWER_ON     (Output) (Init val = 1)
  // D11 : S8_nCS           (Output) (Init val = 1)
  // D7  : SW_1             (Input)
  // D5  : HDSP-250X nRD    (Output) (Init val = 1)
  // D4  : HDSP-250X nWR    (Output) (Init val = 1)
  // D1  : DAC_nINT         (Input)
  // D0  : DIR_nCS          (Output) (Init val = 1)
  LATD  = 0b0111100000110001;
  TRISD = 0b1000011111001110;

  // E7..0 : HDSP-250X Data bus (Output) (used write only)
  // E8    : USB_LAN_CPLD_nCS   (Output) (Init val = 1)
  LATE  = 0b0000000100000000;
  TRISE = 0b1111111000000000;

  // F1  : SW_5             (Input)
  // F0  : SW_2             (Input)
  LATF  = 0b0000000000000000;
  TRISF = 0b1111111111111111;

  // G1  : SW_4             (Input)
  // G0  : SW_3             (Input)
  // G9  : USB_LAN_NMR_nCS  (Output) (Init val = 1)
  LATG  = 0b0000001000000000;
  TRISG = 0b1111110111111111;

  PPSUnLock;

  // SPI 1 FP
  //
  iPPSInput( IN_FN_PPS_SDI1  , IN_PIN_PPS_RP17);     // SPI MISO
//  iPPSInput( IN_FN_PPS_SDI1  , IN_PIN_PPS_RPI40);     // SPI MISO
  iPPSInput( IN_FN_PPS_SCK1IN, IN_PIN_PPS_RP5);      // SPI SCK readback
  iPPSOutput(OUT_PIN_PPS_RP10, OUT_FN_PPS_SDO1);     // SPI MOSI
  iPPSOutput(OUT_PIN_PPS_RP5 , OUT_FN_PPS_SCK1OUT);  // SPI SCK

  // SPI 2 CPLD/S8/DIR
  //
/*
  iPPSInput( IN_FN_PPS_SDI2   , IN_PIN_PPS_RP15);    // SPI MISO
  iPPSInput( IN_FN_PPS_SCK2IN , IN_PIN_PPS_RP30);    // SPI SCK readback
  iPPSOutput(OUT_PIN_PPS_RP2, OUT_FN_PPS_SDO2);      // SPI MOSI
  iPPSOutput(OUT_PIN_PPS_RP30 , OUT_FN_PPS_SCK2OUT); // SPI SCK
*/
  // FIXME :  OUT_PIN_PPS_RP30 doesn't exist ??!?
  // -> swapped MISO & SCK
  iPPSInput( IN_FN_PPS_SDI2  , IN_PIN_PPS_RP30);     // SPI MISO
  iPPSInput( IN_FN_PPS_SCK2IN, IN_PIN_PPS_RP15);     // SPI SCK readback
  iPPSOutput(OUT_PIN_PPS_RP2  , OUT_FN_PPS_SDO2);    // SPI MOSI
  iPPSOutput(OUT_PIN_PPS_RP15, OUT_FN_PPS_SCK2OUT);  // SPI SCK

  // SPI 3 USB_LAN
  //
  iPPSInput( IN_FN_PPS_SDI3  , IN_PIN_PPS_RP8);      // SPI MISO
  iPPSInput( IN_FN_PPS_SCK3IN, IN_PIN_PPS_RP21);     // SPI SCK readback
  iPPSOutput(OUT_PIN_PPS_RP26, OUT_FN_PPS_SDO3);     // SPI MOSI
  iPPSOutput(OUT_PIN_PPS_RP21, OUT_FN_PPS_SCK3OUT);  // SPI SCK

  // IR
  //
  iPPSInput( IN_FN_PPS_IC5   , IN_PIN_PPS_RPI38);     // Input Capture 5

  //
  //
  iPPSInput( IN_FN_PPS_IC1   , IN_PIN_PPS_RP14);      // Rot A A
  iPPSInput( IN_FN_PPS_IC2   , IN_PIN_PPS_RPI40);     // Rot A B

  PPSLock;
}


void standbyIO (void)
{
  // Configure all pins as inputs except what is on
  // the Front Pannel board (ie flash, encoder, buttons, IR and display)


	// Configure all IOs as digital.
	AD1PCFG = 0xffff;


  // All inputs
  LATA  = 0b0000000000000000;
  TRISA = 0b1111111111111111;

  // B15 : FP_nRST          (Output) (Init val = 0)
  // B12 : STATUS_LED       (Output) (Init val = 0)
  // B3  : LINK_ON          (Output) (Init val = 0)
  // B2  : LINK_FEEDBACK    (Input)
  // B1  : PRE_MUTE         (Output) (Init val = 1)
  // B0  : TRIG_IN          (Input)  [pull-up]
  LATB  = 0b0000010000000010;
  TRISB = 0b0110101111110101;
  CNPU1bits.CN2PUE = 1;

  // All inputs
  LATC  = 0b0000000000000000;
  TRISC = 0b1111111111111111;

  // D14 : FP_nCS           (Output) (Init val = 1)
  // D13 : FLASH_nCS        (Output) (Init val = 1)
  // D12 : PSU_POWER_ON     (Output) (Init val = 0) (in standby)
  // D7  : SW_1             (Input)
  // D5  : HDSP-250X nRD    (Output) (Init val = 0) (in standby)
  // D4  : HDSP-250X nWR    (Output) (Init val = 0) (in standby)
  // D1  : DAC_nINT         (Input)
  LATD  = 0b0110000000000000;
  TRISD = 0b1000111111001111;

  // E7..0 : HDSP-250X Data bus (Output) (used write only)
  LATE  = 0b0000000000000000;
  TRISE = 0b1111111100000000;

  // F1  : SW_5             (Input)
  // F0  : SW_2             (Input)
  LATF  = 0b0000000000000000;
  TRISF = 0b1111111111111111;

  // G1  : SW_4             (Input)
  // G0  : SW_3             (Input)
  LATG  = 0b0000000000000000;
  TRISG = 0b1111111111111111;


  PPSUnLock;

  // SPI 2 CPLD/S8/DIR
  //
  iPPSOutput(OUT_PIN_PPS_RP2 , OUT_FN_PPS_NULL);    // SPI MOSI
  iPPSOutput(OUT_PIN_PPS_RP15, OUT_FN_PPS_NULL);    // SPI SCK

  // SPI 3 USB_LAN
  //
  iPPSOutput(OUT_PIN_PPS_RP26, OUT_FN_PPS_NULL);    // SPI MOSI
  iPPSOutput(OUT_PIN_PPS_RP21, OUT_FN_PPS_NULL);    // SPI SCK

  // IR
  //
  iPPSInput( IN_FN_PPS_IC5   , IN_PIN_PPS_RPI38);     // Input Capture 5

  // Rot A
  //
  iPPSInput( IN_FN_PPS_IC1   , IN_PIN_PPS_RP14);      // Rot A A
  iPPSInput( IN_FN_PPS_IC2   , IN_PIN_PPS_RPI40);     // Rot A B

  // Rot B
  //
  iPPSInput( IN_FN_PPS_IC3   , IN_PIN_PPS_RPI35);      // Rot A A
  iPPSInput( IN_FN_PPS_IC4   , IN_PIN_PPS_RPI36);     // Rot A B

  PPSLock;
}


void config_IO_interrupts (void)
{
  initIOChangeNotififactionInt();
}


void close_IO_interrupts (void)
{
}


/*****************************************************************************
 * test_voltage
 *
 * Description			: This function enables the PSU, tests if all the voltages
 *						  are correct, then disables the PSU.
 *
 * Returns			 0	: No error
 *					-1	: Power failure
 *
 *****************************************************************************/
char test_voltage (void)
{
#if 0
	char error = -1;
	unsigned int timeOutVoltage = readTimerCounter();

	while ( ((VOLTAGE_ERROR) || (OVERCURRENT_ERROR)) &&
          (readTimerDiff(timeOutVoltage) < TIME_OUT_VOLTAGE) );

	if (readTimerDiff(timeOutVoltage) < TIME_OUT_VOLTAGE)
		error = 0;

	return error;
#else
  return 0;
#endif
}

/*****************************************************************************
 * test_overcurrent
 *
 * Overview		   : This function tests if there is an overcurrent in the output
 *
 * Returns		 0 : No problem
 *				-1 : Overcurrent occured
 *
 *****************************************************************************/
char test_overcurrent(void)
{
#if 0
	char err = 0;
	unsigned char protection = 0;

	// read the overcurrent pin
	if (OVERCURRENT_ERROR)
		protection = 1;

	if (protection)
	{
		delay_ms(10);

		// read a second time to avoid false alarm
		if (OVERCURRENT_ERROR)
			protection = 1;
		else
			protection = 0;
	}

	if (protection)
		err = -1;				// overcurrent detected
	else
		err = 0;				// false detection - no error detected

	return err;
#else
  return 0;
#endif
}

/*****************************************************************************
 * getCmd
 *
 * Description	:	This function looks for any action on the keypad, the remote
 *					or the encoder - depending on the value of next2Scan - and returns
 *					the corresponding command.
 *
 * Returns		:	Command code or NO_CMD, as defined in "cmd_mgr.h"
 *
 *****************************************************************************/
unsigned char getTrigger(void)
{
	unsigned char cmd = NO_CMD;

	if (volatileConfig.state == STATE_STANDBY)
	{
		if (link_get_trig_in() == 0)
			cmd = CMD_WAKE_UP;
	}
	else
	{
		if (link_get_trig_in() == 0)
			volatileConfig.trig_in_detected = 1;

		if (volatileConfig.trig_in_detected)
		{
			if (link_get_trig_in() == 1)
				cmd = CMD_STANDBY;
		}
	}

	return cmd;
}


/*****************************************************************************
 * usbDetect
 *
 * Description	:	This function checks if a USB host is connected.
 *
 * Returns		:	0 -> no USB host detected
 *					1 -> USB host connected
 *
 *****************************************************************************/
unsigned char usbDetect(void)
{
  return GetUsbPower();
}


/*****************************************************************************/


void md_assert_cs(void)
{
  S8_nCS = 0;
}


void md_deassert_cs(void)
{
  S8_nCS = 1;
}


/*****************************************************************************/
