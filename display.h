
/*
 * File:         display
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Display management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef	__DISPLAY_H
#define	__DISPLAY_H

/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <string.h>

#include "variables.h"
#include "config_mgr.h"
#include "SCF5742.h"
#include "eeprom.h"
#include "digital_in.h"
#include "periph_timer.h"


/** D E C L A R A T I O N S **************************************************/
void displayVolumeMode(void);
void displayCDMode(void);
void displayDACMode(void);
void displaySelectTrack(unsigned char mode, unsigned char track);

void displayOvercurrent(void);
void putNbrLcd_U(unsigned int n,char pos, char * str);


/** E N D   O F   F I L E ****************************************************/
#endif
