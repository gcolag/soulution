

/*
 * File:         eeprom
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Internal EEPROM driver
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __EEPROM
#define __EEPROM

/** I N C L U D E S **********************************************************/
#include <p24fxxxx.h>
#include <spi.h>

#include "variables.h"
#include "periph_spi.h"


/** D E F I N E S ************************************************************/
// Chip select line.
#define CS_FLASH		LATAbits.LATA2

#define FLASH_SECTOR	0x00000

// Data types.
#define byte	unsigned char
#define word	unsigned int
#define dword	unsigned long

// Buffer is 69 bytes (64 data bytes + 5 overhead bytes) long.
#define	FLASH_BUFFER_SIZE	(69)
#define	FLASH_OVER_HEAD		(5)
#define	FLASH_DATA_SIZE		(FLASH_BUFFER_SIZE - FLASH_OVER_HEAD)

// Instructions.
#define	WREN		0b00000110	// Write enable.
#define	WRDI		0b00000100	// Write Disable.
#define	RDID		0b10011111	// Read Identification (M25P80 device only).
#define	RDSR		0b00000101	// Read Status Register.
#define	WRSR		0b00000001	// Write Status Register.
#define	STD_READ	0b00000011	// Read Data Bytes.
#define	FAST_READ	0b00001011	// Read Data Bytes at Higher Speed.
#define	PP			0b00000010	// Page Program.
#define	SE			0b11011000	// Sector Erase.
#define	BE			0b11000111	// Bulk Erase.
#define	DP			0b10111001	// Deep Power-down.
#define	RES			0b10101011	// Release from Deep Powerdown.


// SST25VF040
// 
#define	WRSR    0b00000001	// Write-Status-Register


// Device type
//
#define M25P40      0
#define SST25VF040  1


// -----------------------------------------------------------------------------
// M25P40: The memory is organized as 8 sectors, each containing 256 pages. 
// Each page is 256 bytes wide. Thus, the whole memory can be viewed as 
// consisting of 2048 pages, or 524'288 bytes.
// -----------------------------------------------------------------------------
// Sector's start address on M25P40
// SECTOR  7	0x070000	// Last sector
// SECTOR  6	0x060000
// SECTOR  5	0x050000
// SECTOR  4	0x040000
// SECTOR  3	0x030000
// SECTOR  2	0x020000
// SECTOR  1	0x010000
// SECTOR  0	0x000000

// M25P40 RDID data-out sequence = 0x20-0x20-0x14-0x10.
#define	M25P40_IDENT		0x00132020


/** S T R U C T U R E S ******************************************************/
typedef union
{
    byte _byte[FLASH_BUFFER_SIZE];
    struct
    {
        byte CMD;
        byte LEN;
        union
        {
            unsigned long addr; 
            struct
            {
                byte low; //Little-endian order
                byte high;
                byte upper;
            };
        }ADR;
        byte data[FLASH_DATA_SIZE];
    };
} T_FLASH_DATA_PACKET;

typedef union
{
	byte value;
	struct
	{
		unsigned char wip : 1;		// Write In Progress.
		unsigned char wel : 1;		// Write Enable Latch.
		unsigned char bpb : 3;		// BP2-BP0 : block protect bits. 
		unsigned char  : 1;			// Not used.
		unsigned char  : 1;			// Not used.
		unsigned char srwd : 1;		// Status Register Write Disable.
	};
} T_FlashStatus;


/** D E C L A R A T I O N S **************************************************/


void writeConfigToFlash(void);
void loadConfigFromFlash(void);
void flashReadID(byte* buffer);


/** E N D   O F   F I L E ****************************************************/
#endif
