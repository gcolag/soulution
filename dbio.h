
/*
 * File:         dbio
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 *
 * Created:      2015/04/16
 * Description:  Dsp Board I/O
 *
 * History :
 *
 *   2015/04/16  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __DBIO_H
#define	__DVIO_H

/*****************************************************************************/


int  dbio_standby(void);
int  dbio_init(void);

void dbio_assert_main_mute(void);
void dbio_deassert_main_mute(void);

void dbio_assert_usb_lan_reset();
void dbio_deassert_usb_lan_reset();

void dbio_select_nmr();
void dbio_select_usb();

int  dbio_get_usb_bus_detect(void);
int  dbio_get_osc_22M_en(void);
int  dbio_get_osc_24M_en(void);

#if 0
void dbio_assert_osc_22M_en(void);
void dbio_deassert_osc_22M_en(void);
void dbio_assert_osc_24M_en(void);
void dbio_deassert_osc_24M_en(void);
#endif


/*****************************************************************************/


void dbio_select_22M(void);
void dbio_select_24M(void);

void dbio_select_usblan(void);
void dbio_select_dir(void);

void dbio_assert_dir_reset(void);
void dbio_deassert_dir_reset(void);

void dbio_assert_s8_reset(void);
void dbio_deassert_s8_reset(void);

void dbio_assert_dit_reset(void);
void dbio_deassert_dit_reset(void);

void dbio_s8_force_invalid_mode(void);
void dbio_s8_disable_forced_invalid_mode(void);

int  dbio_get_s8_nirq(void);

int dbio_get_usb_lan_n22M_24M(void);

void dbio_assert_mute_ack(void);
void dbio_deassert_mute_ack(void);


/*****************************************************************************/

#endif	/* __DBIO_H */
