
/*
 * File:         mcp23s17
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/09
 * Description:  MCP23S17 I/O expander driver
 *
 * History :
 *
 *   2015/04/09  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef _MCP23S17_H_
#define _MCP23S17_H_

/*****************************************************************************/

#include "GenericTypeDefs.h"

/*****************************************************************************/


int  mcp23s17_init(void);

void mcp23s17_gpio_set(UINT16 pin_mask);
void mcp23s17_gpio_clr(UINT16 pin_mask);
void mcp23s17_gpio_clr_shadow(UINT16 pin_mask);


void mcp23s17_tris_set(UINT16 pin_mask);
void mcp23s17_tris_clr(UINT16 pin_mask);

unsigned int mcp23s17_gpio_get( void );


/*****************************************************************************/

#endif // _MCP23S17_H_



