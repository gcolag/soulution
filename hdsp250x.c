
/*
 * File:         hdsp250x
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  HDSP 250X generic driver
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include <p24fxxxx.h>
#include <PPS.h>

#include "GenericTypeDefs.h"
#include "mcp23s17.h"
#include "hdsp250x.h"


/*****************************************************************************/


// HAL
//


#define U4_CS_MASK  0x0800
#define U3_CS_MASK  0x0400
#define U2_CS_MASK  0x0200
#define U1_CS_MASK  0x0100
#define RST_MASK    0x0080
#define FL_MASK     0x0001

#define ADDR_MASK   0x003E
#define ADDR_SHIFT  1

#define HDSP250X_DATA    LATE

#define HDSP250X_nRD     LATDbits.LATD5
#define HDSP250X_nWR     LATDbits.LATD4


static void assert_cs(hdsp250x_dev_type* dev)
{
  UINT16 cs_mask[] = { U4_CS_MASK, U3_CS_MASK, U2_CS_MASK, U1_CS_MASK };

  if (dev->dev_index > 3)
    return;

  mcp23s17_gpio_clr(cs_mask[dev->dev_index]);
}


static void deassert_cs(hdsp250x_dev_type* dev)
{
  UINT16 cs_mask[] = { U4_CS_MASK, U3_CS_MASK, U2_CS_MASK, U1_CS_MASK };

  if (dev->dev_index > 3)
    return;

  mcp23s17_gpio_set(cs_mask[dev->dev_index]);
}


static void assert_rst(void)
{
  mcp23s17_gpio_clr(RST_MASK);
}


static void deassert_rst(void)
{
  mcp23s17_gpio_set(RST_MASK);
}


#if 0 // Unused
static void assert_fl(void)
{
  mcp23s17_gpio_clr(FL_MASK);
}


static void deassert_fl(void)
{
  mcp23s17_gpio_set(FL_MASK);
}
#endif


static void load_addr(UINT8 addr)
{
  UINT16 mcp23s17_addr;

  mcp23s17_addr = (addr << ADDR_SHIFT) & ADDR_MASK;
  mcp23s17_gpio_clr_shadow(ADDR_MASK);
  mcp23s17_gpio_set(mcp23s17_addr);
}


static void load_data(UINT8 data)
{
  HDSP250X_DATA = data;
}


#if 0 // Unused
static void assert_rd(void)
{
  HDSP250X_nRD = 0;
}


static void deassert_rd(void)
{
  HDSP250X_nRD = 1;
}
#endif


static void assert_wr(void)
{
  HDSP250X_nWR = 0;
}


static void deassert_wr(void)
{
  HDSP250X_nWR = 1;
}


/*****************************************************************************/


#define CHAR_ADDR         0x18
#define CTRLW_ADDR        0x10
#define UDC_RAM_ADDR      0x08
#define UDC_ADDRREG_ADDR  0x00

#define BRIGHTNESS_MASK   0x07


/*****************************************************************************/


void hdsp250x_standby(void)
{
  mcp23s17_tris_clr(0xFFFF);
  mcp23s17_gpio_clr(0xFFFF);

  HDSP250X_DATA = 0x00;

  HDSP250X_nRD = 0;
  HDSP250X_nWR = 0;
}


void hdsp250x_init(void)
{
  // All outputs
  mcp23s17_tris_clr(0xFFFF);

  // Disable all chip selects
  // leds on
  // nFL = 1
  // ADDR = 0x00
  mcp23s17_gpio_set(0xFF01);

  // reset strobe
  assert_rst();
  deassert_rst();
}


void hdsp250x_test(void)
{
  int pos = 0;
  UINT8 addr;
  UINT8 data = 'A';
  hdsp250x_dev_type dev_var, *dev = &dev_var;

  dev->dev_index = 0;

  addr = CHAR_ADDR | pos; 
  load_addr(addr);
  load_data(data);
  assert_wr();
  assert_cs(dev);
  deassert_cs(dev);
  deassert_wr();
}


void hdsp250x_write_char(hdsp250x_dev_type* dev, UINT8 pos, char data)
{
  UINT8 addr;

  if (dev->dev_index > 8)
    return;

  // remap '^' on first user define char where the "degree" symbol is
  if (data == 94)
    data = 128;

  addr = CHAR_ADDR | pos; 
  load_addr(addr);
  load_data(data);
  assert_wr();
  assert_cs(dev);
  deassert_cs(dev);
  deassert_wr();
}


void hdsp250x_set_brightness(hdsp250x_dev_type* dev, UINT8 brightness)
{
  UINT8 addr;
  UINT8 data;

  if (dev->dev_index > 8)
    return;

  data = brightness & BRIGHTNESS_MASK;
  addr = CTRLW_ADDR;

  load_addr(addr);
  load_data(data);
  assert_wr();
  assert_cs(dev);
  deassert_cs(dev);
  deassert_wr();
}


/*****************************************************************************/


// Init User Define Charset.
// We only need to create the "degree" symbol.
// 


void hdsp250x_init_udc(hdsp250x_dev_type* dev)
{
  UINT8 udc_addr;
  UINT8 addr;
  int i;
  unsigned char degree_char_def[7] = {
   0x06, 0x09, 0x09, 0x06, 0x00, 0x00, 0x00,	// 0x5E (^) °
  };

  // Load UDC address reg with 0x00
  //
  udc_addr = 0;
  addr = UDC_ADDRREG_ADDR;
  load_addr(addr);
  load_data(udc_addr);
  assert_wr();
  assert_cs(dev);
  deassert_cs(dev);
  deassert_wr();

  // Load seven lines of char definition
  //
  for (i=0 ; i<7 ; i++)
  {
    addr = UDC_RAM_ADDR | i;
    load_addr(addr);
    load_data(degree_char_def[i]);
    assert_wr();
    assert_cs(dev);
    deassert_cs(dev);
    deassert_wr();
  }
}


/*****************************************************************************/
