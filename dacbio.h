
/*
 * File:         dacbio
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/19
 * Description:  DAC Board I/O
 *
 * History :
 *
 *   2015/04/19  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __DACBIO_H
#define	__DACVIO_H

/*****************************************************************************/


int  dacbio_init(void);

int  dacbio_get_int(void);


/*****************************************************************************/


int dacbio_left_detect(void);
int dacbio_right_detect(void);


/*****************************************************************************/


void dacbio_left_assert_rst(void);
void dacbio_left_deassert_rst(void);

void dacbio_left_assert_mcu_unmute(void);
void dacbio_left_deassert_mcu_unmute(void);

void dacbio_left_assert_mcu_mute(void);
void dacbio_left_deassert_mcu_mute(void);

int  dacbio_left_get_unmute_readback(void);


/*****************************************************************************/


void dacbio_right_assert_rst(void);
void dacbio_rightt_deassert_rst(void);

void dacbio_right_assert_mcu_unmute(void);
void dacbio_right_deassert_mcu_unmute(void);

void dacbio_right_assert_mcu_mute(void);
void dacbio_right_deassert_mcu_mute(void);

int  dacbio_right_get_unmute_readback(void);


/*****************************************************************************/


void dacbio_assert_rst(void);
void dacbio_deassert_rst(void);

void dacbio_assert_mcu_unmute(void);
void dacbio_deassert_mcu_unmute(void);

void dacbio_assert_mcu_mute(void);
void dacbio_deassert_mcu_mute(void);


/*****************************************************************************/


#define dacbio_assert_main_mute   dbio_assert_main_mute
#define dacbio_deassert_main_mute dbio_deassert_main_mute


/*****************************************************************************/

#endif	/* __DACBIO_H */
