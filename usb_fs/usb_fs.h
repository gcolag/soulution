
/*
 * File:         usb_fs
 * Based on:     
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 *
 * Created:      2015/04/09
 * Description:  S8 EVAL USB FS
 *
 * History :
 *
 *   2015/04/09  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __USB_FS_H
#define __USB_FS_H

/*****************************************************************************/


#include "stdtypes.h"


/*****************************************************************************/


char DetectFileForModuleDspFirmwareUpdate(char* firmware_filename);


/*****************************************************************************/

#endif // __USB_FS_H

