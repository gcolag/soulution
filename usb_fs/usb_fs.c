
/*
 * File:         usb_fs
 * Based on:     
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 *
 * Created:      2015/07/03
 * Description:  S8 EVAL USB FS
 *
 * History :
 *
 *   2015/07/03  1.0  Initial release
 *
 */

/****************************************************************************/


#include <p24fxxxx.h>
#include <PPS.h>
#include <i2c.h>
#include <uart.h>

#include "Compiler.h"
#include "GenericTypeDefs.h"
#include "HardwareProfile.h"

#include "MDD File System/FSIO.h"
#include "USB/usb.h"
#include "USB/usb_host_msd.h"
#include "USB/usb_host_msd_scsi.h"

#include "stdtypes.h"
#include "io.h"
#include "periph_timer.h"
#include "periph_spi.h"
//#include "periph_uart.h"
//#include "s8.h"
//#include "md_firmware_update.h"
//#include "dsd1792.h"
#include "log.h"


/****************************************************************************/


#define BLMedia_MediumAttached()            USBHostMSDSCSIMediaDetect()
#define BLMedia_InitializeTransport()       USBInitialize(0)


#ifndef MAX_LOCATE_RETRYS
  #define MAX_LOCATE_RETRYS            3
#endif


#ifndef MAX_NUM_MOUNT_RETRIES
  #define MAX_NUM_MOUNT_RETRIES       10
#endif

// Flag indicating media attached
BOOL            MediaPresent;


BOOL USB_ThumbDriveEventHandler(BYTE address, 
                                USB_EVENT event, 
                                void *data, DWORD size)
{
//  log_msg_fmt("[%s]\n", __FUNCTION__);

  switch( event )
  {        
  case EVENT_VBUS_REQUEST_POWER:
    // The data pointer points to a byte that represents the amount of power
    // requested in mA, divided by two.  If the device wants too much power,
    // we reject it.
    if (((USB_VBUS_POWER_EVENT_DATA*)data)->current <= 
        (MAX_ALLOWED_CURRENT / 2))
    {
      return TRUE;
    }
    else
    {
      MediaPresent = FALSE;
      log_msg_fmt("USB Err 100\n");
    }
    break;
    
  case EVENT_VBUS_RELEASE_POWER:
    // The Explorer 16 cannot turn off Vbus through software.
    log_msg_fmt("USB Err 101\n");
    return TRUE;
    
  case EVENT_HUB_ATTACH:
    log_msg_fmt("USB Err 102\n");
    return TRUE;
    
  case EVENT_UNSUPPORTED_DEVICE:
    log_msg_fmt("USB Err 103\n");
    return TRUE;

  case EVENT_CANNOT_ENUMERATE:
    log_msg_fmt("USB Err 104\n");
    return TRUE;

  case EVENT_CLIENT_INIT_ERROR:
    log_msg_fmt("USB Err 105\n");
    return TRUE;

  case EVENT_OUT_OF_MEMORY:
    log_msg_fmt("USB Err 106\n");
    return TRUE;

  case EVENT_UNSPECIFIED_ERROR:   // This should never be generated.
    log_msg_fmt("USB Err 107\n");
    return TRUE;
  default:
    break;
  }

  return FALSE;

} // USB_ThumbDriveEventHandler


BOOL BLMedia_MonitorMedia ( void )
{
    BYTE            mediaPresentNow;
    BYTE            mountTries;

    USBTasks();

    mediaPresentNow = USBHostMSDSCSIMediaDetect();
    if (mediaPresentNow != MediaPresent)
    {
        if (mediaPresentNow)
        {
            mountTries = MAX_NUM_MOUNT_RETRIES;
            while(!FSInit() && mountTries--);
            if (!mountTries)
            {
              log_msg_fmt("[%s] USB init err\n", __FUNCTION__);
              MediaPresent = FALSE;
            }
            else
            {
                MediaPresent = TRUE;
            }
        }
        else
        {
            MediaPresent    = FALSE;
        }
    }

    return TRUE;

} // MonitorMedia


BOOL BLMedia_LocateFile(char *file_name)
{
  //BOOL        fileFound;
  SearchRec   searchRecord;

  if (FindFirst(file_name, 
                ATTR_DIRECTORY | ATTR_ARCHIVE | ATTR_READ_ONLY | ATTR_HIDDEN, 
                &searchRecord) == 0)
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
} // BLMedia_LocateFile


char DetectFileForModuleDspFirmwareUpdate(char* firmware_filename)
{
  static BOOL    LoadingApplication      = TRUE;
  static BOOL    TransportInitialized    = FALSE;
  static BOOL    BootMediumAttached      = FALSE;
  static BOOL    BootMediumAttached_old      = FALSE;
  static BOOL    BootImageFileFound      = FALSE;
  static BOOL    LookedForFile = FALSE;
  static BOOL    NotifyRemoveMedium      = TRUE;
  int     ErrCount                = 0;

  // used to abort media present after n iterations
	unsigned long	media_load_count = 0;	


  LookedForFile = FALSE;

  log_msg_fmt("Detect firm\n");

  // Loader main loop
  while (LoadingApplication)
  {
    if(media_load_count>ABC_MEDIA_LOAD_MAX)
    {
      return -1;
    }
	    
    if (TransportInitialized)
    {
      // Keep the boot medium alive
      TransportInitialized = BLMedia_MonitorMedia();
            
      // Check for the boot medium to attach
      BootMediumAttached_old = BootMediumAttached;
      BootMediumAttached = BLMedia_MediumAttached();

      if (BootMediumAttached_old != BootMediumAttached)
      {
        if (BootMediumAttached)
          log_msg_fmt("Medium attached\n");
        else
          log_msg_fmt("No medium\n");
      } 
      
      if (BootMediumAttached)
      {
        if (!LookedForFile)
        {
          // Attempt to locate the boot image file
          BootImageFileFound = BLMedia_LocateFile(firmware_filename);
          if (BootImageFileFound)
          {
            LookedForFile = TRUE;
            log_msg_fmt("Image found\n");
            return 1;
          }
          else
          {
            // Count and, if necessary, report the errors locating the file
            ErrCount++;
            if (ErrCount > MAX_LOCATE_RETRYS)
            {
              ErrCount = 0;
              LookedForFile = TRUE;

              log_msg_fmt("No image found\n");

              return -2;
            }
          }
        }
        else
        {
          // Medium must be removed once the firmware has been flashed
          if (NotifyRemoveMedium)
          {
            log_msg_fmt("Remove medium\n");
            NotifyRemoveMedium = FALSE;
          }
          return -4;
        }
      }
      else
      {
        LookedForFile = FALSE;
        NotifyRemoveMedium = TRUE;
        media_load_count++;
      }
    }
    else
    {
      // Initialize transport layer used to access the boot image's file system
      TransportInitialized = BLMedia_InitializeTransport();
      if (TransportInitialized)
      {
        log_msg_fmt("Transport initialised\n");
      }
    }
    
  }
	return 0;
}


/****************************************************************************/
