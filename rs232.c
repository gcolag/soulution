
/*
 * File:         rs232
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  RS232 control management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include "rs232.h"


static unsigned char rx_buffer[RS232_RX_BUFFER_SIZE];
static unsigned char rx_index;
unsigned int rs232_timeout;
unsigned char rs232_state;
unsigned char rs232_byte_arrived;
unsigned char rs232_data_arrived;


/** I N T E R R U P T S ******************************************************/
// UART 2 ISR
void __attribute__((interrupt, no_auto_psv)) _U2RXInterrupt()
{
	Nop();
	if (!rs232_byte_arrived)
	{
		RS232_DISABLE_RX;					// Host stops sending bytes
		rs232_state = RS232_RECEIVING;
		rs232_byte_arrived = 1;
	}
	IFS1bits.U2RXIF = 0;
}


/** F U N C T I O N S ********************************************************/
void initRS232 (void)
{
	RS232_ENABLE_RX;						// Host is allowed to send bytes
	
	OpenUART2 (	UART_EN |
				UART_MODE_FLOW |
				UART_UEN_00 |
				UART_DIS_WAKE |
				UART_BRGH_SIXTEEN |
				UART_NO_PAR_8BIT |
				UART_1STOPBIT,
				UART_TX_ENABLE |
				UART_INT_RX_CHAR |
				UART_ADR_DETECT_DIS |
				UART_RX_OVERRUN_CLEAR,38);
	
	ConfigIntUART2(UART_RX_INT_EN | UART_RX_INT_PR6 | UART_TX_INT_DIS);				// No interrupt
	
	rs232_state = RS232_IDLE;
	rx_buffer[0] = 0;
	rx_index = 0;
	rs232_byte_arrived = 0;
	rs232_data_arrived = 0;
}


void stopRS232 (void)
{
	CloseUART2();
	RS232_ENABLE_RX;
}


void sendRS232 (char *Text)
{
	unsigned char i;
	unsigned int timeRef = readTimerCounter();
	
	RS232_DISABLE_RX;							// Host stops sending bytes
	
	if (volatileConfig.state == STATE_STANDBY)
		RS232_ENABLE_TX;
	
	while ((RS232_CTS != 0) && (readTimerDiff(timeRef) > RS232_TX_TIMEOUT));	// Wait for CTS# DOWN w- timeout
	
	if (RS232_CTS == 0)							// Other device ready to accept transmission
	{
		while(BusyUART2());
		
		for (i=0;*(Text+i);i++)
		{
			WriteUART2(*(Text+i));				// Send character
			while(BusyUART2());
		}
		
		// DEBUG -> est-ce qu'on garde <CR>?
		WriteUART2(0x0D);						// Send <CR> '\r'
		WriteUART2(0x0A);						// Send <LF> '\n'
		while(BusyUART2());
	}
	
	if (volatileConfig.state == STATE_STANDBY)
		RS232_DISABLE_TX;
	
	RS232_ENABLE_RX;							// Host is allowed to send bytes
}


void receiveRS232 (unsigned char mode)
{
	if (mode == 0)	// Receiving
	{
		rx_buffer[rx_index++] = ReadUART2();
		rx_buffer[rx_index] = 0;			// NULL terminate the string after each byte received
		
		rs232_timeout = readTimerCounter();
	}
	else			// Timeout has elapsed
	{
		// Check if there was an error
		if (U2STAbits.OERR)
		{
			Nop();
			U2STAbits.OERR = 0;				// Error! At least 1 byte was missed...
			rx_index = 0;					// Reset reception
			rx_buffer[0] = 0;				// RX buffer is corrupted
		}
		else
		{
			Nop();
			rs232_data_arrived = 1;			// Data correctly received
		}
		
		rs232_state = RS232_IDLE;
	}
	
	if (!DataRdyUART2())					// UART2 internal buffer is empty
		RS232_ENABLE_RX;					// Host is allowed to send bytes
}


void readRS232 (char * str)
{
	strcpy (str, (char *)rx_buffer);
	sendRS232((char *)rx_buffer);
	rx_index = 0;					// Reset reception
	rx_buffer[0] = 0;				// RX buffer is empty
}
