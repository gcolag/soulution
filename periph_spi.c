
/*
 * File:         periph_spi
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  SPI peripheral custom library
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include "periph_spi.h"


/** F U N C T I O N S ********************************************************/
// Send one byte of data and receive one back at the same time.
unsigned char mWriteSPI1( unsigned char i )
{
    SPI1BUF = i;					// write to buffer for TX
	while(!SPI1STATbits.SPIRBF);
    return SPI1BUF;					// read the received value
}

// Send one dummy byte of data and receive one back at the same time.
unsigned char mReadSPI1( void )
{
    SPI1BUF = 0x00;					// write dummy byte to buffer for TX
    while(!SPI1STATbits.SPIRBF);	// wait for transfer to complete
    return SPI1BUF;					// read the received value
}

void mOpenSPI1(unsigned int config1, unsigned int config2, unsigned int stat)
{
    SPI1CON1 = config1;
	SPI1CON2 = config2;
    SPI1STAT = stat;
}

void mCloseSPI1(void)
{
	IEC0bits.SPI1IE = 0;	// Disable Interrupt.
	SPI1STATbits.SPIEN = 0;	// Disable peripheral.
	IFS0bits.SPI1IF = 0;	// Clear Interrupt flag.
}

void mConfigIntSPI1(unsigned int config)
{
	ConfigIntSPI1(config);
}

// Send one byte of data and receive one back at the same time.
unsigned char mWriteSPI2( unsigned char i )
{
    SPI2BUF = i;					// write to buffer for TX
    while(!SPI2STATbits.SPIRBF);	// wait for transfer to complete
    return SPI2BUF;    				// read the received value
}

// Send one dummy byte of data and receive one back at the same time.
unsigned char mReadSPI2( void )
{
    SPI2BUF = 0x00;					// write dummy byte to buffer for TX
    while(!SPI2STATbits.SPIRBF);	// wait for transfer to complete
    return SPI2BUF;    				// read the received value
}

void mOpenSPI2(unsigned int config1, unsigned int config2, unsigned int stat)
{
    SPI2CON1 = config1;
	SPI2CON2 = config2;
    SPI2STAT = stat;
}

void mCloseSPI2(void)
{
	IEC2bits.SPI2IE = 0;	// Disable Interrupt.
	SPI2STATbits.SPIEN = 0;	// Disable peripheral.
	IFS2bits.SPI2IF = 0;	// Clear Interrupt flag.
}

void mConfigIntSPI2(unsigned int config)
{
	ConfigIntSPI2(config);
}


/*****************************************************************************/


#define SPI_PRI_PRESCALER	0x00	// primary prescaler 64:1
#define SPI_SEC_PRESCALER	0x00	// secondary prescaler 8:1


/*****************************************************************************/


void spi1_open( UINT8 mode )
{
	switch( mode )
	{
		case MODE0 :
			SPI1CON1bits.CKP = 0;
			SPI1CON1bits.CKE = 1;
			break;
		case MODE1 :
			SPI1CON1bits.CKP = 0;
			SPI1CON1bits.CKE = 0;
			break;			
		case MODE2 :
			SPI1CON1bits.CKP = 1;
			SPI1CON1bits.CKE = 1;
			break;	
		case MODE3 :
			SPI1CON1bits.CKP = 1;
			SPI1CON1bits.CKE = 0;
			break;
	}		
	
	SPI1CON1bits.MSTEN = 1;
	SPI1CON1bits.MODE16 = 0;
	SPI1CON1bits.SPRE = SPI_SEC_PRESCALER;
	SPI1CON1bits.PPRE = SPI_PRI_PRESCALER;
	
	SPI1CON1bits.SMP = 0; // ?
	
	SPI1STATbits.SPIEN = 1;	
}	


UINT8 spi1_write( UINT8 data )
{
	SPI1BUF = data & 0xff;

  /* return the RBF bit status */ /* while buffer not ready to read */
	while( SPI1STATbits.SPIRBF == 0 ); 

	return (SPI1BUF & 0xff);
}


void spi1_close( void )
{		
  /* Disable the module. All pins controlled  by PORT Functions */

	SPI1STATbits.SPIEN = 0;   
}	


/*****************************************************************************/


void spi2_open( UINT8 mode )
{
	switch( mode )
	{
		case MODE0 :
			SPI2CON1bits.CKP = 0;
			SPI2CON1bits.CKE = 1;
			break;
		case MODE1 :
			SPI2CON1bits.CKP = 0;
			SPI2CON1bits.CKE = 0;
			break;			
		case MODE2 :
			SPI2CON1bits.CKP = 1;
			SPI2CON1bits.CKE = 1;
			break;	
		case MODE3 :
			SPI2CON1bits.CKP = 1;
			SPI2CON1bits.CKE = 0;
			break;
	}		
	
	SPI2CON1bits.MSTEN = 1;
	SPI2CON1bits.MODE16 = 0;
	SPI2CON1bits.SPRE = SPI_SEC_PRESCALER;
	SPI2CON1bits.PPRE = SPI_PRI_PRESCALER;
	
	SPI2CON1bits.SMP = 0; // ?
	
	SPI2STATbits.SPIEN = 1;	
}	


UINT8 spi2_write( UINT8 data )
{
	SPI2BUF = data & 0xff;

  /* return the RBF bit status */ /* while buffer not ready to read */
	while( SPI2STATbits.SPIRBF == 0 ); 

	return (SPI2BUF & 0xff);
}


void spi2_close( void )
{		
  /* Disable the module. All pins controlled  by PORT Functions */

	SPI2STATbits.SPIEN = 0;   
}	


/*****************************************************************************/


void spi3_open( UINT8 mode )
{
	switch( mode )
	{
		case MODE0 :
			SPI3CON1bits.CKP = 0;
			SPI3CON1bits.CKE = 1;
			break;
		case MODE1 :
			SPI3CON1bits.CKP = 0;
			SPI3CON1bits.CKE = 0;
			break;			
		case MODE2 :
			SPI3CON1bits.CKP = 1;
			SPI3CON1bits.CKE = 1;
			break;	
		case MODE3 :
			SPI3CON1bits.CKP = 1;
			SPI3CON1bits.CKE = 0;
			break;
	}		
	
	SPI3CON1bits.MSTEN = 1;
	SPI3CON1bits.MODE16 = 0;
	SPI3CON1bits.SPRE = SPI_SEC_PRESCALER;
	SPI3CON1bits.PPRE = SPI_PRI_PRESCALER;
	
	SPI3CON1bits.SMP = 0; // ?
	
	SPI3STATbits.SPIEN = 1;	
}	


UINT8 spi3_write( UINT8 data )
{
	SPI3BUF = data & 0xff;

  /* return the RBF bit status */ /* while buffer not ready to read */
	while( SPI3STATbits.SPIRBF == 0 ); 

	return (SPI3BUF & 0xff);
}


void spi3_close( void )
{		
  /* Disable the module. All pins controlled  by PORT Functions */

	SPI3STATbits.SPIEN = 0;   
}	


/*****************************************************************************/
