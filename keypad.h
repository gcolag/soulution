
/*
 * File:         keypad
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Keypad management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef __KEYPADS_H
#define __KEYPADS_H

/** I N C L U D E S **********************************************************/


#include <p24fxxxx.h>

#include "variables.h"
#include "periph_timer.h"
#include "io.h"
#include "encoder.h"

#include "keys.h"						// For keys definition
#include "io.h"						// For keys definition


/*****************************************************************************/


// HAL
//

#define keypad_get_key1  get_key4
#define keypad_get_key2  get_key5
#define keypad_get_key3  get_key3


/** D E F I N E S ************************************************************/


// Constants for timeouts
#define	KEY_REPEAT_DELAY_SLOW			500
#define	KEY_REPEAT_DELAY_MEDIUM			400
#define	KEY_REPEAT_DELAY_FAST			300
#define	KEY_REPEAT_DELAY				KEY_REPEAT_DELAY_MEDIUM

// State machine
#define WAIT_FOR_PRESS					0x00				// Waiting for Button press
#define	REPEAT_PRESS					0x01				// Press Button detected, waiting for repeat
#define	NO_REPEAT_PRESS					0x02				// Press Button detected



/** V A R I A B L E S ********************************************************/


/*****************************************************************************
 * Repeat mode
 * 
 * mskRepeatKey is a mask where each bit defines the repeat mode of a key:
 * Set a value of 0 if the repeat delay must be used.
 * Set a value of 1 if the key.count must count every detection.
 * 
 * Bits signification :	1  1  1  1  K3 K2 K1 EP
 *
 * With	:	K3 = POWER
 * 			K2 = PROGRAM
 * 			K1 = OPEN
 * 			EP = Encoder push
 *
 *****************************************************************************/
extern volatile unsigned char 		mskRepeatKey;


/** D E C L A R A T I O N S **************************************************/


void initKeypad(void);
void readKeypad(keyStruct * keypad);


/** E N D   O F   F I L E ****************************************************/
#endif
