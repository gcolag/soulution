
/*
 * File:         hal
 * Based on:     
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 *
 * Created:      2015/07/03
 * Description:  Hadrware Abstraction Layer
 *
 * History :
 *
 *   2015/07/03  1.0  Initial release
 *
 */

/****************************************************************************/


#include "stdtypes.h"
#include "periph_spi.h"
#include "io.h"
#include "dbio.h"
#include "utils.h"
#include "fp_display.h"
#include "mdu.h"

#include "usb_fs/MDD File System/FSIO.h"
#include "usb_fs/USB/usb.h"
#include "usb_fs/USB/usb_host_msd.h"
#include "usb_fs/USB/usb_host_msd_scsi.h"


/****************************************************************************/


uint16_t spi_read_write(uint16_t data)
{
  return spi2_write(data);
}


int get_s8_irq(void)
{
  return dbio_get_s8_nirq();
}


/*
 * Progress monitor.
 *
 * Input params :
 *  ratio  : 0..100 percent of completion
 *
 * This call back is called periodically and passed a percent of completion
 * (ratio) parameter.
 */
void progress_monitor(unsigned char ratio)
{
  char dbuff[12];

  iToStr(ratio, dbuff);
  fpd_write_string(1, 0, dbuff);
  fpd_write_string(1, 4, "\045");
}


/*
 * Report status.
 *
 * Input params :
 *  code     : main status code
 *  sub_code : this is an error code reported by the algo layer
 *
 * This call back is called to notify the different stages of 
 * programming.
 */
void report_status(unsigned char code)
{
  switch (code)
  {
    case MDU_START_PROCESS: 
      fpd_clear();
      fpd_write_string(0, 4, "Updt MODDSP");
      delay_ms(2000);
      break;

    case MDU_PROGRAMMING_SUCCESS:
      fpd_clear();
      fpd_write_string(0, 4, "SUCCESS !  ");
      delay_ms(2000);
      fpd_clear();
      break;

    case MDU_PROGRAMMING_FAILED:
      fpd_clear();
      fpd_write_string(0, 4, "FAILED :(  ");
      delay_ms(2000);
      fpd_clear();
      break;

    case MDU_START_CHECK_FILE:
      fpd_clear();
      fpd_write_string(0, 4, "Check file  ");
      break;

    case MDU_ERASING:
      fpd_clear();
      fpd_write_string(0, 4, "Erasing     ");
      break;

    case MDU_FLASHING:
      fpd_clear();
      fpd_write_string(0, 4, "Flashing    ");
      break;

    case MDU_WRONG_CRC:
      fpd_clear();
      fpd_write_string(0, 4, "CRC Error   ");
      delay_ms(2000);
      break;

    case MDU_WRONG_MAGIC:
      fpd_clear();
      fpd_write_string(0, 4, "Wrong ID    ");
      delay_ms(2000);
      break;
  }
}


void* fsopen(char* filename, char* mode)
{
  return (void*)FSfopen(filename, mode);
}


void fsclose (void* fp)
{
  FSfclose((FSFILE*)fp);
}


static int fsread(void* fhandle, unsigned char* buff, int len)
{
  return FSfread(buff, 1, len, (FSFILE*)fhandle);
}


static int fsrewind(void* fp)
{
  FSrewind((FSFILE*)fp);
  return 0;
}


static int32_t fsgetsize(void* fhandle)
{
  FSFILE* fp = (FSFILE*)fhandle;

  return fp->size;
}


void mdu_s8_force_invalid_mode(void)
{
  selectNetworkInput();
  dbio_s8_force_invalid_mode();
}

void mdu_s8_disable_forced_invalid_mode(void)
{
  selectUserSelectedInput();
  dbio_s8_disable_forced_invalid_mode();
}


static mdu_ops_type mdu_ops_var = 
{
  spi_read_write,
  md_assert_cs,
  md_deassert_cs,
  dbio_assert_s8_reset,
  dbio_deassert_s8_reset,
  get_s8_irq,

  delay_ms,

  mdu_s8_force_invalid_mode,
  mdu_s8_disable_forced_invalid_mode,

  progress_monitor,
  report_status,

  fsopen,
  fsclose,
  fsread,
  fsrewind,
  fsgetsize,
};
static mdu_ops_type* mdu_ops = &mdu_ops_var;


/****************************************************************************/


void hal_init(void)
{
  mdu_register(mdu_ops);
}


/****************************************************************************/
