
/*
 * File:         pcm1792
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  pcm1792 DAC driver
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef	__PCM1792_H
#define	__PCM1792_H

/** I N C L U D E S **********************************************************/


#include <p24fxxxx.h>

#include "periph_spi.h"
#include "periph_timer.h"


/** D E F I N E S ************************************************************/
// Chip select lines, when SPI bus used.
//#define CS_DAC_L			LATEbits.LATE6
//#define CS_DAC_R			LATEbits.LATE5

// Left/Right selection, when SPI bus used.
#define DAC_ID_LEFT			0x10
#define DAC_ID_RIGHT		0x01

// Device registers.
#define DAC_REG_16			0x10	// 16.
#define DAC_REG_17			0x11	// 17.
#define DAC_REG_18			0x12	// 18.
#define DAC_REG_19			0x13	// 19.
#define DAC_REG_20			0x14	// 20.
#define DAC_REG_21			0x15	// 21.
#define DAC_REG_22			0x16	// 22.
#define DAC_REG_23			0x17	// 23.


/** D E C L A R A T I O N S **************************************************/
void initPCM1792 (unsigned char id);
void mutePCM1792 (unsigned char id);
void unmutePCM1792 (unsigned char id);


/** E N D   O F   F I L E ****************************************************/
#endif
