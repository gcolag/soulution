
all:
#	scp "out/firmware.hex" 192.168.131.1:/Volumes/EXT/firmware.hex
#	ssh 192.168.131.1 "diskutil umount /Volumes/EXT"
	ssh 192.168.131.1 df
	scp out/firmware.hex 192.168.131.1:/media/julien/EXT/firmware.hex
	ssh 192.168.131.1 "sync;umount /media/julien/EXT"
	ssh 192.168.131.1 df

v2.8.2.0:
	ssh 192.168.131.1 df
	scp ../dac760_firmware_v2.8.2.0.hex 192.168.131.1:/media/julien/EXT/firmware.hex
	ssh 192.168.131.1 "sync;umount /media/julien/EXT"
	ssh 192.168.131.1 df

eject:
#	ssh 192.168.131.1 "diskutil umount /Volumes/EXT"
	ssh 192.168.131.1 "sync;umount /media/julien/EXT"

