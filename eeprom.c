
/*
 * File:         eeprom
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/10
 * Description:  Internal EEPROM driver
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/** I N C L U D E S **********************************************************/


#include <p24fxxxx.h>
#include <PPS.h>
#include <spi.h>

#include "periph_spi.h"
#include "io.h"

#include "eeprom.h"


/** D E F I N I T I O N S ****************************************************/


// HAL
//


static UINT8 spi_read_write(UINT8 dataOut)
{
  return spi1_write(dataOut);
}


#define assert_cs   assert_flash_cs
#define deassert_cs deassert_flash_cs


static void openSPI(unsigned int config1,
                    unsigned int config2,
                    unsigned int stat)
{
  spi1_close();
  mOpenSPI1(config1, config2, stat);
}


static void closeSPI(void)
{
  mCloseSPI1();
  spi1_open(0);
}


static void writeSPI(unsigned char i)
{
  spi_read_write(i);
}


static unsigned char readSPI(void)
{
  return spi_read_write(0x00);
}


/** V A R I A B L E S ********************************************************/
// Private Status buffer.
static T_FlashStatus FlashStatus;
static int g_device_type = -1;


/** P R I V A T E  P R O T O T Y P E S ***************************************/
void flashReadData(dword address, byte* buffer, word len);
byte flashReadStatus(void);
void flashSectorErase(dword address);
void flashWriteData(dword address, byte* buffer, word len);

static void check_device_type(void);
static void flash_unprotect(void);


/** F U N C T I O N S ********************************************************/
/*****************************************************************************
 * loadConfigFromFlash
 *
 * Description			: This function loads userConfig data from the EEPROM
 *
 *****************************************************************************/
void loadConfigFromFlash(void)
{
	unsigned char dSize = sizeof(userConfig);
	unsigned char * ptr = (unsigned char *) (&userConfig);

	check_device_type();

	// Read data from external flash memory.
	flashReadData(FLASH_SECTOR, ptr, dSize);
}

/*****************************************************************************
 * writeConfigToFlash
 *
 * Description			: This function saves userConfig data in the EEPROM
 *
 *****************************************************************************/
void writeConfigToFlash(void)
{
	unsigned char dSize = sizeof(userConfig);
	unsigned char * ptr = (unsigned char *)(&userConfig);
	
	check_device_type();

	// Erase sector - flash memory !
	flashSectorErase(FLASH_SECTOR);
	
	// Wait end of operation.
	do
	{
		FlashStatus.value = flashReadStatus();
	} while (FlashStatus.wip == 1);
	
	// Write data to external flash memory.
	flashWriteData(FLASH_SECTOR, ptr, dSize);
	
	// Wait end of operation.
	do
	{
		FlashStatus.value = flashReadStatus();
	} while (FlashStatus.wip == 1);
}

/*****************************************************************************
 * flashReadData
 *
 * Description			: This function reads data in the EEPROM
 *
 *****************************************************************************/
void flashReadData(dword address, byte* buffer, word len)
{
	word ctr;
	byte* pAddress;
	
	// 24 bytes adress only.
	address &= 0x00FFFFFF;
	pAddress = (byte*)&address;
	
	openSPI(MASTER_ENABLE_ON | PRI_PRESCAL_4_1 | SEC_PRESCAL_4_1 | SPI_SMP_OFF |
			SPI_CKE_OFF | CLK_POL_ACTIVE_LOW, 0, SPI_ENABLE);
	
	// Send READ command.
  assert_cs();
	writeSPI(STD_READ);
	
	// Send starting address.
	writeSPI(*(pAddress+2));
	writeSPI(*(pAddress+1));
	writeSPI(*(pAddress+0));
	
	// Read data from flash memory.
	for (ctr=0 ; ctr<len ; ctr++)
	{
		buffer[ctr] = readSPI();
	}

	deassert_cs();
	
	closeSPI();
}


byte flashReadStatus(void)
{
	byte status;
	
	openSPI(MASTER_ENABLE_ON | PRI_PRESCAL_4_1 | SEC_PRESCAL_1_1 | SPI_SMP_OFF |
			SPI_CKE_OFF | CLK_POL_ACTIVE_LOW, 0, SPI_ENABLE);
	
	// Send RDSR (ReaD Status register) command.
  assert_cs();
	writeSPI(RDSR);
	// Read status register from flash memory.
	status = readSPI();
	deassert_cs();
	
	closeSPI();
	
	return status;
}


static void flash_unprotect(void)
{
	openSPI(MASTER_ENABLE_ON | PRI_PRESCAL_4_1 | SEC_PRESCAL_1_1 | SPI_SMP_OFF |
			SPI_CKE_OFF | CLK_POL_ACTIVE_LOW, 0, SPI_ENABLE);

	// Send WREN (WRite Enable) command.
  assert_cs();
	writeSPI(WREN);
	deassert_cs();

  // Unprotect
  assert_cs();
	writeSPI(WRSR);
	writeSPI(0x00);
	deassert_cs();

	closeSPI();
}


void flashSectorErase(dword address)
{
	byte* pAddress = (byte*)&address;

	// -------------------------------------------------------------------------
	// The whole memory can be erased using the Bulk Erase instruction, or a 
	// sector at a time, using the Sector Erase instruction.
	// 1 sector = 256 pages = 256 * 256 bytes = 0x10000.
	// -------------------------------------------------------------------------
	// M25P40 sector addresses = 0x010000, 0x020000, ...0x070000 ( 8 sectors).
	// -------------------------------------------------------------------------
	address &= 0x0F0000; // First address on sector.
	
	openSPI(MASTER_ENABLE_ON | PRI_PRESCAL_4_1 | SEC_PRESCAL_1_1 | SPI_SMP_OFF |
			SPI_CKE_OFF | CLK_POL_ACTIVE_LOW, 0, SPI_ENABLE);
	
	// Send WREN (WRite Enable) command.
  assert_cs();
	writeSPI(WREN);
	deassert_cs();

	// Send SE (Sector Erase) command.
  assert_cs();
	writeSPI(SE);
	
	// Send starting address.
	writeSPI(*(pAddress+2));
	writeSPI(*(pAddress+1));
	writeSPI(*(pAddress+0));
	
	deassert_cs();
	
	closeSPI();
}

void flashWriteData(dword address, byte* buffer, word len)
{
	word ctr = 0;
	byte* pAddress;

	// 24 bytes adress only.
	address &= 0x00FFFFFF;
	pAddress = (byte*)&address;
	
	openSPI(MASTER_ENABLE_ON | PRI_PRESCAL_4_1 | SEC_PRESCAL_1_1 | SPI_SMP_OFF |
			SPI_CKE_OFF | CLK_POL_ACTIVE_LOW, 0, SPI_ENABLE);
	
  switch (g_device_type)
  {
    case SST25VF040:
    {
      int i;

      for (i=0 ; i<len ; i++)
      {
        // Send WREN (WRite Enable) command.
        assert_cs();
        writeSPI(WREN);
        deassert_cs();
        
        // Send PP (Page Program) command, followed by data bytes.
        assert_cs();
        writeSPI(PP);
        
        // Send starting address.
        writeSPI(*(pAddress+2));
        writeSPI(*(pAddress+1));
        writeSPI(*(pAddress+0));
        address++;
        
        writeSPI(buffer[i]);
        deassert_cs();
        
        delay_ms(1);
      }

      break;
    }
    default:
    {
      // Send WREN (WRite Enable) command.
      assert_cs();
      writeSPI(WREN);
      deassert_cs();
      
      // Send PP (Page Program) command, followed by data bytes.
      assert_cs();
      writeSPI(PP);
      
      // Send starting address.
      writeSPI(*(pAddress+2));
      writeSPI(*(pAddress+1));
      writeSPI(*(pAddress+0));

      // Send data.
      for (ctr=0 ; ctr<len ; ctr++) {
        writeSPI(buffer[ctr]);
      }

      deassert_cs();
      break;
    }
  }

	closeSPI();
}

/*****************************************************************************
 * flashReadID
 *
 * Description			: This function reads the flash ID
 *
 *****************************************************************************/
void flashReadID(byte* buffer)
{
	word ctr;
	
	openSPI(MASTER_ENABLE_ON | PRI_PRESCAL_4_1 | SEC_PRESCAL_4_1 | SPI_SMP_OFF |
			SPI_CKE_OFF | CLK_POL_ACTIVE_LOW, 0, SPI_ENABLE);
	
	// Send RDID command.
  assert_cs();
	writeSPI(RDID);
	
	// Read data from flash memory.
	for (ctr=0 ; ctr<3 ; ctr++)
	{
		buffer[ctr] = readSPI();
	}

	deassert_cs();
	
	closeSPI();
}


static void check_device_type(void)
{
  unsigned char id_buffer[10];

  if (g_device_type != -1) return;

  flashReadID(id_buffer);

  if ( (id_buffer[0] == 0xBF) && 
       (id_buffer[1] == 0x25) && 
       (id_buffer[2] == 0x8D) )
  {
    g_device_type = SST25VF040;
    flash_unprotect();
  }
  else
  {
    g_device_type = M25P40;
  }
}


/** E N D   O F   F I L E ****************************************************/
