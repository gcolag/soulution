
/*
 * File:         encoder
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB S�rl
 *               Copyright ABC PCB S�rl 2015
 * Created:      2015/04/10
 * Description:  Encoder management
 *
 * History :
 *
 *   2015/04/10  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/

#ifndef	__ENCODER
#define	__ENCODER

/** I M P O R T A N T ********************************************************/
// You must add the "USE_AND_OR" macro definition to your project build
// options under MPLAB C30 tab.
// Alternatively you can uncomment the line below.
/*****************************************************************************/
//#define USE_AND_OR


/** I N C L U D E S **********************************************************/

#include <p24fxxxx.h>
#include <inCap.h>

#include "io.h"
#include "variables.h"
#include "periph_timer.h"


/*****************************************************************************/


// HAL
//

#define encoder_get_A_push_button  get_encoder_A_push_button

#define encoder_get_rot_A_A        get_rotA_A
#define encoder_get_rot_A_B        get_rotA_B


#define encoder_get_B_push_button  get_encoder_B_push_button

#define encoder_get_rot_B_A        get_rotB_A
#define encoder_get_rot_B_B        get_rotB_B


/** D E F I N E S ************************************************************/


#define IDLE					0x00
#define CW						0x01
#define CCW						0x02

#define ENCODER_STEP_TIME 		150		// en ms
#define ENCODER_RELEASE_TIME 	350		// en ms


/** T Y P E S ****************************************************************/
typedef struct 
{
	char 			direction;			// voir dessus d�finitions 
	unsigned int 	speed;				// incr�ment � chaque passage pour le nombre de pas 
	unsigned int 	counter;			// nombre de pas depuis le 1er pas de la salve 
	unsigned int 	timer;				// nombre de ms depuis le 1er pas de la salve 
} encoderStruct;


/** D E C L A R A T I O N S **************************************************/


void initEncoderA (void);
void captureEncoderIsrA (void);
void readEncoderKeyA (encoderStruct *);

void initEncoderB (void);
void captureEncoderIsrB (void);
void readEncoderKeyB (encoderStruct *);


/** E N D   O F   F I L E ****************************************************/
#endif
