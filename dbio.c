
/*
 * File:         dbio
 * Project:      Soulution DAC760
 * Author:       Julien Linder (jlinder@abc-pcb.com)
 *               for ABC PCB Sàrl
 *               Copyright ABC PCB Sàrl 2015
 * Created:      2015/04/16
 * Description:  Dsp Board I/O
 *
 * History :
 *
 *   2015/04/16  1.0  Initial release
 *
 * Bugs:         
 *
 */

/*****************************************************************************/


#include "pca9555.h"
#include "cpld_dsp_board.h"


/*****************************************************************************/


#define DSP_BOARD_GENERAL_CTRL_PCA9555_ADDRESS   0x40
#define DSP_BOARD_HDMI_CTRL_PCA9555_ADDRESS      0x42


/*****************************************************************************/


#define DBIO_BIT_CPLD_PROGAMN                15
#define DBIO_BIT_CPLD_DONE                   14
#define DBIO_BIT_CPLD_INITN                  13
#define DBIO_BIT_CPLD_INITN_FORCE            12
#define DBIO_BIT_CPLD_nUSER_PROG             11
#define DBIO_BIT_USB_LAN_nRESET              10
#define DBIO_BIT_USB_LAN_NMR_nUSB            9
#define DBIO_BIT_USB_LAN_nUSB_BUS_DETECT     8
#define DBIO_BIT_BOARD_TYPE_1                7
#define DBIO_BIT_BOARD_TYPE_0                6
#define DBIO_BIT_HARDWARE_REV1               5
#define DBIO_BIT_HARDWARE_REV0               4
#define DBIO_BIT_MAIN_MUTE                   3
#define DBIO_BIT_UNUSED_PIN2                 2
#define DBIO_BIT_OSC_22_EN                   1
#define DBIO_BIT_OSC_24_EN                   0


/*****************************************************************************/


static pca9555_dev_type  general_ctrl_dev_var;
static pca9555_dev_type *general_ctrl_dev = &general_ctrl_dev_var;

static pca9555_dev_type  hdmi_ctrl_dev_var;
static pca9555_dev_type *hdmi_ctrl_dev = &hdmi_ctrl_dev_var;


/*****************************************************************************/


int dbio_standby(void)
{
  pca9555_reg_wr(general_ctrl_dev,
                 PCA9555_REG_OUTPUT_PORT_0,
//                 0x00);
                 0x08); // Keep main mute active in standby mode 
                        // This is not a problem since we drive
                        // an optocoupler
  pca9555_reg_wr(general_ctrl_dev,
                 PCA9555_REG_OUTPUT_PORT_1,
                 0x00);

//FIXME HDMI not used yet

  return 0;
}


int dbio_init(void)
{
  general_ctrl_dev->addr = DSP_BOARD_GENERAL_CTRL_PCA9555_ADDRESS;
  hdmi_ctrl_dev->addr = DSP_BOARD_HDMI_CTRL_PCA9555_ADDRESS;
  general_ctrl_dev->shadow0 = 0x08;
  general_ctrl_dev->shadow1 = 0x00;

  /*
   *  Input  0.7 I : board type 1
   *  Input  0.6 I : board type 0
   *  Input  0.5 I : hardware rev 1
   *  Input  0.4 I : hardware rev 0
   *  Output 0.3 1 : main mute
   *  Input  0.2 I : NC
   *  Input  0.1 I : OSC 22M EN (driven by FPGA)
   *  Input  0.0 I : OSC 24M EN (driven by FPGA)
   */
  pca9555_reg_wr(general_ctrl_dev,
                 PCA9555_REG_OUTPUT_PORT_0,
                 0x08);
  pca9555_reg_wr(general_ctrl_dev,
                 PCA9555_REG_CONFIG_PORT_0,
                 0xF7);

  /*
   *  Output 1.7 0 : CPLD PROGAMN
   *  Input  1.6 I : CPLD DONE
   *  Input  1.5 I : CPLD INITN
   *  Output 1.4 0 : CPLD INITN FORCE
   *  Output 1.3 0 : CPLD nUSER/PROG
   *  Output 1.2 0 : USB_LAN nRESET
   *  Output 1.1 0 : USB_LAN NMR_nUSB
   *  Input  1.0 X : USB_LAN nUSB_BUS_DETECT
   */
  pca9555_reg_wr(general_ctrl_dev,
                 PCA9555_REG_OUTPUT_PORT_1,
                 0x00);
  pca9555_reg_wr(general_ctrl_dev,
                 PCA9555_REG_CONFIG_PORT_1,
//                 0x61);
                 0xF1); // FIXME CPLD prog not used yes  (all inputs)

//FIXME HDMI not used yet (all inputs)

  return 0;
}


/*****************************************************************************/


void dbio_assert_main_mute(void)
{
  pca9555_bit_set(general_ctrl_dev,
                  DBIO_BIT_MAIN_MUTE);
}


void dbio_deassert_main_mute(void)
{
  pca9555_bit_clr(general_ctrl_dev,
                  DBIO_BIT_MAIN_MUTE);
}


void dbio_assert_usb_lan_reset(void)
{
  pca9555_bit_clr(general_ctrl_dev,
                  DBIO_BIT_USB_LAN_nRESET);
}


void dbio_deassert_usb_lan_reset(void)
{
  pca9555_bit_set(general_ctrl_dev,
                  DBIO_BIT_USB_LAN_nRESET);
}


void dbio_select_nmr(void)
{
  pca9555_bit_set(general_ctrl_dev,
                  DBIO_BIT_USB_LAN_NMR_nUSB);
}


void dbio_select_usb(void)
{
  pca9555_bit_clr(general_ctrl_dev,
                  DBIO_BIT_USB_LAN_NMR_nUSB);
}


int  dbio_get_usb_bus_detect(void)
{
  return pca9555_bit_get(general_ctrl_dev, DBIO_BIT_USB_LAN_nUSB_BUS_DETECT);
}


int  dbio_get_osc_22M_en(void)
{
  return pca9555_bit_get(general_ctrl_dev, DBIO_BIT_OSC_22_EN);
}


int  dbio_get_osc_24M_en(void)
{
  return pca9555_bit_get(general_ctrl_dev, DBIO_BIT_OSC_24_EN);
}


/*****************************************************************************/


#if 0

// Here for debug purpose only ; these pins are mormaly 
// driven by the FPGA.


void dbio_assert_osc_22M_en(void)
{
  pca9555_bit_set(general_ctrl_dev,
                  DBIO_BIT_OSC_22_EN);
}


void dbio_deassert_osc_22M_en(void)
{
  pca9555_bit_clr(general_ctrl_dev,
                  DBIO_BIT_OSC_22_EN);
}


void dbio_assert_osc_24M_en(void)
{
  pca9555_bit_set(general_ctrl_dev,
                  DBIO_BIT_OSC_24_EN);
}


void dbio_deassert_osc_24M_en(void)
{
  pca9555_bit_clr(general_ctrl_dev,
                  DBIO_BIT_OSC_24_EN);
}

#endif


/*****************************************************************************/


// Outputs
//
#define DBIO_BIT_MUTE_ACK      6
#define DBIO_BIT_DISABLE_S8    5
#define DBIO_BIT_SEL_22M_n24M  4
#define DBIO_BIT_nUSBLAN_DIR   3
#define DBIO_BIT_DIR_nRESET    2
#define DBIO_BIT_S8_nRESET     1
#define DBIO_BIT_DIT_nRESET    0

// Inputs
//
#define DBIO_BIT_S8_nINT       2
#define DBIO_BIT_USB_LAN_n22M_24M  1
#define DBIO_BIT_DSD_nPCM      0


void dbio_select_22M(void)
{
  cdb_bit_set(DBIO_BIT_SEL_22M_n24M);
}


void dbio_select_24M(void)
{
  cdb_bit_clr(DBIO_BIT_SEL_22M_n24M);
}


void dbio_select_usblan(void)
{
  cdb_bit_clr(DBIO_BIT_nUSBLAN_DIR);
}


void dbio_select_dir(void)
{
  cdb_bit_set(DBIO_BIT_nUSBLAN_DIR);
}


void dbio_assert_dir_reset(void)
{
  cdb_bit_clr(DBIO_BIT_DIR_nRESET);
}


void dbio_deassert_dir_reset(void)
{
  cdb_bit_set(DBIO_BIT_DIR_nRESET);
}


void dbio_assert_s8_reset(void)
{
  cdb_bit_clr(DBIO_BIT_S8_nRESET);
}


void dbio_deassert_s8_reset(void)
{
  cdb_bit_set(DBIO_BIT_S8_nRESET);
}


void dbio_assert_dit_reset(void)
{
  cdb_bit_clr(DBIO_BIT_DIT_nRESET);
}


void dbio_deassert_dit_reset(void)
{
  cdb_bit_set(DBIO_BIT_DIT_nRESET);
}


void dbio_s8_force_invalid_mode(void)
{
  cdb_bit_set(DBIO_BIT_DISABLE_S8);
}


void dbio_s8_disable_forced_invalid_mode(void)
{
  cdb_bit_clr(DBIO_BIT_DISABLE_S8);
}


int dbio_get_s8_nirq(void)
{
  return cdb_get_bit(DBIO_BIT_S8_nINT);
}


int dbio_get_usb_lan_n22M_24M(void)
{
  return cdb_get_bit(DBIO_BIT_USB_LAN_n22M_24M);
}


void dbio_assert_mute_ack(void)
{
  cdb_bit_clr(DBIO_BIT_MUTE_ACK);
}


void dbio_deassert_mute_ack(void)
{
  cdb_bit_set(DBIO_BIT_MUTE_ACK);
}


/*****************************************************************************/
